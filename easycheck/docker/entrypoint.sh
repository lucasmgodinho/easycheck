#!/bin/sh

chmod -R 777 /var/www/html/Homologacao/assets
chmod -R 777 /var/www/html/Homologacao/protected/runtime

cd /var/www/html/Homologacao/protected && /var/www/html/Homologacao/protected/yiic migrate --interactive=0
/bin/sh -c apache2-foreground
