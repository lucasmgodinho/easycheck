<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '',
    'language' => 'pt_br',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.commands.*',
        'application.components.helpers.*',
        'application.components.helpers.esquemas_xsd_s1_0.*',
        'application.controllers.*',
    ),
    'aliases' => array(
        'xupload' => 'ext.xupload'
    ),
    'modules' => include 'modules.php',
    // application components
    'components' => array(
        'user' => array(
            'autoUpdateFlash' => false
            // enable cookie-based authentication
        ),
        'session' => array(
            'class' => 'CDbHttpSession',
            //'cacheID' => 'sessionCache',
            'timeout' => '14400',
            //'sessionName'=>'GeneralSession'
//            'connectionID' => 'db',
            'autoCreateSessionTable' => true,
        ),
        'cache' => array(
            'class' => 'CZendDataCache',
        ),
        'db' => include 'bd.php',
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
        'excel'=>array(
            'class'=>'ext.phpexcel.vendor.PHPExcel',
        ),
        'format' => array(
            'class'=>'application.components.Formatter',
            'booleanFormat' => array('Não', 'Sim'),
            'dateFormat' => 'd/m/Y',
            'datetimeFormat' => 'd/m/Y H:i:s'
        ),
    ),
    'params' => array(
        'defaultPageSize' => 10,
        'uploadPath' => dirname(__FILE__, 4) . '/arquivos_migracao/',
        'uploadUrl' => MAIN_LINK . '/arquivos_migracao/',
    ),
);
