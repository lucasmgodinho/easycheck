<?php
$env = basename(dirname(__DIR__, 3));
$bd = "/var/www/config/" . $env . "/bd_config.php";

if (is_file($bd)) {
    return include $bd;
} else {
    return array(
        'connectionString' => getenv('EASYCHECK_CONNECTION_STRING') ?: 'pgsql:host=easycheck-db;port=5432;dbname=easycheck',
        'emulatePrepare' => false,
        'username' => getenv('EASYCHECK_PG_USERNAME') ?: 'postgres',
        'password' => getenv('EASYCHECK_PG_PASSWORD') ?: '234jco8geij2rg',
        'charset' => 'utf8'
    );
}