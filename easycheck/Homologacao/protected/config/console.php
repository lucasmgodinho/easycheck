<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '',
    'language' => 'pt_br',
    // preloading 'log' component
    'preload' => array('log'),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.components.helpers.*',
        'application.components.helpers.esquemas_xsd_s1_0.*',
        'application.commands.*',
    ),
    // application components
    'components' => array(
        'session' => array(
            'class' => 'CHttpSession',
            'timeout' => '100',
            'autoStart' => 'false',
            'cookieMode' => 'only'
        ),
        'db' => include 'bd.php',
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    'commandMap' => array(
        'migrate' => array(
            'class' => 'system.cli.commands.MigrateCommand',
            'migrationPath' => 'application.migrations',
            'migrationTable' => 'tbl_migration',
            'connectionID' => 'db',
            'templateFile' => 'application.migrations.template',
        ),
    ),
    'params' => array(
        'defaultPageSize' => 10,
        'uploadPath' => dirname(__FILE__, 4) . '/arquivos_migracao/',
        'uploadUrl' => MAIN_LINK . '/arquivos_migracao/',
    ),
);