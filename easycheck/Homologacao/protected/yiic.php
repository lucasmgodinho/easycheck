<?php

$env = basename(dirname(__DIR__, 2));
$cliente_config = "/var/www/config/" . $env . "/cliente_config.php";

if (is_file($cliente_config)) {
    include $cliente_config;
}elseif(getenv('EASYCHECK_CLIENTES')){
    $clientes = getenv('EASYCHECK_CLIENTES');
    $clientes = explode(",",$clientes);
    define('CLIENTES', $clientes);
}else{
    define('CLIENTES', ['desenvolvimento']);
}

$d = DIRECTORY_SEPARATOR;

// change the following paths if necessary
$yiic = dirname(__FILE__) . $d . '..' . $d . '..' . $d . 'yii-core' . $d . 'framework' . $d . 'yii.php';
$config = dirname(__FILE__) . $d . 'config' . $d . 'console.php';

//Inclui a classe Yii na aplicação
require_once($yiic);
//Cria a aplicação com a configuração setada em protected/config/main.php
Yii::createConsoleApplication($config)->run();
