<?php /* @var $this Controller */ ?>
<?php /* @var $content string*/ ?>
<?php
$this->beginContent('//layouts/header');
$this->endContent();
?>

<?php $this->beginContent('//mindmap/main'); ?>
<div id="content">
    <?php echo $content; ?>
</div><!-- content -->
<?php $this->endContent(); ?>

<?php
$this->beginContent('//layouts/footer');
$this->endContent();
?>
