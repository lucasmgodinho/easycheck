<?php
/**
 * Layout do corpo da página
 * Contém o menu lateral, barra superior, breadcrumbs, menu do usuário (perfil, etc), rodapé e
 * principalmente o conteúdo das páginas renderizadas
 */
$fotoUsuario = Yii::app()->user->papel == 'inspetor' ?
    Inspetor::model()->findByPk(Yii::app()->user->IDInspetor)->getFoto(32) :
    Moderador::model()->findByAttributes(['IDInspetor' => Yii::app()->user->IDInspetor])->getFoto(32);
?>

<!-- BEGIN HEADER (cabeçalho da página) -->
<div class="header navbar navbar-inverse navbar-fixed-top">
    <div id="toppanel">
        <div id="panel">
        </div>
    </div>
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <div class="container-fluid">
            <!-- BEGIN LOGO -->
            <a class="brand" href="<?= Yii::app()->homeUrl ?>">
                <img height="40" width="175" src="../../../assets/images/main/EasyCheckLogo3.png" alt="logo"/>
            </a>
            <!-- END LOGO -->
            <!-- Cabeçalho de impressão -->
            <div class="header-print visible-print">
                <p>Gerado <?= HData::agora() ?> por <?= Yii::app()->user->login_inspetor?></p>
            </div>
            <!-- Fim Cabeçalho de impressão -->
            <!-- BEGIN TOP NAVIGATION MENU (icones e outros elementos do topo) -->
            <ul class="nav pull-right" >
                <!-- BEGIN HELP BUTTON -->
                <?php //$this->widget('application.widgets.WMensagens', array());?>
                <li class="dropdown">
                    <a title="Ajuda" class="dropdown-toggle" style="height:10px"
                       href="<?= $this->createUrl('/site/', array()) ?>"><i class="icon-question-circle"></i></a>
                </li>
                <!-- END HELP BUTTON -->
                <!-- BEGIN USER INFO DROPDOWN -->
                <li class="dropdown user">
                    <a title="Minhas Configurações" href="#" class="dropdown-toggle" data-toggle="dropdown"
                       data-hover="dropdown" data-close-others="true">
                        <img alt="Minha Foto" style="height: 40px; margin-right: 3px" src="<?= $fotoUsuario ?>"/>
                        <span class="username"><?=
                            isset(Yii::app()->user->login_inspetor) ? Yii::app()->user->login_inspetor : Yii::app(
                            )->user->logout(); ?></span>
                        <i class="icon-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=
                            $this->createUrl(
                                '/Perfil/',
                                array('IDInspetor' => Yii::app()->user->IDInspetor)
                            ) ?>">
                        <li><a href="javascript:;" id="trigger_fullscreen"><i class="icon-arrows-alt"></i> Tela
                                Cheia</a></li>
                        <li><a href="index.php?r=site/logout"><i class="icon-key"></i> Sair</a></li>
                    </ul>
                </li>
                <!-- END USER INFO DROPDOWN -->
            </ul>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
    <!-- BEGIN SIDEBAR -->
    <div class="top-bar">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu">
            <?php
            $this->widget(
                'zii.widgets.CMenu',
                array(
                    'items' => $this->getMenu(),
                    'htmlOptions' => array('class' => 'page-sidebar-menu', 'id' => 'menu'),
                    'submenuHtmlOptions' => array('class' => 'sub-menu'),
                )
            );
            ?>
        </ul>
        <ul id="pesquisa" class='page-sidebar-menu'></ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
                <p>Here will be a configuration form</p>
            </div>
        </div>
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE  BREADCRUMB-->
                    <!-- END PAGE  BREADCRUMB-->

                </div>
            </div>
            <?php

            ?>

            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <div class="portlet box">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="portlet-title main">
                            <div class="caption">
                                <span class="icon-stack ">
                                    <i class="icon-bars icon-stack-2x"></i>
                                    <i class="icon-stack-1x "></i>
                                </span>
                                <?= $this->pageTitle; ?>
                            </div>
                        </div>
                        <!-- end PAGE TITLE -->
                        <div class="portlet-body main-portlet flip-scroll clearfix">
                            <?= $content; ?>
                        </div>
                        <!-- END PAGE CONTENT-->
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner">
        <?php echo date('Y');?> &copy; <b class="">EasyCheck</b> (V. 1.0)
    </div>
    <div class="footer-markup visible-print">
        <br/> <?= $this->createAbsoluteRequestUrl(); ?>
    </div>
    <div class="footer-tools">
        <a target="_blank" class="logoFooterInstituicoes"></a>
    </div>
</div>
<script>
    $('.form-actions').not('.no-buttom-form-action').prepend("<i title=\"Voltar\" style=\"cursor:pointer; margin: 8px 8px 10px 8px;\" onclick=\"window.history.back()\" id=\"btn_voltar_form\" class=\"m-icon-big-swapleft float-left\"></i>");
</script>
