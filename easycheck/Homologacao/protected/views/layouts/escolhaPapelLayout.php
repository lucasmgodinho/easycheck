<!DOCTYPE html>
<link rel="shortcut icon" type="image/gif" href="<?php echo ASSETS_LINK; ?>/images/main/favicon.png">
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/main.css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/login.css" rel="stylesheet" type="text/css"/>
    <meta charset="UTF-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <style>

        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        .full-screen {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
            background: url(../../../assets/images/main/menu_bg1.png) !important;
        }
        .content {
            background-color: #fff;
            border: 1px solid #6fb597;
            border-bottom: 0;
            width: 291px;
            margin: 100px auto;
            margin-bottom: 20%;
            padding: 30px;
            padding-top: 20px;
            padding-bottom: 50px;
            -webkit-box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2) !important;
            -moz-box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2) !important;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2) !important;
            border-radius: 20px !important;
        }
        .content h1 {
            margin-bottom: 20px;
        }
        .content form div {
            margin-bottom: 10px;
        }
        .content form button {
            padding: 10px 20px;
            background-color: #527578;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        .content form button:hover {
            background-color: #005a6a;
        }
    </style>
</head>
<body>

<div class="full-screen">
    <div class="content">
        <?php echo $content; ?>
    </div>
</div>
</body>
</html>
