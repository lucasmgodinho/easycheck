<?php /* @var $this Controller */ ?>
<?php
$this->beginContent('//layouts/header');
$this->endContent();
?>

<?php $this->beginContent('//layouts/main'); ?>
    <div id="content">
        <?php echo $content; ?>
    </div><!-- content -->
<?php $this->endContent(); ?>

<?php
$this->beginContent('//layouts/footer');
$this->endContent();
?>