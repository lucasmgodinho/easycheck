<?php
/* @var $this SiteController */
//$var = Yii::app()->cache();
$this->pageTitle = 'Home';
?>

<div class="span12 welcome-message">
    Olá <span class="user-name"> <?=Yii::app()->user->nome_inspetor?></span>, bem-vindo ao EasyCheck!
    <div style="font-size: 24px">Você realizou o login como <span class="user-name"><?=Yii::app()->user->papel?></span>.</div>
</div>

    <div class="span12 margin-0">
        <!-- BEGIN INLINE NOTIFICATIONS PORTLET-->
        <div class="portlet box empresa">
            <div class="portlet-title">
                <div class="caption"><i class="icon-cogs"></i> Notificações</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="scroller" data-height="200px" data-always-visible="1" data-rail-visible1="1">
                    <ul class="feeds">
                    </ul>
                </div>
            </div>
        </div>
        <!-- END INLINE NOTIFICATIONS PORTLET-->
    </div>
