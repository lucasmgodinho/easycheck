<?php
/* @var $this SiteController */
/* @var $error array */

?>

<div class="page-404" <?php $code = $error['errorCode']; if($code == '0' || $code == '1'){ echo 'style="margin-top: 200px;"';}?>>
    <link href="<?= ASSETS_LINK; ?>/css/error.css" rel="stylesheet" type="text/css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <div class="number">
        <?php
        if($code != '0' && $code != '1') {
            echo $code;
        }else{
            echo '<img src="'.ASSETS_LINK.'/images/main/EasyCheckLogo3.png"/>';
        }
        ?>
    </div>
    <div class="details">
        <?php
        if($code == '1'){
            ?>
            <h3>Olá! estamos em atualização...<br/></h3>
            <h4>Em breve a EasyCheck estará de volta.</h4>
            <?php
        }
        if($code == '0'){
          ?>
            <h3>Olá! essa página está em construção...<br/></h3>
            <h4>Em breve este módulo estará funcionando.</h4>
            <?php
        }
        if($code != '0' && $code != '1' && $code != '2'){
            ?>
            <h3>Isto é constrangedor...não sabemos o que fazer!<br/> <i class="icon-ban-circle icon-4x"></i></h3>
            <h4><?= $message ?></h4>
        <?php } ?>
        <?php
        if (!isset($externo) && $code != '0' && $code != '1' && $code != '2') {
            ?>
            <p>
                Você está perdido? Não se preocupe, agora você pode:
            <ul>
                <li><a href="index.php">Voltar para página inicial</a>.</li>
                <li><a href="index.php?r=site/faq">Buscar ajuda em nosso Faq</a>.</li>
                <li><a href="index.php?r=site/contatos">Reportar o acontecido</a>.</li>
            </ul>

            </p>
        <?php } ?>
    </div>
</div>
