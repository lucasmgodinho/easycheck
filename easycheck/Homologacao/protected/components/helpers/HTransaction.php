<?php

class HTransaction
{
    /* @var CDbTransaction */
    private $transaction;

    /* @var boolean */
    private $active = false;


    public function __construct()
    {
        $this->beginTransaction();
    }

    public function beginTransaction()
    {
        if (Yii::app()->db->getCurrentTransaction() === null) {
            $this->transaction = Yii::app()->db->beginTransaction();
            $this->transaction->attachbehavior('modelSaveTransaction', new CBehavior);
            $this->active = true;
        }
    }

    public function rollback()
    {
        if (!$this->active) {
            return;
        }
        $this->transaction->rollback();
    }

    public function commit()
    {
        if (!$this->active) {
            return;
        }
        $this->transaction->commit();
    }
}