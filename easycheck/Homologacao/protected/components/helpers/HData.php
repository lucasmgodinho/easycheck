<?php

/**
 * Classe HData composta por métodos auxiliares (Helpers) estáticas que modificam, calculam e formatam
 * Componentes de data (geralmente strings) para fazer a interface entre o que está guardado no banco de dados
 * e o que o usuário precisa/pode ver, e vice-versa.
 * @package base.Helpers
 */
class HData
{

    /**
     * Formato sql de data<BR />
     * Exemplo: 2013-10-28
     */
    CONST SQL_DATE_FORMAT = 'Y-m-d';

    /**
     * Formato brasileiro de data<BR />
     * Exemplo: 28/10/2013
     */
    CONST BR_DATE_FORMAT = 'd/m/Y';

    /**
     * Formato sql de data e hora<BR />
     * Exemplo: 2013-10-28 15:12:59
     */
    CONST SQL_DATETIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * Formato brasileiro de data e hora<BR />
     * Exemplo: 28/10/2013 15:12:59
     */
    CONST BR_DATETIME_FORMAT = 'd/m/Y H:i:s';

    /**
     * Recebe uma String que representa uma data em um determinado formato, e converte-a para
     * o formato especificado em $formatoDestino.
     * Para escolher o formato, utilize as constantes desta classe (HData)
     * @param String $data data a ser formatada. se vazio, o valor retornado será nulo
     * @param String $formatoDestino formato no qual a data deve sair (usar Constantes da classe HData)
     * @param type $formatoOrigem formato no qual a String $data está (usar Constantes da classe HData)
     * @return String data formatada. se o formato de entrada for inválido, o parametro $data passado será retornado
     */
    public static function formataData(
        $data,
        $formatoDestino = HData::BR_DATETIME_FORMAT,
        $formatoOrigem = HData::SQL_DATETIME_FORMAT
    ) {
        if ($data == null) {
            return null;
        }

        if (empty($data)) {
            return null;
        }

        $d = DateTime::createFromFormat($formatoOrigem, $data);
        if ($d != null) {
            return $d->format($formatoDestino);
        }
        return $data;
    }

    /**
     * Função para retornar a ldata do dia atual em qualquer formato passado
     * @return $data Data de 'hoje' no formato passado como parametro
     */
    public static function hoje($formatoRetorno = HData::BR_DATE_FORMAT)
    {
        $hoje = new DateTime();
        return $hoje->format($formatoRetorno);
    }

    /**
     * Retorna um timestampo com o horário atual
     * @param string $formatoRetorno formato a ser retornado
     * @return string horário atual no formato pedido
     */
    public static function agora($formatoRetorno = HData::BR_DATETIME_FORMAT)
    {
        return self::hoje($formatoRetorno);
    }

    /**
     * Converte uma data do formato SQL_DATE_FORMAT para o formato BR_DATE_FORMAT
     * Este método é um "Wrapper" para a função HData::formataData.
     * Para conversões de data mais específicas, chame-a diretamente
     * @param $data String data a ser convertida
     */
    public static function sqlToBr($data)
    {
        return self::formataData($data, HData::BR_DATE_FORMAT, HData::SQL_DATE_FORMAT);
    }

    /**
     * Converte uma data do formato  BR_DATE_FORMAT para o formato SQL_DATE_FORMAT
     * Este método é um "Wrapper" para a função HData::formataData.
     * Para conversões de data mais específicas, chame-a diretamente
     * @param $data String data a ser convertida
     */
    public static function brToSql($data)
    {
        return self::formataData($data, HData::SQL_DATE_FORMAT, HData::BR_DATE_FORMAT);
    }

    /**
     * Converte uma data do formato  BR_DATETIME_FORMAT para o formato SQL_DATETIME_FORMAT
     * Este método é um "Wrapper" para a função HData::formataData.
     * Para conversões de data mais específicas, chame-a diretamente
     * @param $data String data a ser convertida
     */
    public static function dateTimeBrToSql($data)
    {
        return self::formataData($data, HData::SQL_DATETIME_FORMAT, HData::BR_DATETIME_FORMAT);
    }

    /**
     * Converte uma data do formato SQL_DATETIME_FORMAT para o formato BR_DATETIME_FORMAT
     * Este método é um "Wrapper" para a função HData::formataData.
     * Para conversões de data mais específicas, chame-a diretamente
     * @param $data String data a ser convertida
     */
    public static function dateTimeSqlToBr($data)
    {
        return self::formataData($data, HData::BR_DATETIME_FORMAT, HData::SQL_DATETIME_FORMAT);
    }

    /*
     * @return data no padrão BR sem as horas do Timestamp
     */
    public static function dateTimeSqlToDateBr($data){
        return HData::formataData($data,HData::BR_DATE_FORMAT);
    }

    /*
     * @return data no padrão SQL sem as horas do Timestamp
    */
    public static function dateTimeSqlToDateSql($data){
        return HData::formataData($data,HData::SQL_DATE_FORMAT);
    }

    public static function converteMilisegundosParaTempo($ms)
    {
        $segundos = floor($ms / 1000);
        $minutos = floor($segundos / 60);
        $horas = floor($minutos / 60);

        $segundos = $segundos % 60;
        $minutos = $minutos % 60;
        $horas = $horas % 24;

        return sprintf('%02d:%02d:%02d', $horas, $minutos, $segundos);
    }

}

?>
