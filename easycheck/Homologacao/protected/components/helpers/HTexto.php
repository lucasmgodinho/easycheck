<?php

class HTexto
{
    /**
     * Codificação Default de todo o sistema
     */
    CONST DEFAULT_ENCODING = 'utf-8';

    /**
     * Linguagem padrão de todo o sistema
     */
    CONST DEFAULT_LANGUAGE = 'pt-br';

    /**
     * Máscara padrão para CPF
     */
    CONST MASK_CPF = '999.999.999-99';

    /**
     * Máscara Padrão para CNPJ
     */
    CONST MASK_CNPJ = '99.999.999/9999-99';

    /**
     * Máscara Padrão para CEP
     */
    CONST MASK_CEP = '99999-999';


    static $tiposPessoa = array(
        'j' => 'Jurídica',
        'f' => 'Física',
        'p' => 'Pública'
    );

    static $simOUnao = array(
        '1' => 'Sim',
        '0' => 'Não',
    );

    public static function milissegundosParaMinutos($milissegundos) {
        $minutos = floor($milissegundos / (60 * 1000)) . ' minutos';
        return $minutos;
    }

    public static function tiraAcentos($string)
    {
        return preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities($string));
    }

    public static function tiraLetras($string, $manterComparadores = true, $manterFloats = false)
    {
        if ($manterComparadores) {
            return preg_replace("/[^0-9><-]/", "", $string);
        }
        if ($manterFloats) {
            return preg_replace("/[^0-9.><-]/", "", $string);
        }
        return self::somenteNumeros($string);
    }

    public static function formataTelefone($ddd = '', $tel)
    {
        $tel = str_replace(array('.', '-'), '', $tel);
        $ddd = str_replace(array('.', '-'), '', $ddd);
        if ($ddd != '') {
            $ddd = '(' . trim($ddd) . ') ';
        }
        $tel = substr($tel, 0, strlen($tel) - 4) . '-' . substr($tel, -4); // retorna "ef"
        return $ddd . $tel;
    }

    public static function somenteNumeros($string)
    {
        return preg_replace("/[^0-9]/", "", $string);
    }

    public static function formataString($mascara, $string, $pad = '', $pad_length = 0, $pad_type = STR_PAD_RIGHT)
    {
        if (substr_count($mascara, '9') != strlen($string)) {
            return $string;
        }

        $mascara = str_replace("9", "#", $mascara);
        if ($pad != '') {
            $string = str_pad($string, $pad_length, $pad, $pad_type);
        }

        $string = str_replace(" ", "", $string);
        for ($i = 0; $i < strlen($string); $i++) {
            $mascara[strpos($mascara, "#")] = $string[$i];
        }
        return $mascara;
    }

    public static function formataCPFPrivado($mascara, $texto, $caracterVisivel)
    {
        $x = [];
        $tamanhoTexto = strlen($texto) - $caracterVisivel;
        $cpf = substr($texto, 0, $caracterVisivel);
        for ($i = 0; $i < $tamanhoTexto; $i++) {
            $x[$i] = 'x';

        }
        $cpf = $cpf . implode($x);
        $mascara = HTexto::formataString($mascara, $cpf);
        return $mascara;
    }

    public static function somenteLetras($string)
    {
        return str_replace(range(0, 9), null, $string);
    }

    public static function bindValues($texto, $parametros = array())
    {
        foreach ($parametros as $parametro => $valor) {
            $texto = str_replace('{{' . $parametro . '}}', $valor, $texto);
        }
        return $texto;
    }

    public static function explodeDbConnectionString($connectionString = '')
    {
        if (empty($connectionString)) {
            $connectionString = Yii::app()->db->connectionString;
        }
        $data = explode(':', $connectionString);
        $return['db'] = $data[0];
        $data = $data[1];
        $data = explode(';', $data);

        foreach ($data as $field) {
            $values = explode('=', $field);
            $return[$values[0]] = $values[1];
        }
        return $return;
    }


    public static function formataDecimal($numero, $casas_decimais = 2, $arredondar = true)
    {
        return HTexto::formataDecimalSql($numero, $casas_decimais, $arredondar);
    }

    public static function formataDecimalSql($valor, $casas_decimais = 2)
    {
        $valor = (string)str_replace(',', '.', $valor);
        $explode = explode('.', $valor);
        if (strrchr($valor, "E") || strrchr($valor, "e") || (isset($explode[1]) && strrchr($explode[1], 'E'))) {
            $valor = (string)str_replace('e', 'E', $valor);
            $notacaoCientifica = explode('E', $valor);
            $decimalNotacao = explode('.', $notacaoCientifica[0]);
            $casasDecimais = abs($notacaoCientifica[1]);
            !isset($decimalNotacao[1]) ?: $casasDecimais += strlen($decimalNotacao[1]);
            return sprintf('%.' . $casasDecimais . 'F', $valor);
        }

        if (count($explode) == 1) {
            return sprintf('%.0F', $valor);
        }

        $decimal = array_pop($explode);
        $parteInteira = implode('', $explode);
        $numero = ltrim($parteInteira . '.' . $decimal, '');

        if (!is_int($casas_decimais)) {
            $casas_decimais = strlen($decimal);
        }

        $numero = sprintf('%.' . $casas_decimais . 'F', $numero);
        return $numero;
    }

    public static function printMe($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        die();
    }

    public static function byteaToString($conteudo_bytea)
    {
        $return = base64_decode(stream_get_contents($conteudo_bytea));
        if(empty($return)){
            $return = base64_decode(stream_get_contents($conteudo_bytea,-1,0));
        }
        return $return;
    }

    public static function ordenaArray()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row) {
                    $tmp[$key] = iconv('UTF-8', 'ASCII//TRANSLIT', $row[$field]);
                    if (is_string($tmp[$key])) {
                        $date = DateTime::createFromFormat('d/m/Y', $tmp[$key]);
                        if ($date !== false) {
                            $tmp[$key] = $date->getTimestamp();
                        }
                    }
                }
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }


    public static function isEmptyMultiArray(array $array): bool
    {
        $empty = true;

        array_walk_recursive($array, function ($leaf) use (&$empty) {
            if (empty($leaf)) {
                return;
            }

            $empty = false;
        });

        return $empty;
    }

    public static function compare_multi_arrays($array1, $array2) {
        $differences = array();
        foreach ($array1 as $key => $value) {
            if (!array_key_exists($key, $array2)) {
                $differences[$key] = $value;
            } elseif (is_array($value)) {
                $recursive_diff = self::compare_multi_arrays($value, $array2[$key]);
                if (!empty($recursive_diff)) {
                    $differences[$key] = $recursive_diff;
                }
            } elseif ($value != $array2[$key]) {
                $differences[$key] = $value;
            }
        }
        $diff_keys = array_diff_key($array1, $array2) + array_diff_key($array2, $array1);
        foreach ($diff_keys as $key => $value) {
            if (!array_key_exists($key, $differences)) {
                $differences[$key] = $value;
            }
        }
        return $differences;
    }
}

?>
