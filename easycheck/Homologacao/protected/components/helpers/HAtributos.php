<?php

/**
 * Classe para manipular os parametros e atributos globais e locais
 * regra geral: se existir o atributo local, retorna ele. senão, tenta retornar ele global. na pior das hipoteses volta null
 * @package base.Helpers
 */
class HAtributos
{

    public static function getGlobal($param)
    {
        $session = Yii::app()->session;
        if (isset($session['atributosGlobais'][$param])) {
            return $session['atributosGlobais'][$param];
        } else {
            return null;
        }
    }

    public static function getLocal($param)
    {
        $session = Yii::app()->session;
        if (isset($session['atributosCliente'][$param])) {
            return $session['atributosCliente'][$param];
        } else {
            return self::getGlobal($param);
        }
    }

}

?>
