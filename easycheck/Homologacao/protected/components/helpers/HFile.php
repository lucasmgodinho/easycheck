<?php

/**
 * Esta classe é composta de métodos que visam auxiliar nas tarefas básicas de manipulação
 * de imagens e outros arquivos
 *
 */
class HFile
{
    CONST FOTO_X = 354;
    CONST FOTO_Y = 472;

    /**
     * Organiza uma posição do $_FILES[] para o padrão HTLM correto
     * A Forma com que o Yii Framework trabalha inputs to tipo file parece "confundir" o html,
     * de forma a sempre parecer que existem vários arquivos enviados por aquele input.
     * Para manter a coesão entre ambas as tecnologias, essa função recebe o array no formato 'multiarquivo'
     * e transforma para o padrão.
     * Para exemplificar, esta função faz este array:
     * array(1) {
     * ["Trabalhador"]=>
     * array(5) {
     *   ["name"]=>
     *   array(1) {
     *     ["foto_trabalhador"]=>
     *     string(10) "andrew.png"
     *   }
     *   ["type"]=>
     *   array(1) {
     *     ["foto_trabalhador"]=>
     *     string(9) "image/png"
     *   }
     *   ["tmp_name"]=>
     *   array(1) {
     *     ["foto_trabalhador"]=>
     *     string(24) "C:\xampp\tmp\php7673.tmp"
     *   }
     *   ["error"]=>
     *   array(1) {
     *     ["foto_trabalhador"]=>
     *     int(0)
     *   }
     *   ["size"]=>
     *   array(1) {
     *     ["foto_trabalhador"]=>
     *     int(29608)
     *   }
     * }
     * }
     * Neste Array:
     * array(5) {
     *     ["name"]=>
     *          string(10) "andrew.png"
     *     ["type"]=>
     *          string(9) "image/png"
     *     ["tmp_name"]=>
     *          string(24) "C:\xampp\tmp\php7673.tmp"
     *     ["error"]=>
     *          int(0)
     *     ["size"]=>
     *          int(29608)
     * }
     * ATENÇÃO: Este método só faz sentido se utilizado com envios únicos de arquivo!
     * @param array $file $_FILES['NOME_DO_ELEMENTO']
     * @return array
     */
    public static function organizaFile($file, $arquivo = '')
    {
        $aux = array();
        foreach ($file as $key => $value) {
            if(empty($arquivo)) {
                $aux[$key] = array_pop($value);
            }else{
                $aux[$key] = $value[$arquivo];
            }
        }
        return $aux;
    }

    /*
     * @param array $FILE $_FILES['NOME_DO_ELEMENTO']
     * Prepara uma imagem para ser armazenada
     * Além das verificações de existência, a foto é redimensionada para o tamanho FOTO_X x FOTO_Y
     * a foto é codificada em base64
     */
    public static function preparaImagemBase64($FILE, $arquivo = '', $dimensao_X = self::FOTO_X, $dimensao_y = self::FOTO_Y, $resize = true)
    {
        Yii::import('ext.upload.Upload');
        $file = HFile::organizaFile($FILE, $arquivo);

        if ($file['tmp_name'] == '' || !is_uploaded_file($file['tmp_name'])) {
            return '';
        }

        $imagem = new Upload($file);
        $imagem->image_ratio_x = $dimensao_X;
        $imagem->image_ratio_y = $dimensao_y;
        $imagem->image_resize = $resize;
        return base64_encode($imagem->process());
    }
}
