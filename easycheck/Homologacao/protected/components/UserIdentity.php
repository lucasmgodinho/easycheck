<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 * @package base.Components
 */
class UserIdentity extends CUserIdentity
{

    const ERROR_USER_EXPIRED = 3;
    const ERROR_USER_DISABLED = 4;
    const ERROR_USER_BLOCKED = 5;
    const ERROR_PROJECT_INVALID = 6;

    private $tempoBloqueio;

    /**
     * Autenticação do usuário e set informações do usuário
     * que serão necessarias para o sistema e o registro do historico de Login
     */

    /**
     * Corrigido o tempo de bloqueio do login, antes os minutos eram somados diretamente aos segundos sem fazer sua conversão para segundos
     */
    public function authenticate()
    {
        $Schema = Yii::app()->db->createCommand()
            ->select('p.IDSchema')
            ->from('public.Schema p')
            ->where(
                '"p"."slug_Schema" = :schema',
                array(':schema' => CLIENTE)
            )
            ->queryRow();

        //Consulta para a verificação do login
        $IDUsuario = Yii::app()->db->createCommand()
            ->select('*')
            ->from(CLIENTE . '.Inspetor')
            ->where("login_inspetor = :login", array(':login' => $this->username))
            ->queryRow();

        if ($IDUsuario) {
            //Consulta de verificação da senha digitada
            $hashSenhaArmazenada = Yii::app()->db->createCommand()
                ->select("senha_inspetor")
                ->from(CLIENTE . '.Inspetor')
                ->where("login_inspetor = :login", array(':login' => $IDUsuario['login_inspetor']))
                ->queryScalar();

            if (!$hashSenhaArmazenada || crypt($this->password, $hashSenhaArmazenada) != $hashSenhaArmazenada) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID; //Erro caso a consulta nao consiga encontrar o usuário
                return !$this->errorCode;

            }

            if (isset($IDUsuario['tempo_utilizacaoUsuario'])) {
                $dtCriacao = new DateTime($IDUsuario['dt_criacaoUsuario']);
                $hj = new DateTime(date("Y-m-d H:i:s"));
                $diff = $dtCriacao->diff($hj);
                if ($diff->format('%a') >= $IDUsuario['tempo_utilizacaoUsuario']) {
                    $this->errorCode = self::ERROR_USER_EXPIRED;
                }
            }

            if (!$Schema) {
                $this->errorCode = self::ERROR_PROJECT_INVALID; //Erro caso a consulta nao consiga encontrar o usuário
                return !$this->errorCode;
            } else {
                Yii::app()->session['id_schema'] = $Schema['IDSchema'];
            }

            if ($this->errorCode == 100) {
                $this->guardaInfoUsuarioSession($IDUsuario);
                $this->errorCode = self::ERROR_NONE; //retorna em caso do usuário ser autenticado
            }

        } else {
            $this->errorCode = self::ERROR_USERNAME_INVALID; //Erro caso a consulta nao consiga encontrar o usuário            
        }

        return !$this->errorCode;
    }

    /**
     * Guarda informações básicas do usuário recém-logado para posteriores usos
     * é importante saber o IDProfissional pois por ele podemos saber dados mais específicos quando
     * necessário.
     * @param array $IDUsuario array do bd com os dados do Usuário recém-logado
     */
    function guardaInfoUsuarioSession($IDUsuario)
    {
        //Informações do usuários pertinentes para o sistema
        Yii::app()->user->setState('login_inspetor', $IDUsuario['login_inspetor']);
        Yii::app()->user->setState('nome_inspetor', $IDUsuario['nome_inspetor']);
        Yii::app()->user->setState('IDInspetor', $IDUsuario['IDInspetor']);
    }

    public function setTempoBloqueio($tempoBloqueio)
    {
        $this->tempoBloqueio = $tempoBloqueio;
    }

    public function getTempoBloqueio()
    {
        return $this->tempoBloqueio;
    }

}
