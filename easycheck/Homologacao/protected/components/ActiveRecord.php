<?php


class ActiveRecord extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ActiveRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function save($param1 = true, $param2 = null, $dependentes = [], $apendentes = [])
    {
        $flagValidatePrincipal = true;

        if (!empty($dependentes)) {
            $flagValidateDependente = true;
            foreach ($dependentes as $dependente) {
                if (!is_string($dependente)) {
                    $dependente->scenario = 'validate';
                    if (!$dependente->validate()) {
                        $flagValidateDependente = false;
                        $flagValidatePrincipal = false;
                    }
                }
            }
            if ($flagValidateDependente) {
                foreach ($dependentes as $fk => $dependente) {
                    if (!is_string($dependente)) {
                        $dependente->save();
                        $this->$fk = $dependente->primaryKey;
                    } else {
                        $obj = $dependentes[$dependente];
                        $this->$fk = $obj->primaryKey;
                    }
                }
                if (!$this->validate()) {
                    foreach ($dependentes as $dependente) {
                        if (!is_string($dependente)) {
                            $dependente->delete();
                        }
                    }
                    $flagValidatePrincipal = false;
                }
            }
        }

        if ($flagValidatePrincipal) {
            if (!empty($apendentes)) {
                $principal = parent::save($param1, $param2);
                $flagValidateApendente = true;
                if ($principal) {
                    foreach ($apendentes as $apendente) {
                        $apend = $apendente;
                        $apend = array_pop($apend);
                        $key = key($apendente);
                        $apend->$key = $this->primaryKey;
                        if (!$apend->validate()) {
                            $flagValidateApendente = false;
                        }
                    }
                    if ($flagValidateApendente) {
                        foreach ($apendentes as $apendente) {
                            $apend = $apendente;
                            $apend = array_pop($apend);
                            $apend->save();
                        }
                        return true;
                    } else {
                        $this->delete();
                        if (!empty($dependentes)) {
                            foreach ($dependentes as $dependente) {
                                if (!is_string($dependente)) {
                                    $dependente->delete();
                                }
                            }
                        }
                        return false;
                    }
                }
            } else {
                return parent::save($param1, $param2);
            }
        }
        return false;
    }

    public function labelModel()
    {
        if (isset($this->attributeLabels()[$this->getPrimaryKeyAttribute()])) {
            return $this->attributeLabels()[$this->getPrimaryKeyAttribute()];
        }
        return null;
    }

    public function getPrimaryKeyAttribute()
    {
        if ($this->tableName() == null) {
            return null;
        }
        $table = $this->getMetaData()->tableSchema;
        if (is_string($table->primaryKey)) {
            return $table->primaryKey;
        } elseif (is_array($table->primaryKey)) {
            foreach ($table->primaryKey as $key => $name) {
                return $key;
            }
        } else {
            return null;
        }
    }


    public function dumpMe($die = true, $depth = 10, $highlight = true)
    {
        CVarDumper::dump($this, $depth, $highlight);
        if ($die) {
            die();
        }
    }

    public function comparaData($attribute, $params)
    {
        if (!$attribute || !isset($params['compareTo'])) {
            return null;
        }
        $comapare = $params['compareTo'];
        if (!$this->$attribute || !$this->$comapare) {
            return null;
        }
        if (!isset($params['operator'])) {
            $params['operator'] = '>=';
        }
        if (!isset($params['format'])) {
            $params['format'] = HData::BR_DATE_FORMAT;
        }
        $data1 = dateTime::createFromFormat($params['format'], $this->$attribute);
        $data2 = dateTime::createFromFormat($params['format'], $this->$comapare);

        switch ($params['operator']) {
            case '=':
            case '==':
                if ($data1 == $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser igual a "{compareValue}".';
                }
                break;
            case '!=':
                if ($data1 != $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser diferente de "{compareValue}".';
                }
                break;
            case '>':
                if ($data1 > $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser maior que "{compareValue}".';
                }
                break;
            case '>=':
                if ($data1 >= $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser maior ou igual a "{compareValue}".';
                }
                break;
            case '<':
                if ($data1 < $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser menor que "{compareValue}".';
                }
                break;
            case '<=':
                if ($data1 <= $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser menor ou igual a "{compareValue}".';
                }
                break;
            default:
                throw new CException(
                    Yii::t('yii', 'Invalid operator "{operator}".', array('{operator}' => $params['operator']))
                );
        }

        $message = Yii::t(
            'yii',
            $message,
            array(
                '{attribute}' => $this->getAttributeLabel($attribute),
                '{compareValue}' => $this->$comapare
            )
        );

        if (!empty($message)) {
            $this->addError(
                $attribute,
                $message,
                array('{attribute}' => $attribute, '{compareValue}' => $params['compareTo'])
            );
        }
    }

    public function carregarRelacoes($relations)
    {
        $encontraRealcoes = $this->with($relations)->findByPk($this->primaryKey);
        $this->setAttributesRelated($encontraRealcoes->getAttributesRelated());
    }

    public function load($attrsModel, $formModel = null)
    {
        if (!is_array($attrsModel)) {
            return false;
        }

        if (!empty($formModel) && !isset($attrsModel[$formModel])) {
            return false;
        }

        $model = get_class($this);
        if (!empty($formModel)) {
            $model = $formModel;
        }

        if (isset($attrsModel[$model])) {
            $attrsModel = $attrsModel[$model];
        }

        $this->setAttributes($attrsModel);
        return true;
    }

    public function saveAttributes($attributes)
    {
        $this->load($attributes);
        return $this->save();
    }
}
