<?php

class CMigrations extends CDbMigration
{

    public function insertMultiple($table, $data)
    {
        echo "    > insert into $table ...";
        $time=microtime(true);
        $builder=$this->getDbConnection()->getSchema()->getCommandBuilder();
        $command=$builder->createMultipleInsertCommand($table,$data);
        $command->execute();
        echo " done (time: ".sprintf('%.3f', microtime(true)-$time)."s)\n";
    }

}