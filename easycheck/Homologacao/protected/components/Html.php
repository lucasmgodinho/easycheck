<?php

/**
 * Classe Html
 * Reúne métodos estáticos que renderizam porções de código html
 * Herda quase todos os métodos da Classe CHtml.
 * A partir da versão 0.9, a CHtml não deve ser utilizada diretamente, evitando conflitos e perda de funcionalidades
 * Adicionadas nesta classe
 */
class Html extends CHtml
{

    /**
     * Gera uma lista ordenada ou não, com os array de objetos passados, podendo personificar a label
     * Se uma determinada posição do parametro $data for uma string, a posição li equivalente irá conter o seu valor
     * @param $data array de objetos|string
     * @param $label o que e como será impresso cada item da lista
     * @param $htmlOptions array com opções de HTML opcionais.
     * @param $listTag 'ul' ou 'ol' qual tipo de lista será montada
     * @return null|string
     */
    public static function htmlList($data, $label = '', $htmlOptions = array(), $listTag = 'ul')
    {
        if (empty($data)) {
            return null;
        }

        $lista = '';
        foreach ($data as $key => $item) {
            $value = $key;
            if (is_string($item)) {
                $value = $item;
            } elseif (isset($item->$label)) {
                $value = $item->$label;
            }
            $lista .= self::tag(
                'li',
                array(),
                $value
            );
        }
        $lista = self::tag($listTag, $htmlOptions, $lista);
        return $lista;

    }

    /**
     * Monta uma lista com a label, de cada Atributo, do model passado
     * @param $model Modelo que possui o atributo
     * @param $attribute array com os objetos a serem mostrados
     * @param $label propriedade ou função 'get' para ser exibida na lista
     * @param array $htmlOptions array com mais opções HTML
     * @param string $listTag tipo de lista a ser montada 'ul' ou 'ol'
     * @return null|string
     */
    public static function activeList($model, $attribute, $label, $htmlOptions = array(), $listTag = 'ul')
    {
        if (isset($model->$attribute)) {
            return self::htmlList($model->$attribute, $label, $htmlOptions, $listTag);
        }
    }

    /*
     * @return string
     * @since 10/10/2017
     */
    public static function createLink($label, $url, $urlOptions = array('target' => '_blank'))
    {
        return Html::link(
            $label,
            Yii::app()->createUrl($url),
            $urlOptions
        );
    }
}