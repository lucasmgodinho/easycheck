<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 * @package base.Components
 */
class Controller extends CController
{

    CONST MODAL_LAYOUT_FILE = '//layouts/iframe';
    CONST CLASSE_PADRAO_TIPO_MODULO = 'tipo-modulo-default';
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var int a ação que será executada neste exato momento. Vazio significa que estamos na home;
     */
    public $IDAcao = '';

    /**
     * @var Array crumbs da página
     */
    public $crumbs = array();

    /**
     * @var array array que terá todas as actions do controller com seus respectivos ID's de ação
     */
    public $acoesActions = array();

    /**
     * @var array Estará setado com as dicas cadastradas para a Ação
     */
    public $dicas = array();

    /**
     * @var array Estará setado com as ajudas cadastradas para a Ação
     */
    public $ajudas = array();

    /**
     * Essa função que permite apenas usuarios que atendam as condições,
     * especificadas na accessRules poderão acessar o sistema.
     * A função e herdada em todos os controllers do sistema
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
//            'VerificaSenhaExpirada - mudarSenhaExpirada', // Valida se o usuário pode executar aquela ação
            'VerificaAcao - error, login, logout' // Valida se o usuário pode executar aquela ação
        );
    }

    /**
     * Função para a verificar se o usuário possui a permissão para executar ação requisitada.
     * Nessa função ele também faz o Log atraves da função registrarLog().
     * @param type $filterChain
     * @throws CHttpException
     */
    public function filterVerificaAcao($filterChain)
    {
        $filterChain->run();
    }

    /**
     * Especifica o acesso que o certo tipo de usuario pode ter dentro do sistema
     * A função e herdada em todos os controllers do sistema
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', //permitir o acesso
                'actions' => array('*'), // todas as ações
                'users' => array('@'), // '@' são os usuários autenticados
            ),
            array(
                'allow', //permitir o acesso
                'actions' => array('login', 'logout', 'captcha', 'lock', 'paginaManutencao', 'enviaNotificacoes'), // apenas à essas ações
                'users' => array('*'), // '*' e qualquer tipo de usuário
            ),
            array(
                'deny', // nega acesso
                'users' => array('?'), //'?' Usuários que não identificados
            ),
        );
    }

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
    }

    /**
     * Essa Função retorna o menu que será apresentado para o usuário.
     * O menu é montado embasado nas ações que cada usuário pode acessar, através da função getAcoesUsuario(IDUsuario)
     * Buscando otimizar o processo de montagem do menu, utilizamos um processo de armazenagem do mesmo
     * na session de cada application, garantindo que, caso o menu já tenha sido montado naquela sessão,
     * ele será simplesmente processado, sem acessos ao banco de dados desnecessários.

     * @return array Array multidimensional no formato esperado pela Widget CMenu.
     */
    protected function getMenu($papel = 'inspetor')
    {
        if (!is_null(Yii::app()->session['menu'])) //caso não exista essa entrada na session...
        {
            return Yii::app()->session['menu'];
        } //menu
        if (isset(YII::app()->user->IDInspetor)) { //caso o usuário esteja autenticado
            $menu = ($this->getItensMenu($papel)); //monta o menu baseado nas ações
            YII::app()->session['menu'] = $menu; //guarda o menu na session para usos futuros
            return $menu; //retorna o menu
        } else {
            return array();
        }
    }

    /**
     * Este método é quem definitivamente formata, recupera e calcula o menu do usuário
     * Sua complexidade é dada pela normalização do banco de dados. Como o único elo entre os módulos, itens
     * de menu e tipos de módulos e o usuário são as ações delegas aos grupos que ele participa,
     * precisamos montar o menu de "baixo pra cima".
     * Se a ordem de um elemento ainda não estiver definida, usa-se o proprio nome como parâmetro de ordenação.
     * @return array ids das ações delegadas àquele usuário
     */
    protected function getItensMenu($papel)
    {

        $menuItens = array();

        if ($papel == 'moderador'){

            $modulo = Modulo::model()->findAll();

            foreach ($modulo as $modulo) {

                $label = '<span class="title">' . $modulo->slug_modulo . '</span><span class="selected"></span><span class=""></span>';
                $linkOptions = array('href' => 'index.php?r=' . $modulo->link_modulo);
                $menuItens[] = array(
                    'label' => $label,
                    'linkOptions' => $linkOptions,
                );

            }

        } else {
            $modulo = Modulo::model()->findByPk(2);

            $label = '<span class="title">' . $modulo->slug_modulo . '</span><span class="selected"></span><span class=""></span>';
            $linkOptions = array('href' => 'index.php?r=' . $modulo->link_modulo);
            $menuItens[] = array(
                'label' => $label,
                'linkOptions' => $linkOptions,
            );
        }
        return $menuItens;

    }


    /**
     * Monta o array de um modulo no padrão do CMenu.
     * @return array do modulo no padrão CMenu
     */
    protected function montaArrayModulo($modulo)
    {
        $label = '<i class="icon-' . $modulo->icone_modulo . ' MOD' . $modulo->IDModulo . '"></i><span title="' . $modulo->desc_modulo . '" class="title">' . $modulo->nome_modulo . '</span><span class="selected"></span><span class=""></span>';
        $linkOptions = array('href' => '#');
        $arrayModulo = array('label' => $label, 'linkOptions' => $linkOptions);
        return $arrayModulo;
    }

    /**
     * retorna todas as ações que podem ser utilizadas por um determinado cliente.
     * Esta função verifica quais as funções associadas a modulos disponiblizados para um projeto, através
     * da tabela projetos_tem_modulos
     */
    public function getAcoesCliente($slug)
    {

        $auxAcoes = Acao::model()->findAllBySql(
            'SELECT DISTINCT
                "AC".*
                    FROM
                "Projetos_tem_modulos" "PM"
                    INNER JOIN "Projeto" "PJ" ON "PJ".slug_projeto = \'' . $slug . '\' AND "PJ"."IDProjeto" = "PM"."IDProjeto"
        INNER JOIN "Modulo" "MO" ON "PM"."IDModulo" = "MO"."IDModulo"
        INNER JOIN "Item_menu" "IM" ON "IM"."IDModulo" = "MO"."IDModulo"
        INNER JOIN "Acao" "AC" ON "AC"."IDItem_menu" = "IM"."IDItem_menu"
        WHERE ("PM".dt_fim > now() OR "PM".dt_fim IS NULL) AND ("PM".dt_inicio < now()  OR "PM".dt_inicio IS NULL)'
        );

        foreach ($auxAcoes as $acao) {
            $acoes[$acao->IDAcao] = $acao;
        }
        return $acoes;
    }

    /**
     * Retorna a ação da action que foi requisitada, do array $acoesActions do controller.
     * Em caso de não encontrada aquela possição retornará NULL
     * @param string $action Possição do vetor que será retornada
     * @return int Retornará o valor da possição desejada
     */
    public function getActions($action)
    {
        return !empty($this->acoesActions[$action]) ? $this->acoesActions[$action] : null;
    }

    /**
     * Essa função tambem verifica  se o usuario possui certa ação, porem essa pagina
     * apenas verifica e retorna true em caso de possuir a ação e false caso não tenha.
     * Adptada para receber o id da ação tanto o número quanto a string da action, para que
     * se possa abranger verificações que não necessitem de outra action.
     * @param mixed $action Posição do vetor $acoesActions a ser retornada
     * @return boolean
     */
    public function verificaAcao($action)
    {
        if (is_int($action)) {
            $IDAcao = $action;
        } else {
            $IDAcao = $this->getActions($action);
        } //retorna o id da ação da action foi requisitada

        if (is_null($IDAcao)) //verifica se o valor e nulo ou não
        {
            return true;
        } //caso seja null, o usuário estará liberado para acessar a pagina
        else {
            return isset(Yii::app()->session['acoes'][$IDAcao]);
        }
    }

    /**
     * Função que carrega as dicas que pertence a determinada Ação
     */
    public function retornaDicas($acao)
    {
        if (!empty($acao)) //Carrega as dicas, caso existam, para aquela view
        {
            $this->dicas = Dica::model()->with('tipoDica')->findAll('"IDAcao" = :acao', array(':acao' => $acao));
        } else {
            $this->dicas = null;
        }
    }

    /**
     * Função que carrega as Ajudas que pertence a determinada Ação, adptada para retornar um array
     * id/objeto para facilitar para a localização de cada ajuda na.
     */
    public function carregaAjuda($id)
    {
        //Carrega as ajudas, caso existam, para aquela view        
        $this->ajudas = Ajuda::model()->findByPk($id);
    }

    /**
     * Função que retorna o texto a ser colocado na title para ser feita o tooltip da ajuda
     * @param int $id
     * @return String
     */
    public function retornaAjuda($id)
    {
        $this->carregaAjuda($id);
        $ajuda = $this->ajudas;
        if ($ajuda == null) {
            return false;
        }
        if ($ajuda->video_ajuda != null) {
            $title = '<b>' . $ajuda->texto_ajuda . '</b>';
            $content = $ajuda->geraIframe();
        } else {
            $title = '<b>Ajuda</b>';
            $content = $ajuda->texto_ajuda;
        }
        $retorno = '<i class="icon-question-circle popovers icon-large botaoAjuda" data-html="true" data-original-title="' . $title . '" data-content="' . $content . '"></i>';
        return $retorno;

    }

    /**
     * Renderiza uma view
     * Se @link Controller::isModalRequest() retornar verdadeiro, o layout aplicado será
     * o especificado em @link Controller::MODAL_LAYOUT_FILE;
     * para sobreescrever este comportamento, utilize o método view da classe CController,
     * que permanece inalterado
     * @param mixed $view view a ser renderizada
     * @param Array $data dados a serem passados pra view
     * @param Boolean $return Se deve-se retornar e conteúdo renderizado ou somente imprimí-lo
     */

    /**
     * Alterada para solicitar novamente a senha do usuário caso esteja inativo
     */
    public function render($view, $data = null, $return = false)
    {
        if ($this->isModalRequest()) {
            $this->layout = self::MODAL_LAYOUT_FILE;
        }

        // se o usuário não estiver inativo exibe o solicitado
//        if($this->getCookie()) {
//            $this->setCookie();
//        }else{
//            if(Yii::app()->request->requestUri != '/index.php?r=site/lock') {
//                Yii::app()->user->setState('returnUrl', Yii::app()->request->requestUri);
//                Yii::app()->request->redirect(
//                    'index.php?r=site/lock'
//                );
//            }
//        }
        parent::render($view, $data, $return);
    }

    /**
     * Retorna se o request atual vem de um modal ou não
     * @return Boolean Se o request atual vem de um modal ou não
     */
    public function isModalRequest()
    {
        return (isset($_REQUEST['in-modal']) && $_REQUEST['in-modal'] === 'in-modal');
    }

    /**
     * Redireciona para a url especificada.
     * Se @link Controller::isModalRequest() retornar verdadeiro, o modal atual será fechado
     * e a grid que originou o modal é recarregada.
     * @param mixed $url a ser enviada
     * @param boolean $terminate
     * @param int $statusCode
     */

    public function redirect($url, $terminate = true, $statusCode = 302)
    {
        if ($this->isModalRequest()) {
//            $script = "window.parent.$('.modal').on('hidden', function(){\n"
//                . "window.parent.$('.grid-view').yiiGridView('update');\n"
//                . "window.location = window.location\n"
//                . "});";
//            $script .= "window.parent.$('.modal').modal('hide');";
            $script = "window.parent.$('.grid-view').yiiGridView('update');\n
                        window.parent.$('.modal').modal('hide');\n";
            echo Html::script($script);
            YII::app()->end();
        }
        parent::redirect($url, $terminate = true, $statusCode = 302);
    }

    /**
     * Retorna a classe do Tipo de Módulo pai da página atual
     * Usa o atributo 'crumbs' da classe Controller, sendo este um array que deve ser obtido pela widget WBreadCrumbs
     * Caso essa classe não possa ser retornada, a função retornará a constante CLASSE_PADRAO_TIPO_MODULO
     * @return string Nome da Classe
     */
    public function getClassTipoModulo()
    {
        $crumbs = $this->getCrumbs();
        if ($crumbs == null) {
            return self::CLASSE_PADRAO_TIPO_MODULO;
        }
        if (isset($crumbs[1]['classe'])) {
            return $crumbs[1]['classe'];
        }
        return self::CLASSE_PADRAO_TIPO_MODULO;

    }

    /**
     * Retorna a Url completa do request atual
     * Se baseia no método {@link CController::createAbsoluteUrl}
     * @return string url no formato http://dominio.com/index.php?r=CAMINHO&OutroParamentro=123
     */
    public function createAbsoluteRequestUrl()
    {
        return $this->createAbsoluteUrl(isset($_GET['r']) ? '/' . $_GET['r'] : '', $_GET);
    }


    /**
     * Retorna o Ícone para a página atual
     * Caso não seja possível setar o ícone a partir do penultima posição das Crumbs,
     */
    public function getIconePagina()
    {
        $crumbs = $this->getCrumbs();
        if (isset($crumbs[count($crumbs) - 2])) {
            $icone = $crumbs[count($crumbs) - 2]['icone'];
        } else {
            $icone = 'reorder';
        }
        return $icone;

    }

    /**
     * Metodo para retornar e setar na session os modulos do Schema, com dentro do periodo.
     */
    public function getModulosSchema()
    {
        if (!is_null(Yii::app()->session['modulos'])) {
            return Yii::app()->session['modulos'];
        }

        $IDModulos = Yii::app()->db->createCommand()
            ->select('*')
            ->from('Modulo')
            ->queryAll();;

        foreach ($IDModulos as $IDModulo) {
            $modulos[] = $IDModulo['IDModulo'];
        }

        Yii::app()->session['modulos'] = $modulos;
        return Yii::app()->session['modulos'];
    }

    /**
     * Seta o cookie para travar a tela caso tenha inatividade.
     */
    public function setCookie(){
        if(isset( Yii::app()->user->IDInspetor)) {
            setcookie('inspetor', Yii::app()->user->IDInspetor, (time() + (HAtributos::getGlobal('TEMPO_COOKIE_INATIVO_SEGUNDOS'))));
        }
    }

    /**
     * retorna o cookie para travar a tela caso tenha inatividade.
     */
    public function getCookie(){
        if(!isset($_COOKIE['usuario']) && isset(Yii::app()->user->IDUsuario)){
            return false;
        }
        return true;
    }

    /*
     * @param $alertas, array bidimensional
     * @return void
     * @since 1.10 17/05/2017
     */
    public function criaAlertasFlash($alertas = []){
        foreach($alertas as $alerta) {
            Yii::app()->user->setFlash(
                $alerta['tipo'],
                $alerta['mensagens']
            );
        }
    }


    /*
     * @return void
     * @since 2.0 27/06/2107
     */
    public function actionPaginaConstrucao(){
        throw new CHttpException(0);
    }
}
