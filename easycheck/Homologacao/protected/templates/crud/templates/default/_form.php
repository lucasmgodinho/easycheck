<?php
/**
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>


<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'id'=>'" . $this->class2id($this->modelClass) . "-form',
	'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
            'class' => 'form-horizontal margin-0',
            )
        )); ?>\n"; ?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

    <?php
    foreach ($this->tableSchema->columns as $column) {
        if ($column->autoIncrement) {
            continue;
        }
        ?>
        <div class="control-group">
            <?php // echo "<?php echo " . $this->generateActiveLabel($this->modelClass, $column",array('class'=>'control-label')") . "; ?>
            <?php echo "<?php echo \$form->labelEx(\$model,'{$column->name}',array('class'=>'control-label'));?>\n"; ?>
            <div class="controls">
                <?php echo "<?php echo " . $this->generateActiveField($this->modelClass, $column) . "; ?>\n"; ?>
                <?php echo "<?php echo \$form->error(\$model,'{$column->name}',array('class'=>'help-inline')); ?>\n"; ?>
            </div>
        </div>
    <?php
    }
    ?>
</div>

<div class="form-actions">
    <?php echo "<?php echo Html::submitButton(\$model->isNewRecord ? 'Criar  '.\$model->labelModel() : 'Salvar Alterações', array('class'=>'botao')); ?>\n"; ?>
</div>
<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
