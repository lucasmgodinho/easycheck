<?php
/**
 * This is the template for generating the model class of a specified table.
 * - $this: the ModelCode object
 * - $tableName: the table name for this class (prefix is already removed if necessary)
 * - $modelClass: the model class name
 * - $columns: list of table columns (name=>CDbColumnSchema)
 * - $labels: list of attribute labels (name=>label)
 * - $rules: list of validation rules
 * - $relations: list of relations (name=>relation declaration)
 */
?>
<?php echo "<?php\n"; ?>

/**
* This is the model class for table "<?php echo $tableName; ?>".
*
* The followings are the available columns in table '<?php echo $tableName; ?>':
<?php foreach ($columns as $column): ?>
    * @property <?php echo $column->type . ' $' . $column->name . "\n"; ?>
<?php endforeach; ?>
<?php if (!empty($relations)): ?>
    *
    * The followings are the available model relations:
    <?php foreach ($relations as $name => $relation): ?>
        * @property <?php
        if (preg_match("~^array\(self::([^,]+), '([^']+)', '([^']+)'\)$~", $relation, $matches)) {
            $relationType = $matches[1];
            $relationModel = $matches[2];

            switch ($relationType) {
                case 'HAS_ONE':
                    echo $relationModel . ' $' . $name . "\n";
                    break;
                case 'BELONGS_TO':
                    echo $relationModel . ' $' . $name . "\n";
                    break;
                case 'HAS_MANY':
                    echo $relationModel . '[] $' . $name . "\n";
                    break;
                case 'MANY_MANY':
                    echo $relationModel . '[] $' . $name . "\n";
                    break;
                default:
                    echo 'mixed $' . $name . "\n";
            }
        }
        ?>
    <?php endforeach; ?>
<?php endif; ?>
* @package base.Models
*/
class <?php echo $modelClass; ?> extends ActiveRecord
{

/**
* Retorna o nome da tabela representada pelo Modelo.
*
* @return string nome da tabela
*/
public function tableName()
{
return <?=
strpos($tableName, '.') === false ? "'" . $tableName . "'" : "CLIENTE.'." . explode(
        '.',
        $tableName
    )[1] . '\'' ?>;
}

/**
* Retorna as regras de validação para o Modelo
* @return Array Regras de Validação.
*/
public function rules()
{
return array(
<?php foreach ($rules as $rule): ?>
    <?php echo $rule . ",\n"; ?>
<?php endforeach; ?>
// @todo Please remove those attributes that should not be searched.
array('<?php echo implode(', ', array_keys($columns)); ?>', 'safe', 'on'=>'search'),
);
}

/**
* Retorna as relações do modelo
* @return Array relações
*/
public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
return array(
<?php foreach ($relations as $name => $relation): ?>
    <?php echo "'$name' => $relation,\n"; ?>
<?php endforeach; ?>
);
}

/**
* Retorna as labels dos atributos do modelo no formato (atributo=>label)
* @return Array labels dos atributos.
*/
public function attributeLabels()
{
return array(
<?php foreach ($labels as $name => $label): ?>
    <?php echo "'$name' => '$label',\n"; ?>
<?php endforeach; ?>
);
}

/**
* Retorna uma lista de modelos baseada nas definições de filtro da tabela
* @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
*/
public function search()
{
// @todo Please modify the following code to remove attributes that should not be searched.

$criteria=new CDbCriteria;

<?php
foreach ($columns as $name => $column) {
    if ($column->type === 'string') {
        echo "\t\t\$criteria->compare('LOWER(\"$name\")',mb_strtolower(\$this->$name),true);\n";
    } else {
        if ($column->isPrimaryKey) {
            echo "\t\t\$criteria->compare('\"$name\"',HTexto::tiraLetras(\$this->$name));\n";
            $primary = $name;
        } else {
            echo "\t\t\$criteria->compare('\"$name\"',\$this->$name);\n";
        }
    }
}
?>

return new CActiveDataProvider($this, array(
'criteria'=>$criteria,
'Pagination' => array(
'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
),
<?php if (isset($primary)) {
    echo "'sort' => array(
        'defaultOrder' => '\"$primary\" DESC',
    )";
}?>
));
}

<?php if ($connectionId != 'db'): ?>
    /**
    * @return CDbConnection the database connection used for this class
    */
    public function getDbConnection()
    {
    return Yii::app()-><?php echo $connectionId ?>;
    }

<?php endif ?>
/**
* Returns the static model of the specified AR class.
* Please note that you should have this exact method in all your CActiveRecord descendants!
* @param string $className active record class name.
* @return <?php echo $modelClass; ?> the static model class
*/
public static function model($className=__CLASS__)
{
return parent::model($className);
}
}
