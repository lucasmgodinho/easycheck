<?php

/**
 * Renderiza um conjunto de campos que representa um intervalo (período) delimitado por duas datas
 * @package base.Widgets
 */
class WDataRange extends CWidget
{

    /**
     * Representação do início do intervalo
     * @var String
     */
    public $startAttribute = '';

    /**
     * Representação do fim do intervalo
     * @var String
     */
    public $endAttribute = '';

    /**
     * Modelo
     * @var CAtiveRecord
     */
    public $model = '';

    /**
     * HtmlOptions para o atributo a ser renderizado
     * @var Array
     */
    public $labelFieldHtmlOptions = array();


    private $rangeOptions = [
        'hoje' => "'Hoje': ['today', 'today']",
        'ontem' => "'Ontem': ['yesterday', 'yesterday']",
        'este_mes' => "'Este Mês': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()]",
        'mes_passado' => "'Mês Passado': [Date.today().moveToFirstDayOfMonth().add({
                        months: -1
                    }), Date.today().moveToFirstDayOfMonth().add({
                        days: -1
                    })]",
        'prox_mes' => "'Próximo Mês': [Date.today().moveToFirstDayOfMonth().add({
                        months: +1
                    }), Date.today().add({
                        months: +1
                    }).moveToLastDayOfMonth()]",
        'prox_trimeste' => "'Próximos Trimestre': [Date.today().moveToFirstDayOfMonth().add({
                        months: +1
                    }), Date.today().add({
                        months: +3
                    }).moveToLastDayOfMonth()]",
        'prox_ano' => "'Próximo Ano': [Date.today().moveToFirstDayOfMonth().add({
                        months: +1
                    }), Date.today().add({
                        years: +1
                    }).moveToLastDayOfMonth()]",
    ];

    /**
     * HtmlOptions para o atributo a ser renderizado
     * @var Array
     */
    public $rangeDefault = array('hoje','ontem','este_mes','mes_passado','prox_mes','prox_trimeste','prox_ano');

    public $rangeDates = [];

    /**
     * atributo a ser renderizado
     * @var String
     */
    public $labelFieldAttribute = '';

    public function init()
    {
        if (empty($this->rangeDates)){
            $this->rangeDates = $this->rangeDefault;
        }

        if (isset($this->labelFieldHtmlOptions['class'])) {
            $this->labelFieldHtmlOptions['class'] .= ' date-range';
        } else {
            $this->labelFieldHtmlOptions['class'] = ' date-range m-wrap span3';
        }
        $this->labelFieldHtmlOptions['readonly'] = 'readonly';
        $this->labelFieldHtmlOptions['style'] = 'cursor:pointer;';
    }

    public function run()
    {
        $insertRange = [];
        foreach ($this->rangeDates as $rage){
            $insertRange[] = $this->rangeOptions[$rage];
        }

        $insertRangeDates = implode(',', $insertRange);

        $this->render(
            'DataRange',
            array(
                'startAttribute' => $this->startAttribute,
                'endAttribute' => $this->endAttribute,
                'model' => $this->model,
                'labelFieldHtmlOptions' => $this->labelFieldHtmlOptions,
                'labelFieldAttribute' => $this->labelFieldAttribute,
                'insertRangeDates' => $insertRangeDates
            )
        );
    }

}
