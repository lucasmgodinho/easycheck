<?php

/**
 * Classe responsável por consultar e montar as breadcrumbs de cada página
 * Se baseia em um determinando Id de ação, fornecido por cada action através da utilização
 * da variávei Controller::IDAcao.
 * Caso nenhuma ação seja setada, a breadcrumb se resumirá no link para a home,
 * partindo do conceito de que esta será a única página do sistema na qual não se precisará de uma
 * determinada ação para se adentrar
 *
 * Caso seja necessário, pode-se inserir manualmente crumbs através da propriedade outrasCrumbs, que é
 * mesclada com as crumbs geradas no final do processo

 * @package base.Widgets
 */
class WBreadCrumbs extends CWidget
{

    /**
     * @var Id da ação atual
     */
    public $idAcao = '';

    /**
     * @var array crumbs
     */
    public $crumbs = array();
    /**
     * @var $separador icone da biblioteca Font Awesome)
     */
    public $separador = 'angle-right';
    /**
     * @var Outras crumbs para serem adicionadas ao final das geradas automaticamente
     */
    public $outrasCrumbs = array();

    public function init()
    {
        $this->crumbs = array_merge($this->crumbs, $this->outrasCrumbs);
        $this->render('BreadCrumbs'); //com os crumbs prontos, hora de renderizar
    }

}

?>