<?php
/**
 * Widget que constói um campo do tipo select que auto-completa o texto digitado
 * Os dados são obtidos via AJAX, a URL usada para essa consulta é definida na variável $ajaxUrl
 * Essa URL deve retornar um objeto JSON contendo as opções que serão exibidas.
 * Cada opção deve ter as propriedas "title" (texto corresponte que será exibido) e "id" (value da opção)
 * Podem ser passados parâmetros para a função AJAX em forma de Array chave-valor através do atributo $params

 * @package base.Widgets
 */
class WAutoComplete extends CWidget
{
    /**
     * @var CAtiveForm form onde a widget está localizada
     */
    public $form;

    /**
     *
     * @var CActiveRecord model ao qual o formulário pertence
     */
    public $model;

    /**
     *
     * @var String nome do atributo correspondente ao campo
     */
    public $attributeName;

    /**
     *
     * @var Array html_options vetor contendo as propriedades html do campo
     * @example 'class'=>'botao'
     */
    public $htmlOptions = array();

    /** @var String  url da função ajax que vai trazer os dados
     */
    public $ajaxUrl;

    /** @var String placeholder do select
     */
    public $placeholder;

    /** @var String texto da opção selecionada
     */
    public $selectedText;

    /**
     * @var array opcoes selecionadas (o array deve conter sub-arrays com as chaves 'id' e 'title')
     */
    public $selectedList  = array();

    /** @var bool se for true o select é multiplo
     */
    public $multiple = false;

    /** @var integer número minimo de caracteres a serem digitados param carregar os resultados
     */
    public $minimumInput = 3;

    /** @var Array de parâmetros para serem passados na função ajax
     */
    public $params = array();

    /** @var Int Máximo de resultados exibídos por página
     */
    public $maxPageResults = 10;

    public function run()
    {
        $this->render(
            'AutoComplete',
            array(
                'form' => $this->form,
                'model' => $this->model,
                'attributeName' => $this->attributeName,
                'ajaxUrl' => $this->ajaxUrl,
                'placeholder' => $this->placeholder,
                'multiple' => $this->multiple,
                'minimumInput' => $this->minimumInput,
                'selectedText' => $this->selectedText,
                'selectedList' => $this->selectedList,
                'params' => CJSON::encode($this->params),
                'htmlOptions' => $this->htmlOptions,
                'maxPageResults' => $this->maxPageResults
            )
        );
    }

} 