<?php
Yii::import('zii.widgets.CDetailView');

/**
 * Renderiza determinados dados de um modelo em uma visão simples e amigável
 * Esta classe herda quase todos os seus métodos da zii.CDetailView.
 * A novidade é a possibilidade de imprimir os dados gerados contextualizados no modelo em questão
 * @package base.Widgets
 */
class DetailView extends CDetailView
{
    public $htmlOptions = array('class' => ' detail-view table table-hover table-bordered table-striped');

    public $nullDisplay = '<span class="null-display">Não existem dados a exibir</span>';

    public $blankDisplay = 'Não existem dados a exibir';

    public $showBotaoVoltar = true;

    /**
     * Inicializa o DetailView
     * Cria a o link para o arquivo novo CSS que será utilizado.
     * Retorna a init() da classe pai.
     */
    public function init()
    {
        if ($this->cssFile === null) {
            $this->cssFile = ASSETS_LINK . '/css/detail-view.css';
        }
        $init = parent::init();

        return $init;

    }

    /**
     * Inicializa o Renderiza
     * Cria o botão para voltar para a página de busca
     * Retorna a run() da classe pai.
     */
    public function run()
    {
        $run = parent::run();
        if ($this->showBotaoVoltar && !$this->getController()->isModalRequest()) {
            echo Html::link(
                '',
                $this->getController()->createUrl('search'),
                array('class' => 'm-icon-big-swapleft print-hidden float-left', 'style' => 'margin: 8px 8px 10px 8px')
            );
        }
        return $run;

    }
}
