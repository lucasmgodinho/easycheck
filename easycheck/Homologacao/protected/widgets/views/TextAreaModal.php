<div id="modal-view-textarea" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3>Digite no campo abaixo:</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-12 containerModal"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn blue">Fechar</button>
    </div>
</div>

<script>
    function abreModalTextArea(campo) {
        $('.containerModal').html("<textarea class='span12 textAreaModal' rows= '15' placeholder='Digite aqui'></textarea>");

        $('.textAreaModal').val(campo.val());

        $('#modal-view-textarea').modal({
            show: true
        });

        $('.textAreaModal').on('change',function(){
            var textAreaModal = $(this).val();
            campo.val(textAreaModal);
        });

        $('#modal-view-textarea').on('hidden.bs.modal', function () {
            $('.containerModal').html('');
        });
    }
</script>
