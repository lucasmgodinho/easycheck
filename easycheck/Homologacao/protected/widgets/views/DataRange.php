<?php
/* @var ActiveRecord $model */
/* @var string $labelFieldAttribute */
/* @var string $labelFieldHtmlOptions */
/* @var string $startAttribute */
/* @var string $endAttribute */
/* @var string $insertRangeDates */

?>
<div class='date-range-wrapper'>
    <?php echo Html::activeTextField($model, $labelFieldAttribute, $labelFieldHtmlOptions); ?>
    <?php echo Html::activeHiddenField($model, $startAttribute, array('class' => 'date-range-inicio')); ?>
    <?php echo Html::activeHiddenField($model, $endAttribute, array('class' => 'date-range-fim')); ?>

</div>

<?php

Yii::app()->clientScript->registerScript(
    'search2',
    "
        $('#" . Html::activeId($model, $labelFieldAttribute) . "').daterangepicker({
                ranges: {
                    ".$insertRangeDates."
                },
                opens: (App.isRTL() ? 'left' : 'right'),
                format: 'dd/MM/yyyy',
                separator: ' à ',
                startDate: Date.today().add({
                    days: -29
                }),
                endDate: Date.today(),
                locale: {
                    applyLabel: 'Inserir',
                    fromLabel: 'De',
                    toLabel: 'Até',
                    customRangeLabel: 'Personalizar',
                    daysOfWeek: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sá'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    firstDay: 1
                },
                showWeekNumbers: true,
                buttonClasses: ['btn-danger']
            },
            function (start, end) {
                $('.date-range').parent('div').children('.date-range-inicio').val(start.toString('yyyy-MM-dd HH:mm:ss'));
                $('.date-range').parent('div').children('.date-range-fim').val(end.toString('yyyy-MM-dd 23:59:59'));
            });
    ",
    CClientScript::POS_READY
);
?>