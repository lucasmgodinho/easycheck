<?php
/**
 * Widget que extende a verdadeira CGridView, sobreescrevendo parametros globais de configuração
 * no construtor desta classe foi adicionado o botão para adicionar novo registro. Ele só será renderizado
 * se a opção GridView::addNew for true e o usuário tiver a permissão necessária para criar itens neste controller.
 * caso contrário, ele não será mostrado.
 * Esta funcionaliade foi implementada pela excelente e completa extensão ext.QuickDlgs
 * @package base.Widgets.Grid
 */
Yii::import('zii.widgets.grid.CGridView');
Yii::import('application.widgets.grid.ButtonColumn');
Yii::import('application.widgets.grid.CheckBoxColumn');
Yii::import('application.widgets.grid.DataColumn');


class GridView extends CGridView
{

    /**
     * Configurações de html/css do <table> da CGridView
     * @var string configurações html/css
     */
    public $itemsCssClass = 'table table-striped table-hover table-bordered flip-scroll alturaminima';

    /**
     * Texto a ser mostrado que traz informações sobre a página atual e a quantidade de registros encontrados
     * @var string Texto a ser mostrado
     */
    public $summaryText = 'Exibindo de {start} a {end} de {count} registro(s).';
    public $summaryCssClass = 'float-right contador-grid-view';
    public $cssFile = false;

    /**
     * Valor a se renderizar em caso de entrada nula
     * @var string texto a ser mostrado
     */
    public $nullDisplay = ' - ';

    /**
     * Valor a se renderizar em caso de entrada em branco
     * @var string texto a ser mostrado
     */
    public $blankDisplay = '&nbsp;';

    /**
     * caso verdadeiro, adicionar o botão de novo registro, que se abrirá em um "modal"
     * @var bool
     */
    public $addNew = true;

    /**
     * @var String classe CSS para ser colocada no menu da paginação
     */
    public $pagerCssClass = 'pagination pagination-centered';

    /**
     * @var Classe da paginação a ser utilizada
     */
    public $pager = array('class' => 'application.widgets.grid.LinkPager');

    /**
     * Habilitar o historico de navegação da tabela na url, via get
     */
    public $enableHistory = true;
    /**
     *
     * Mostrar os flashes sempre que a tabela for atualizada
     */
    public $afterAjaxUpdate = 'function(id, data){getFlashes();}';
    public $beforeAjaxUpdate;

    /**
     * @var bool Mostrar Create em um modal
     */
    public $addInModal = false;

    /**
     * @var String Monta o Create sem menu, cabeçalho e rodapé
     */
    public $addInIframe = false;

    public $rowCssClass = array();

    public $htmlOptions = [];

    /**
     * @var String Texto que será exibido no botão de cadastro do model
     */
    public $buttonCreateLinkOptions = '';

    /**
     * @var mixed Quando for false não permitirá deletar varios registros de uma só vez
     */
    public $acaoDeletarColetiva = array();
    public $acaoDesativaColetiva = array();
    public $urlDesativar = '';

    public $exibeBotaoCreate = true;

    public $widgetMenuAcoes = 'WMenuAcoes';

    /**
     * @var String Texto que será exibido no botão de cadastro do model
     */
    public $labelBotaoCadastro = '';

    /**
     * @var String que inclui algum parametro GET na url
     */
    public $AddRoute = '';

    public $newAddRoute = '';

    public function init()
    {
        if ($this->AddRoute != '') {
            if ($this->newAddRoute != '') {
                $this->AddRoute = $this->newAddRoute . $this->AddRoute;
            } else {
                $this->AddRoute = $this->controller->createUrl('create') . $this->AddRoute;
            }
        } else {
            if ($this->newAddRoute != '') {
                $this->AddRoute = $this->newAddRoute;
            } else {
                $this->AddRoute = $this->controller->createUrl('create');
            }
        }
        if (empty($this->htmlOptions['title'])) {
            $this->htmlOptions['title'] = $this->dataProvider->model->labelModel();
        }
        parent::init();
        !$this->addNew ?: $this->widget(
            'application.widgets.grid.'.$this->widgetMenuAcoes,
            array(
                'AddRoute' => (!$this->addInModal ? ($this->addInIframe ? $this->AddRoute . "&in-modal=in-modal" : $this->AddRoute) : ''),
                'addInModal' => $this->addInModal,
                'gridId' => $this->id,
                'labelBotaoCadastro' => $this->labelBotaoCadastro,
                'model' => $this->dataProvider->model,
                'acaoDeletarColetiva' => $this->acaoDeletarColetiva,
                'acaoDesativaColetiva' => $this->acaoDesativaColetiva,
                'exibeBotaoCreate' => $this->exibeBotaoCreate,
                'buttonCreateLinkOptions' => $this->buttonCreateLinkOptions
            )
        );
    }

    public function run()
    {
        $this->registerClientScript();

        echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";

        $this->renderContent();
        $this->renderKeys();

        Yii::app()->user->setState('pageSize', null);

        echo CHtml::closeTag($this->tagName);
    }


}

?>
