<div id="modal-create" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-12">
                <iframe src="<?= $this->createUrl('create', array('in-modal' => 'in-modal')); ?>"
                        style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<script>
    function refreshModal() {
        $iframe = $("#modal-create").find("iframe");
        iframeSrc = $iframe.attr("src");
        $iframe.attr("src", iframeSrc)
    }

    $("#modal-create").bind('hide', function () {
        $.fn.yiiGridView.update('<?=$IDGrid?>');
    });

</script>
