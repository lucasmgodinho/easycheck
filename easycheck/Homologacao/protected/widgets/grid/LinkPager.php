<?php

/**
 * Classe para performar a paginação das tabelas
 * Extende a classe do core original, CLinkPager
 * Algumas classes css foram modificadas para utilização do bootstrap paginator
 * as funcionalidades não fora alteradas
 * @package base.Widgets.Grid
 */
class LinkPager extends CLinkPager
{

    const CSS_FIRST_PAGE = 'first';
    const CSS_LAST_PAGE = 'last';
    const CSS_PREVIOUS_PAGE = 'previous';
    const CSS_NEXT_PAGE = 'next';
    const CSS_INTERNAL_PAGE = 'page';
    const CSS_HIDDEN_PAGE = 'hidden';
    const CSS_SELECTED_PAGE = 'disabled';

    public $hiddenPageCssClass = self::CSS_SELECTED_PAGE;
    public $selectedPageCssClass = self::CSS_SELECTED_PAGE;
    public $cssFile = false;
    public $header = false;


    protected function createPageButton($label,$page,$class,$hidden,$selected)
    {
        $pageSize = Yii::app()->user->getState(
            'pageSize',
            Yii::app()->params['defaultPageSize']);
        if($hidden || $selected)
            $class.=' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
        return '<li class="'.$class.'">'.CHtml::link($label,$this->createPageUrl($page).'&pageSize='.$pageSize).'</li>';
    }
}

?>
