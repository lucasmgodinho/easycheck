<?php

/**
 * Widget Responsável por renderizar o menu de ações das GridViews
 * Basicamente existirão 3 tipos de ações atreladas a este menu:
 *  - Adicionar Novo registro
 *  - Realizar ações com vários registros
 *  - Exportar Registros
 *  As ações que podem ser feitas com vários registros podem ser muitas, como excluir, ativar/desativar,
 *  e etc. A única ação coletiva padrão será a de exclusão múltipla. Este comportamento poderá ser desabilitado
 * em qualquer GridView.
 * @package base.Widgets.Grid
 */
class WMenuAcoes extends CWidget
{

    /**
     * @var String Caminho para a action de criação no formato ModuleID/ControllerID/ActionID
     */
    public $AddRoute = '';

    /**
     * @var Boolean se a action de criar deve ser renderizada dentro do modal ou em outra página
     */
    public $addInModal = false;

    /**
     * @var String ID da grid (GridView) que deve ser atualizada depois da inserção
     */
    public $gridId = '';

    /**
     * @var String Texto que será exibido no botão de cadastro do model
     */
    public $labelBotaoCadastro = '';

    /**
     * @var String Texto que será exibido no botão de cadastro do model
     */
    public $buttonCreateLinkOptions = '';

    /**
     * @var Array links que perfomam ações coletivas na GridView0
     */
    public $acoesColetivas = array();

    /**
     * @var Ação básica de exclusão em massa de registros selecionados desta tabela
     * caso seja setada como false, está opção não será inserida no menu de acoes
     */
    public $acaoDeletarColetiva = array();
    public $acaoDesativaColetiva = array();

    public $model;

    /**
     * @var bool Indica se o botão Criar será exibido no menu de ações
     */
    public $exibeBotaoCreate = true;

    /**
     * prepara os dados recebidos para renderizar o menu de ações
     */
    public function init()
    {
        $this->adicionarBotaoDeletarColetivo();
        $this->adicionarBotaoDesativarColetivo();
    }

    public function adicionarBotaoDeletarColetivo()
    {
        if ($this->acaoDeletarColetiva === false || (is_array($this->acaoDeletarColetiva) && !empty($this->acaoDeletarColetiva))) {
            return;
        }

        $this->acaoDeletarColetiva = array(
            'label' => '<i class="icon-trash-o"></i> Excluir Marcados',
            'href' => $this->controller->createUrl('delete', array('id' => '')),
            'ajax' => array(
                'type' => 'POST',
                'data' => 'js:{id:$.fn.yiiGridView.getSelection("' . $this->gridId . '")}',
                'id' => 'simple-link-' . uniqid(),
                'beforeSend' => 'function()
                        {                        
                            if($.fn.yiiGridView.getSelection("' . $this->gridId . '").length < 1){
                                alert("Selecione os registros que deseja excluir");
                                return false;
                            }
                        
                        $(".botao-group .open").removeClass("open");
                          if(confirm("Deseja realmente remover permanentemente todos os registros selecionados?")){
                              $("#' . $this->gridId . '").addClass("grid-view-loading");
                          }else{
                            return false;
                          }                         
                         }',
                'success' => 'function(html){
                        $.fn.yiiGridView.update("' . $this->gridId . '");
                        }'
            ),
            'htmlOptions' => array(),
        );
        $this->acoesColetivas[] = $this->acaoDeletarColetiva;
    }

    public function adicionarBotaoDesativarColetivo()
    {
        if ($this->acaoDesativaColetiva === false || (is_array($this->acaoDesativaColetiva) && !empty($this->acaoDesativaColetiva))) {
            return;
        }

        $linkOptions['data-target'] = '#modal-disable';
        $linkOptions['data-toggle'] = 'modal';
        $linkOptions['onClick'] = 'refreshDisableColetivo()';

        $this->acaoDesativaColetiva = array(
            'label' => '<i class="icon-warning"></i> Desativar/Desassociar Marcados',
            'href' => '#',
            'ajax' => array(
                'type' => 'POST',
                'data' => '',
                'id' => 'simple-link-' . uniqid(),
                'beforeSend' => 'function()
                    {
                    $(".botao-group .open").removeClass("open");
                        if(confirm("Deseja realmente desativar todos os registros selecionados?")){
                            if($.fn.yiiGridView.getSelection("' . $this->gridId . '").length >0)
                            {
                               $("#' . $this->gridId . '").addClass("grid-view-loading");
                            }
                            else
                            {
                                alert("Selecione os registros que deseja desativar");
                                return false
                            }
                        }
                        else return false;
                     }',
                'success' => 'function(html){
                       $.fn.yiiGridView.update("' . $this->gridId . '");
                    }'
            ),
            'htmlOptions' => $linkOptions,
        );

        $this->acoesColetivas[] = $this->acaoDesativaColetiva;

    }

    /**
     * renderiza o menu de ações
     * só aparecem os botões de remoção em massa e criação de registro se o cara tiver as permissões
     */

    public function run()
    {
        if (!$this->labelBotaoCadastro) {
            $this->labelBotaoCadastro = 'Cadastrar ' . $this->model->labelModel();
        }
        echo Html::openTag('div', array('class' => 'menu-acoes', 'style' => 'margin-bottom:10px'));
        //botão "Novo Registro"
        if ($this->controller->verificaAcao('create') && $this->exibeBotaoCreate) {
            echo $this->getBotaoNovoRegistro();
        }
        //Botão de atualizar
        echo $this->getBotaoAtualizar();
        echo Html::closeTag('div');
    }

    /**
     * Retorna a parte do menu com as acoes coletivas
     * caso não existam açoes coletivas no array self::acoesColetivas, null será retornado
     * @return null|String
     */
    public function getAcoesColetivas()
    {
        if (empty($this->acoesColetivas)) {
            return null;
        }
        $menu = '<li class="dropdown-submenu">
                <a class="dropdown-toggle" data-toggle="dropdown">Ações</a>
                <ul class="dropdown-menu">';
        foreach ($this->acoesColetivas as $acao) {
            $menu .= '<li>' . Html::ajaxLink(
                    $acao['label'],
                    $acao['href'],
                    $acao['ajax'],
                    $acao['htmlOptions']
                ) . '</li>';
        }

        $menu .= '</ul></li>';
        return $menu;
    }

    /**
     * Retorna a parte do menu com o botão de adicionar novo registro
     * @return null|String
     */
    public function getBotaoNovoRegistro()
    {
        $linkOptions = empty($this->buttonCreateLinkOptions) ? array('class' => 'botao botao-success') : $this->buttonCreateLinkOptions;

        if ($this->addInModal) {
            $linkOptions['data-target'] = '#modal-create';
            $linkOptions['data-toggle'] = 'modal';
            $linkOptions['onClick'] = 'refreshModal()';
            Yii::app()->controller->renderPartial(
                'application.widgets.grid.views.AddModalContent',
                array('model' => $this->model, 'IDGrid' => $this->gridId)
            );
        }
        echo Html::link(
            '<i class="icon-plus"></i> ' . $this->labelBotaoCadastro,
            $this->AddRoute,
            $linkOptions
        );

    }

    /**
     * Retorna a parte do menu com as opções de exportação
     */
    public function getAcoesExportar()
    {
        $menu = '<li class="dropdown-submenu">
                <a class="dropdown-toggle" data-toggle="dropdown">Exportar </a>
                <ul class="dropdown-menu">';
        $menu .= '<li><a class="grid-view-export" from="' . $this->gridId . '" target="CSV" href="' . YII::app()->createUrl('export/export') . '"><i class="icon-th"></i> CSV</a></li>';
        $menu .= '<li><a class="grid-view-export" from="' . $this->gridId . '" target="XML" href="' . YII::app()->createUrl('export/export') . '"><i class="icon-file"></i> XML</a></li>';
        $menu .= '</ul></li>';
        return $menu;
    }

    public function getBotaoAtualizar()
    {
        $botaoAtualizar = Html::link(
            '<i class="icon-refresh"></i> Atualizar',
            '#',
            array(
                'onclick' => '$.fn.yiiGridView.update(\'' . $this->gridId . '\')',
                'class' => 'botao',
                // 'style'=>'height: 20px;',
                'title' => 'Atualizar'
            )
        );
        $botaoAtualizar .= Html::openTag('div', array('class' => 'botao-group'));
        $botaoAtualizar .= Html::tag(
            'button',
            array(
                'class' => 'botao dropdown-toggle',
                'style' => 'height:30px',
                'data-toggle' => 'dropdown'
            ),
            html::tag('i', array('class' => 'icon-chevron-down'), '', true),
            true
        );
        $botaoAtualizar .= Html::openTag('ul', array('class' => 'dropdown-menu'));
        if ($this->controller->verificaAcao('delete')) {
            $botaoAtualizar .= $this->getAcoesColetivas();
        }
        //Ações de Exportar
        $botaoAtualizar .= $this->getAcoesExportar();
        $botaoAtualizar .= Html::closeTag('ul');
        $botaoAtualizar .= Html::closeTag('div');
        return $botaoAtualizar;

    }

}

?>
