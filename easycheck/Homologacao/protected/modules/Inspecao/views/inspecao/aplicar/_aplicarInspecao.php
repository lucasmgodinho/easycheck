<?php
/* @var $this InspecaoController */
/* @var $model Inspecao */
/* @var $form CActiveForm */

/* @var $tecnicaInspecaoChecklist TecnicaInspecaoChecklist */
/* @var $categoriaDefeitos TecnicaInspecaoCategoriaDefeito */
?>


<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'inspecao-form',
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array(
        'class' => 'form-horizontal margin-0',
    )
));
?>
<p class="note note-info-fillout">
    Responda os itens de avaliação com <b>sim</b>, <b>não</b> ou <b>não se aplica</b>.<br><br>
    Caso um item seja respondido com uma resposta <b>inesperada</b>, aparecerá um painel para que uma ou mais <b>discrepâncias</b> sejam preenchidas.
</p>

<?php
$this->renderPartial(
    'aplicar/_dadosInspecao',
    array(
        'form' => $form,
        'model' => $model,
    )
);
?>
<input type="hidden" id="tempoGasto" name="tempo_gasto" value="">
<div class="row-fluid" style="text-align: center; margin-bottom: 10px">
    <legend><h3> Itens de avaliação </h3></legend>
</div>

<div class="row-fluid">
    <div class="span12">
        <?php
        $numeroDiscrepancia = 1;
        $checklists = $model->iDTecnicaInspecao->tecnicaInspecaoChecklists;
        foreach ($checklists as $checklist) { ?>
            <table class="table table-hover table-striped table-bordered" style=" margin: auto; ">
                <thead>
                <tr>
                    <th colspan="5" style="font-size: large; text-align: center; background-color: rgba(0,128,124,0.24)">
                        <?= $checklist->nome_checklist ?>
                    </th>
                </tr>
                <tr>
                    <th colspan="5" style="font-size: small; background-color: rgba(0,133,131,0.11)">
                        <?= $checklist->descricao_checklist ?>
                    </th>
                </tr>
                </thead>
            </table>
            <?php
            foreach ($checklist->itemChecklists as $itemChecklist) {

                $itemUtilizado = InspecaoItemChecklist::model()->findByAttributes([
                    'IDInspecao' => $model->IDInspecao,
                    'IDItemChecklist' => $itemChecklist->IDItemChecklist
                ]);
                $respostaEsperada = $itemChecklist->respostaEsperada ? 'sim' : 'nao';
                $discrepancias = [];

                if ($itemUtilizado){
                    $nItem = $itemChecklist->primaryKey;
                    $respostaEsperada = $itemChecklist->respostaEsperada ? 'sim' : 'nao';

                    ?>
                    <table class="table table-hover table-striped table-bordered" style=" margin: auto; ">

                    <tbody id="item-<?= $nItem ?>">

                    <tr id="linha-item-<?=$nItem?>" class="<?= $classe = $itemUtilizado->getClasseItem($respostaEsperada) ?>" style="height: 74px;">

                        <td id="num-item-<?=$nItem?>" rowspan="<?=(count($itemUtilizado->discrepancias) + 2)?>" style="width: 5% ;text-align: center; font-size: 14px">
                            <?= '<b>#'.$itemChecklist->numeracao_item .'</b>'?>
                        </td>
                        <td colspan="2" style="width: 70%; text-align: left; font-size: 14px">
                            <?= $itemChecklist->pergunta_item ?>
                        </td>
                        <td colspan="2" style="width: 25% ;text-align: center">
                            <input id="input-<?= $nItem ?>" class="hide" name="item[<?= $nItem ?>]resposta" value="<?= $itemUtilizado->resposta_item ?>">
                            <?=

                            Html::link('<i class="icon-check-square"> Sim</i>', '',
                                array('class' => 'botao botao-'. $classe = $itemUtilizado->resposta_item == 'sim' ?
                                        ($itemUtilizado->resposta_item == $respostaEsperada ? 'success' : 'danger') : '',
                                    'id' => 'botao-sim-'.$nItem,
                                    'title'=>'Marcar como sim',
                                    'onclick' => 'marcaResposta("' . $nItem . '","' . $respostaEsperada . '","sim")')
                            ),

                            Html::link('<i class="icon-close"> Não</i>', '',
                                array('class' => 'botao botao-'. $classe = $itemUtilizado->resposta_item == 'nao' ?
                                        ($itemUtilizado->resposta_item == $respostaEsperada ? 'success' : 'danger') : '',
                                    'id' => 'botao-nao-'.$nItem,
                                    'title'=>'Marcar como não',
                                    'onclick' => 'marcaResposta("' . $nItem . '","' . $respostaEsperada . '","nao")')
                            ),

                            Html::link('<i class="icon-"> N/A </i>', '',
                                array('class' => 'botao botao-'. $classe = $itemUtilizado->resposta_item == 'na' ? 'inverse' : '',
                                    'id' => 'botao-na-'.$nItem,
                                    'title'=>'Não se aplica',
                                    'onclick' => 'marcaResposta("' . $nItem . '","' . $respostaEsperada . '","na")')
                            );

                            ?>
                        </td>
                    </tr>
                    <?php
                    $discrepancias[] = new Discrepancia();
                    if ($itemUtilizado->discrepancias){
                        $discrepancias = $itemUtilizado->discrepancias;
                    }

                    foreach ($discrepancias as $discrepancia) {
                        
                        $this->renderPartial(
                            'aplicar/_discrepancia',
                            array(
                                'form' => $form,
                                'nItem' => $nItem,
                                'model' => $model,
                                'discrepancia' => $discrepancia,
                                'categoriaDefeitos' => $categoriaDefeitos,
                                'numeroDiscrepancia' => $numeroDiscrepancia,
                            )
                        );
                        $numeroDiscrepancia++;
                    }
                    ?>
                    </tbody>
                    <tfoot id="botao-add-discrepancia-<?=$nItem?>"
                           class="<?=$classe = $itemUtilizado->getClasseItemFooter($respostaEsperada)?>"
                           style="background-color: #f2dede"><th colspan="5"><?=

                        Html::link('<i class="icon-plus" style=""> Adicionar discrepância </i>', '',
                            array('class' => 'botao botao-danger',
                                'onclick' => 'addDiscrepancia("' . $nItem . '", "' . $numeroDiscrepancia . '")'
                            )
                        );
                        ?></th>
                    </tfoot>
                    <?php
                }
            }
            ?>
            </table>
            <?php
        }
        ?>

    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        'Salvar inspeção',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>

<script>

    $(document).ready(function() {
        var startTime = Date.now();

        $('#inspecao-form').submit(function(event) {
            var endTime = Date.now();
            var timeSpent = endTime - startTime;
            $('#tempoGasto').val(timeSpent);
        });
    });

    var numeroProximaDiscrepancia = <?php echo json_encode($numeroDiscrepancia); ?>;

    function addDiscrepancia(nItem){
        var IDInspecao = <?php echo json_encode($model->primaryKey); ?>;
        console.log(numeroProximaDiscrepancia)
        $.ajax({
            url: '<?= $this->createUrl('ajaxAdicionaLinhaDiscrepancia') ?>',
            type: 'POST',
            data: {
                nItem: nItem,
                numeroDiscrepancia: numeroProximaDiscrepancia,
                IDInspecao: IDInspecao,
            },
            success: function (data) {
                if (data) {
                    var novaDiscrepancia = $(data).hide().removeClass('hidden');
                    $("#item-" + nItem).append(novaDiscrepancia);
                    $("#tipoDefeito-"+nItem + "-" + numeroProximaDiscrepancia).select2();
                    novaDiscrepancia.fadeIn(500);
                    habilitarCamposDiscrepancia(nItem, numeroProximaDiscrepancia)

                    aumentarRowspan(nItem);
                    numeroProximaDiscrepancia++;
                }
            }
        });
    }

    function marcaResposta(item, respostaEsperada, respostaMarcada){

        if (respostaEsperada === respostaMarcada) {
            escondePainelDiscrepancia(item);

            if (respostaMarcada === 'sim') {
                $('#botao-sim-' + item).addClass('botao-success')
                $('#botao-nao-' + item).removeClass('botao-danger').removeClass('botao-success')
                $('#botao-na-' + item).removeClass('botao-inverse')
            }
            if (respostaMarcada === 'nao') {
                $('#botao-sim-' + item).removeClass('botao-danger').removeClass('botao-success')
                $('#botao-nao-' + item).addClass('botao-success')
                $('#botao-na-' + item).removeClass('botao-inverse')
            }

            $('#resposta-' + item).attr('value', 'sim')
            $('#linha-item-' + item).removeClass('error').removeClass('nao-aplica').addClass('success')
            $('#input-' + item).attr('value', respostaMarcada)

        }else if (respostaMarcada === 'na'){
            escondePainelDiscrepancia(item);

            $('#botao-sim-' + item).removeClass('botao-success').removeClass('botao-danger')
            $('#botao-nao-' + item).removeClass('botao-danger').removeClass('botao-success')
            $('#botao-na-' + item).addClass('botao-inverse')

            $('#resposta-' + item).attr('value', 'na')

            $('#linha-item-' + item).removeClass('error').removeClass('success').addClass('nao-aplica')
            $('#input-' + item).attr('value', respostaMarcada)

        }else{
            mostraPainelDiscrepancia(item);

            if (respostaMarcada === 'sim') {
                $('#botao-sim-' + item).addClass('botao-danger')
                $('#botao-nao-' + item).removeClass('botao-danger').removeClass('botao-success')
                $('#botao-na-' + item).removeClass('botao-inverse')
            }
            if (respostaMarcada === 'nao') {
                $('#botao-sim-' + item).removeClass('botao-danger').removeClass('botao-success')
                $('#botao-nao-' + item).addClass('botao-danger')
                $('#botao-na-' + item).removeClass('botao-inverse')
            }

            $('#resposta-' + item).attr('value', 'nao')

            $('#linha-item-' + item).removeClass('success').removeClass('nao-aplica').addClass('error')
            $('#input-' + item).attr('value', respostaMarcada)
        }
    }

    function mostraPainelDiscrepancia(item){

        var discrepancias = document.querySelectorAll('[id^=discrepancia-' + item + ']');

        discrepancias.forEach(function(discrepancia) {

            discrepancia.classList.remove('hidden');
            $(discrepancia).hide();
            $(discrepancia).fadeIn(700);

            let partes = discrepancia.id.split('-');
            let numeroDiscrepancia = partes[partes.length - 1];
            numeroDiscrepancia = parseInt(numeroDiscrepancia, 10);

            habilitarCamposDiscrepancia(item, numeroDiscrepancia);

        });
        $("#botao-add-discrepancia-"+item).show(700);
    }

    function escondePainelDiscrepancia(item){

        var discrepancias = document.querySelectorAll('[id^=discrepancia-' + item + ']');

        discrepancias.forEach(function(discrepancia) {

            $(discrepancia).fadeOut(300);

            let partes = discrepancia.id.split('-');
            let numeroDiscrepancia = partes[partes.length - 1];
            numeroDiscrepancia = parseInt(numeroDiscrepancia, 10);

            desabilitarCamposDiscrepancia(item, numeroDiscrepancia);

        });
        $("#botao-add-discrepancia-"+item).hide(300);
    }

    function habilitarCamposDiscrepancia(item, numeroDiscrepancia){

        $("#tipoDefeito-"+item + "-" + numeroDiscrepancia).attr('disabled', false);
        $("#descricao-"+item + "-" + numeroDiscrepancia).attr('disabled', false);
        $("#localizacao-"+item + "-" + numeroDiscrepancia).attr('disabled', false);
        $("#localRepete-"+item + "-" + numeroDiscrepancia).attr('disabled', false);

    }

    function desabilitarCamposDiscrepancia(item, numeroDiscrepancia){

        $("#tipoDefeito-"+item + "-" + numeroDiscrepancia).attr('disabled', true);
        $("#descricao-"+item + "-" + numeroDiscrepancia).attr('disabled', true);
        $("#localizacao-"+item + "-" + numeroDiscrepancia).attr('disabled', true);
        $("#localRepete-"+item + "-" + numeroDiscrepancia).attr('disabled', true);

    }

    function aumentarRowspan(nItem) {
        var $element = $("#num-item-" + nItem);
        var currentRowspan = parseInt($element.attr('rowspan')) || 1;
        var newRowspan = currentRowspan + 1;
        $element.attr('rowspan', newRowspan);
    }

    function diminuirRowspan(nItem) {
        var $element = $("#num-item-" + nItem);
        var currentRowspan = parseInt($element.attr('rowspan')) || 1;
        if (currentRowspan > 1) {
            var newRowspan = currentRowspan - 1;
            $element.attr('rowspan', newRowspan);
        }
    }

    function removerDiscrepancia(nItem,numeroDiscrepancia){
        var discrepancias = document.querySelectorAll('[id^=discrepancia-' + nItem + ']');

        if (discrepancias.length > 1){
            $('#discrepancia-' + nItem + '-' + numeroDiscrepancia).fadeOut(300, function() {
                $('#discrepancia-' + nItem + '-' + numeroDiscrepancia).remove();
                diminuirRowspan(nItem);
            });
        } else {
            displayFlashes({'warning': ["O Item deve possuir ao menos uma discrepância"]});
        }
    }

</script>
