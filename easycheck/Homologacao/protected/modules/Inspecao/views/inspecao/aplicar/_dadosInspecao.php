<?php
/* @var $this InspecaoController */
/* @var $model Inspecao */
?>

<div class="row-fluid" style="margin-bottom: 20px">
    <div class="span12">
        <div class="row-fluid" style="text-align: center">
            <legend><h3> Dados da Inspeção </h3></legend>
        </div>

        <div class="row-fluid" style="background: #EFEFEF">
            <div class="span4">
                <label class="control-label">Moderador: </label>
                <div class="controls">
                    <span class="text"><b><?= $model->iDModerador ?></b></span>
                </div>
            </div>

            <div class="span4">
                <label class="control-label">Inspetor:</label>

                <div class="controls">
                    <span class="text"><b><?= $model->iDInspetor ?></b></span>
                </div>
            </div>

            <div class="span4">
                <label class="control-label">Data da Inspeção:</label>

                <div class="controls">
                    <span class="text"><b><?= HData::dateTimeSqlToDateBr($model->dt_cadastro) ?></b></span>
                </div>
            </div>

        </div>
        <div class="row-fluid" style="background: #EFEFEF">

            <div class="span4">
                <label class="control-label">Projeto:</label>

                <div class="controls">
                    <span class="text"><b><?= $model->getProjeto() ?></b></span>
                </div>
            </div>

            <div class="span4">
                <label class="control-label">Artefato:</label>

                <div class="controls">
                    <span class="text"><b><?= $model->viewArtefato() ?></b></span>
                </div>
            </div>

            <div class="span4">
                <label class="control-label">Técnica de Inspeção:</label>

                <div class="controls">
                    <span class="text"><b><?= $model->viewTecnicaInspecao() ?></b></span>
                </div>
            </div>
        </div>
    </div>
</div><br>
