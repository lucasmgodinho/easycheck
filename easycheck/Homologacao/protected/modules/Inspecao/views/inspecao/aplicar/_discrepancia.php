<?php
/* @var $this InspecaoController */
/* @var $model Inspecao */

/* @var $nItem */
/* @var $numeroDiscrepancia */
/* @var $categoriaDefeitos */
/* @var $discrepancia Discrepancia */
?>

<tr class="<?= $classeDisc = $discrepancia->isNewRecord ? 'hidden' : '';?> discrepancia" id="discrepancia-<?= $nItem ?>-<?= $numeroDiscrepancia ?>" style="height: 100px;">
    <td colspan="3">
        <legend><h4>Discrepâcia</h4></legend>
        <div>
            <label style="position: absolute; margin-left: 1%; font-size: small; margin-top: -5px">Tipo do defeito:</label>
            <label style="position: absolute; margin-left: 17.3%; font-size: small; margin-top: -5px">Descrição:</label>
            <label style="position: absolute; margin-left: 42.5%; font-size: small; margin-top: -5px">Localização:</label>
            <label style="position: absolute; margin-left: 61%; font-size: small; margin-top: -5px">Onde se repete:</label>
        </div>
        <br>
        <div class="div-textarea-container" style="margin-top: auto">

            <?php echo Html::activeDropDownList(
                $discrepancia,
                '[' . $nItem . ']['.$numeroDiscrepancia.']IDTecnicaInspecaoCategoriaDefeito',
                Html::listData(
                    $categoriaDefeitos,
                    'IDTecnicaInspecaoCategoriaDefeito',
                    'categoria_defeito'
                ),
                array('class' => 'select2 span2',
                    'disabled' => $discrepancia->isNewRecord,
                    'style' => 'margin-left: auto; padding: 10px',
                    'prompt' => 'Tipo do defeito',
                    'id' => 'tipoDefeito-'. $nItem . '-' . $numeroDiscrepancia,
                )
            ); ?>

            <?php echo Html::textArea(
                'Discrepancia[' . $nItem . ']['.$numeroDiscrepancia.'][descricao_discrepancia]',
                $discrepancia->descricao_discrepancia,
                array(
                    'class' => 'm-wrap span4',
                    'disabled' => $discrepancia->isNewRecord,
                    'rows' => 3,
                    'cols' => 1,
                    'style' => 'margin-left: 10px',
                    'placeholder' => 'Descreva detalhadamente a discrepância',
                    'id' => 'descricao-'. $nItem . '-' . $numeroDiscrepancia,
                )
            ); ?>

            <?php echo Html::textArea(
                'Discrepancia[' . $nItem . ']['.$numeroDiscrepancia.'][localizacao_discrepancia]',
                $discrepancia->localizacao_discrepancia,
                array(
                    'class' => 'm-wrap span3',
                    'disabled' => $discrepancia->isNewRecord,
                    'rows' => 3,
                    'cols' => 1,
                    'style' => 'margin-left: 10px',
                    'placeholder' => 'Aponte a localização da discrepância',
                    'id' => 'localizacao-'. $nItem . '-' . $numeroDiscrepancia,
                )
            ); ?>

            <?php echo Html::textArea(
                'Discrepancia[' . $nItem . ']['.$numeroDiscrepancia.'][localOndeRepete]',
                $discrepancia->localOndeRepete,
                array(
                    'class' => 'm-wrap span2',
                    'disabled' => $discrepancia->isNewRecord,
                    'rows' => 3,
                    'cols' => 1,
                    'style' => 'margin-left: 10px',
                    'placeholder' => 'Locais onde se repete',
                    'id' => 'localRepete-'. $nItem . '-' . $numeroDiscrepancia,
                )
            ); ?>

        </div>
    </td>
    <td colspan="1" style="width: 5%">
        <button onclick="removerDiscrepancia(<?= $nItem ?>, <?= $numeroDiscrepancia ?>)" type="button" class="botao botao-danger"">
        <i class="icon-large icon-close"></i>
    </td>
</tr>