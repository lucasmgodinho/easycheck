<?php
/* @var $model Inspecao */
?>
<script>

    var projeto = $('#<?= Html::activeId($model, "IDProjeto") ?>');
    var projetoArtefato = $('#<?= Html::activeId($model, "IDProjetoArtefato") ?>');
    var tecnicaInspecao = $('#<?= Html::activeId($model, "IDTecnicaInspecao") ?>');
    var inspetor = $('#<?= Html::activeId($model, "IDInspetor") ?>');

    $(document).ready(function() {
        $('textarea').jqte(configuracaoTextEditorSimple);
    });

    projeto.change(function() {
        retornaProjetosArtefatos();
    });

    tecnicaInspecao.change(function() {
        retornaChecklistsTecnica();
    });

    function retornaChecklistsTecnica(){

        $.ajax({
            url: '<?= $this->createUrl('ajaxRetornaChecklistsTecnica') ?>',
            type: 'POST',
            data: {
                IDTecnicaInspecao: tecnicaInspecao.val()
            },
            beforeSend: function() {
                $("#itemChecklists").html('');
                $("#itemChecklists").addClass('hide')
                $("#warning-tecnica").addClass('hide')
                $("#info-tecnica").removeClass('hidden')
            },
            success: function(data) {
                if (data) {
                    var novoChecklist = $(data).hide();
                    $("#itemChecklists").removeClass('hide').append(novoChecklist);
                    novoChecklist.fadeIn(500);
                }
            }
        });
    }

    function retornaProjetosArtefatos(){
        projetoArtefato.find('option').remove();

        $.ajax({
            url: '<?= $this->createUrl('ajaxRetornaArtefatoPorProjeto') ?>',
            type: 'POST',
            data: {
                IDProjeto: projeto.val()
            },
            beforeSend: function() {
                projetoArtefato.select2("val", "");
            },
            success: function(data) {
                if (data) {
                    projetoArtefato.attr('disabled', false).html(data);
                }
            }
        });
    }

    function ativarDesativarItem(botao) {
        var $botao = $(botao);
        var $linha = $botao.closest('tr');
        var $input = $linha.find('input[type="hidden"]');
        var $celula = $botao.closest('td');

        if ($input.length > 0) {
            // Manipula o valor do input
            if ($input.val() === 'desativado') {
                $input.val(''); // Limpa o valor
            } else {
                $input.val('desativado'); // Define o valor como 'desativado'
            }

            if ($botao.hasClass('botao-success')) {
                $botao.removeClass('botao-success').addClass('botao-danger');
                $celula.css('background-color', '#f8e1e3');
            } else if ($botao.hasClass('botao-danger')) {
                $botao.removeClass('botao-danger').addClass('botao-success');
                $celula.css('background-color', '#daecde');
            }
        }
    }







</script>
