<?php
/* @var $this InspecaoController */
/* @var $model Inspecao */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'IDInspecao'); ?>
            <?php echo $form->textField($model,'IDInspecao',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDProjetoArtefato'); ?>
            <?php echo $form->textField($model,'IDProjetoArtefato',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDInspetor'); ?>
            <?php echo $form->textField($model,'IDInspetor',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDModerador'); ?>
            <?php echo $form->textField($model,'IDModerador',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDTecnicaInspecao'); ?>
            <?php echo $form->textField($model,'IDTecnicaInspecao',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'observacao'); ?>
            <?php echo $form->textArea($model,'observacao',array('rows'=>6, 'cols'=>50,'class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_cadastro'); ?>
            <?php echo $form->textField($model,'dt_cadastro',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'finalizado'); ?>
            <?php echo $form->checkBox($model,'finalizado'); ?>
        </div>

        <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->