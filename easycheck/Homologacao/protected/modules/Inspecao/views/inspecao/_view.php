<?php
/* @var $this InspecaoController */
/* @var $data Inspecao */
?>

<div class="view">

    	<b><?php echo Html::encode($data->getAttributeLabel('IDInspecao')); ?>:</b>
	<?php echo Html::link(Html::encode($data->IDInspecao), array('view', 'id'=>$data->IDInspecao)); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDProjetoArtefato')); ?>:</b>
	<?php echo Html::encode($data->IDProjetoArtefato); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDInspetor')); ?>:</b>
	<?php echo Html::encode($data->IDInspetor); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDModerador')); ?>:</b>
	<?php echo Html::encode($data->IDModerador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDTecnicaInspecao')); ?>:</b>
	<?php echo Html::encode($data->IDTecnicaInspecao); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('observacao')); ?>:</b>
	<?php echo Html::encode($data->observacao); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('dt_cadastro')); ?>:</b>
	<?php echo Html::encode($data->dt_cadastro); ?>
	<br />

	<?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('finalizado')); ?>:</b>
	<?php echo Html::encode($data->finalizado); ?>
	<br />

	*/ ?>

</div>