<?php
/* @var $this InspecaoController */
/* @var $model Inspecao */

/* @var $tecnicaInspecaos TecnicaInspecao */
/* @var $inspetores Inspetor */
/* @var $projetos ProjetoArtefato */

?>


<?php $this->renderPartial('_form', array(
    'model' => $model,
    'projetos' => $projetos,
    'inspetores' => $inspetores,
    'tecnicaInspecaos' => $tecnicaInspecaos,
)); ?>