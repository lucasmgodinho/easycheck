<?php
/* @var $model Inspecao */
/* @var $form CActiveForm */

/* @var $tecnicaInspecaos TecnicaInspecao */
/* @var $inspetores Inspetor */
/* @var $projetos ProjetoArtefato */

//HTexto::printMe($inspetores);

?>

<div class="tab-pane active" id="tab_info">
    <div class="tabbable tabbable-custom tabs-left">
        <br>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDProjeto', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo
                $form->dropDownList(
                    $model,
                    'IDProjeto',
                    Html::listData(
                        $projetos,
                        'IDProjeto',
                        'nome_projeto'
                    ),
                    array(
                        'class' => 'select2 span3',
                        'placeholder' => 'Escolha um Projeto',
                    )
                )
                ?>
                <?php echo $form->error($model, 'IDProjeto', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDProjetoArtefato', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo
                $form->dropDownList(
                    $model,
                    'IDProjetoArtefato',
                    [''=>''],
                    array(
                        'class' => 'select2 span3',
                        'disabled' => $model->isNewRecord,
                        'placeholder' => 'Selecione o Artefato do Projeto',
                    )
                )
                ?>
                <?php echo $form->error($model, 'IDProjetoArtefato', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDTecnicaInspecao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo
                $form->dropDownList(
                    $model,
                    'IDTecnicaInspecao',
                    Html::listData(
                        $tecnicaInspecaos,
                        'IDTecnicaInspecao',
                        'nome_tecnica'
                    ),
                    array(
                        'class' => 'select2 span3',
                        'placeholder' => 'Escolha uma Técnica de Inspeção',
                    )
                )
                ?>
                <?php echo $form->error($model, 'IDTecnicaInspecao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDInspetor', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo
                $form->dropDownList(
                    $model,
                    'IDInspetor',
                    Html::listData(
                        $inspetores,
                        'IDInspetor',
                        'LabelNomeInspetor'
                    ),
                    array(
                        'class' => 'select2 span3',
                        'placeholder' => 'Escolha os Inspetores',
                        'multiple' => true,
                    )
                )
                ?>
                <?php echo $form->error($model, 'IDInspetor', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <br>

        <fieldset class="control-group">
            <legend>&nbsp;<?php echo $form->labelEx($model, 'observacao', array('class' => 'control-label', 'style' => 'font-size: 15px')); ?></legend>

            <div class="div-textarea-container">
                <?php echo $form->textArea(
                    $model,
                    'observacao',
                    array(
                        'rows' => 6,
                        'cols' => 50,
                        'class' => 'm-wrap span6'
                    )
                ); ?>
                <?php echo $form->error($model, 'observacao', array('class' => 'help-inline')); ?>
            </div>
        </fieldset>
    </div>

</div>


