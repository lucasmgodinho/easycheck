<?php
/* @var $model Inspecao */
/* @var $form CActiveForm */

?>

<div class="tab-pane" id="tab_itensChecklist">
    <div class="tabbable tabbable-custom tabs-left">
        <p id="info-tecnica" class="note note-info-fillout hidden">
            Desative <b>itens</b> não obrigatórios dos checklists da técnica de inspeção escolhida.
        </p>

        <p id="warning-tecnica" class="note note-danger-fillout">
            É necessário escolher uma <b>técnica de inspeção</b> para acessar a customização.
        </p>

        <div id="itemChecklists" class="control-group"></div>

        <br>
    </div>
</div>


