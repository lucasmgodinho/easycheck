<?php
/* @var $this InspecaoController */
/* @var $model Inspecao */

if (Yii::app()->user->papel == 'moderador') {
?>

<p class="note note-warning">
Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada pesquisa.
</p>

<?php $this->widget('application.widgets.grid.GridView', array(
	'id'=>'inspecao-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
            'selectableRows' => 2,
	'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
        array(
            'name' => 'nome_inspetor',
            'value' => '$data->iDInspetor',
            'type' => 'text',
        ),

        array(
            'name' => 'nome_projeto',
            'value' => '$data->iDProjetoArtefato->iDProjeto->nome_projeto',
            'type' => 'text',
        ),
        array(
            'name' => 'nome_tecnica',
            'type' => 'raw',
            'value' => '$data->viewTecnicaInspecao()',
        ),
        array(
            'name' => 'Artefato',
            'type' => 'raw',
            'value' => '$data->viewArtefato()',
            'filter' => '',
        ),
        array(
            'name' => 'Situação',
            'type' => 'raw',
            'value' => '$data->getSituacaoInspecao()',
            'filter' => '',
        ),
		/*
		'IDModerador',
		'IDTecnicaInspecao',
		'observacao',
		'dt_cadastro',
		*/
            array(
                 'class' => 'ButtonColumn',
                 'template' => '{view}{delete}',
                 'buttons' =>
                 array(
                      'view' => array(
                           'visible' => 'true'
                      ),
                      'delete' => array(
                           'visible' => 'true'
                      ),
                 ),
            ),
	),
));

} else {?>
<br>
    <?php
    $this->widget('application.widgets.grid.GridView', array(
        'id'=>'inspecao-grid',
        'addNew' => false,
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'selectableRows' => 2,
        'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            array(
                'name' => 'dt_cadastro',
                'value' => 'HData::dateTimeSqlToDateBr($data->dt_cadastro)',
                'type' => 'raw',
                'filter' => '',
            ),
            array(
                'name' => 'nome_moderador',
                'value' => '$data->iDModerador->iDInspetor',
                'type' => 'text',
                'filter' => '',
            ),
            array(
                'name' => 'nome_projeto',
                'value' => '$data->iDProjetoArtefato->iDProjeto->nome_projeto',
                'type' => 'text',
            ),
            array(
                'name' => 'nome_tecnica',
                'type' => 'raw',
                'value' => '$data->viewTecnicaInspecao()',
            ),
            array(
                'name' => 'Artefato',
                'type' => 'raw',
                'value' => '$data->viewArtefato()',
                'filter' => '',
            ),
            array(
                'name' => 'Situação',
                'type' => 'raw',
                'value' => '$data->getSituacaoInspecao()',
                'filter' => '',
            ),
            /*
            'IDModerador',
            'IDTecnicaInspecao',
            'observacao',
            'dt_cadastro',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{exportarCSV}{aplicar}',
                'buttons' =>
                    array(
                        'exportarCSV' => array(
                            'options' => array(
                                'class' => 'botao',
                                'title' => 'Exportar arquivo em csv'
                            ),
                            'visible' => '$data->finalizado ? true : false',
                            'imageUrl' => false,
                            'label' => '"<i class=\'icon-download icon-large\'> Exportar .csv</i>"',
                            'url' => 'Yii::app()->createUrl("Inspecao/inspecao/ExportarCSV", array("id"=>$data->primarykey))',
                        ),
                        'aplicar' => array(
                            'options' => array(
                                'class' => 'botao botao-success',
                                'title' => 'Aplicar Técnica'
                            ),
                            'visible' => '$data->finalizado ? false : true',
                            'imageUrl' => false,
                            'label' => '"<i class=\'icon-pencil-square-o icon-large\'> Aplicar</i>"',
                            'url' => 'Yii::app()->createUrl("Inspecao/inspecao/aplicar", array("id"=>$data->primarykey))',
                        ),
                    ),
            ),
        ),
    ));
}
    ?>
