<?php
/* @var $this InspecaoController */
/* @var $model Inspecao */
?>
<div class="row-fluid">
    <div class="span12">
        <?php
        $checklists = $model->iDTecnicaInspecao->tecnicaInspecaoChecklists;
        foreach ($checklists as $checklist) { ?>
        <table class="table table-striped table-bordered">

            <thead>
            <tr>
                <th colspan="3" style="font-size: large; text-align: center">
                    <?= $checklist->nome_checklist ?>
                </th>
            </tr>
            <tr>
                <th colspan="3" style="text-align: center">
                    <?= $checklist->descricao_checklist ?>
                </th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($checklist->itemChecklists as $itemChecklist) {
                $itemUtilizados = $model->inspecaoItemChecklists;
                $situacaoItem = 'Desativado';
                foreach ($itemUtilizados as $item) {
                    if ($item->IDItemChecklist == $itemChecklist->IDItemChecklist) {
                        $situacaoItem = 'Ativo';
                    }
                }
                ?>
                    <tr>
                        <td style="width: 15% ;text-align: center">
                            <?= '<b>#'.$itemChecklist->numeracao_item .'</b>'?>
                        </td>
                        <td style="width: 70%">
                            <?= $itemChecklist->pergunta_item ?>
                        </td>
                        <td style="width: 15% ;text-align: center">
                            <?= $situacaoItem ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
