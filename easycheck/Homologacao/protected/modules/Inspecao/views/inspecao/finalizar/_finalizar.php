<?php
/* @var $model Inspecao */
/* @var $form CActiveForm */

?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'finalizar-inspecao-form',
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array(
        'class' => 'form-horizontal margin-0',
        'style'=>'margin-bottom: 10px;'
    )
));
?>

    <h3 class="alert-success">A inspeção foi salva com sucesso! </h3>

    <div class="row-fluid">
        <div class="span12">

            <div class="row-fluid" style="background: #EFEFEF">
                <div class="span4">
                    <label class="control-label">Tempo total gasto:</label>
                    <div class="controls">
                        <span class="text"><b><?= HData::converteMilisegundosParaTempo($model->tempo_gasto) ?></b></span>
                    </div>
                </div>
                <div class="span4"></div>
                <div class="span4"></div>
            </div>
            <legend></legend>
        </div>
    </div>

<?php if ($model->validaFinalizar()){ ?>
    <p class="note note-info-fillout">
        Todos os itens foram respondidos!
    </p>
    <div style="text-align: center; margin-top: 210px">
        <?php echo Html::submitButton(
            'Finalizar inspeção',
            array('class' =>
                'botao botao-info button-submit span3',
                'name' => 'finalizar',
                'style' => 'padding: 10px; font-size: 24px;'
            )
        ); ?>
    </div>
<?php } else { ?>
    <p class="note note-warning-fillout" style="font-size: 14px; margin-top: 10px; margin-bottom: 260px">
        Para finalizar a inspeção, todos os itens devem ser respondidos!
    </p>
<?php } ?>

    <div class="form-actions">
        <?php echo Html::link(
            'Minhas Inspeções',
            ['Inspecao/'],
            array('class' => 'botao botao-success span3', 'style' => 'padding: 10px; font-size: 24px; float: right')
        ); ?>
    </div>
<?php $this->endWidget(); ?>