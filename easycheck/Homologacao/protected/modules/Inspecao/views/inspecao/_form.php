<?php
/* @var $this InspecaoController */
/* @var $model Inspecao */
/* @var $form CActiveForm */

/* @var $tecnicaInspecaos TecnicaInspecao */
/* @var $inspetores Inspetor */
/* @var $projetos ProjetoArtefato */
?>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id'=>'inspecao-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array(
            'class' => 'form-horizontal margin-0',
            )
        )); ?>


<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/jquery-te-1.4.0.css"/>
<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery-te-1.4.0.min.js"></script>
<link rel="stylesheet" type="text/css"
      href="<?php echo ASSETS_LINK; ?>/plugins/jquery-nestable/jquery.nestable.css"/>
<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery-nestable/jquery.nestable.js"></script>

<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="tabbable tabbable-custom tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_info" data-toggle="tab">
                    <i class="icon-large icon icon-briefcase" style="color:#796A83FF"></i>
                    Informações Gerais
                </a>
            </li>
            <li>
                <a href="#tab_itensChecklist" data-toggle="tab">
                    <i class="icon-large icon icon-gears" style="color:#796A83FF"></i>
                    Customizar Técnica
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <?php
            $this->renderPartial(
                'cadastro/_informacoesGerais',
                array(
                    'form' => $form,
                    'model' => $model,
                    'projetos' => $projetos,
                    'inspetores' => $inspetores,
                    'tecnicaInspecaos' => $tecnicaInspecaos,
                )
            );
            $this->renderPartial(
                'cadastro/_itensChecklist',
                array(
                    'form' => $form,
                    'model' => $model,
                )
            );
            ?>
        </div>
    </div>
</div>


<div class="form-actions">
    <?php echo Html::submitButton($model->isNewRecord ? 'Criar  '.$model->labelModel() : 'Salvar Alterações', array('class'=>'botao')); ?>
</div>
<?php $this->renderPartial(
    'script',
    array(
        'model' => $model,
    )
); ?>
<?php $this->endWidget(); ?>
