<?php
/* @var $this InspecaoController */
/* @var $model Inspecao */

?>

<?php $this->widget('application.widgets.DetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'IDInspecao',
		'IDProjetoArtefato',
		'IDInspetor',
		'IDModerador',
		'IDTecnicaInspecao',
		'observacao' => [
			'name' => 'Observação',
			'value' => $model->observacao,
			'type' => 'html'
		],
		'dt_cadastro',
		'finalizado',
		'inspecaoTecnicaInspecao' => array(
			'label' => 'Itens da Técnica de Inspeção',
			'type' => 'raw',
			'value' => $this->renderPartial('view/_ItensInspecao', array('model' => $model), true, true)
		),
	),
)); ?>
