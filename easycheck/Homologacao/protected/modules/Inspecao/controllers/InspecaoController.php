<?php
/**
 * Controller do CRUD Inspecao
 *
 * @package base.Controllers
 */

class InspecaoController extends Controller
{

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle= 'Visualizar '.$model->labelModel().' #'.$id;
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {

        $this->pageTitle= 'Criar '.Inspecao::model()->labelModel();
        $model = new Inspecao;
        $this->performAjaxValidation($model);

        if(isset($_POST['Inspecao']))
        {
            $IDInspetores = $_POST['Inspecao']['IDInspetor'];
            $itens = $_POST['Inspecao']['Item'];

            $transaction = new HTransaction();
            $transaction->beginTransaction();
            try {
                if ($this->salvaInspecoes($IDInspetores, $itens)){
                    $transaction->commit();
                } else {
                    $transaction->rollback();
                }

            } catch (Exception $e) {
                throw $e;
            }
            $this->redirect(array('search'));
        }

        $tecnicaInspecaos = TecnicaInspecao::model()->findAll() ? : [];
        $inspetores = Inspetor::model()->findAll() ? : '';
        $projetos = Projeto::model()->findAll() ? : [];

        if ($tecnicaInspecaos){
            array_unshift($tecnicaInspecaos, "");
        }
        if ($projetos){
            array_unshift($projetos, "");
        }
        if (Yii::app()->user->papel == 'moderador') {
            $this->render('create', array(
                'model' => $model,
                'projetos' => $projetos,
                'inspetores' => $inspetores,
                'tecnicaInspecaos' => $tecnicaInspecaos,
            ));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionAplicar($id)
    {
        $this->pageTitle= 'Aplicar '.Inspecao::model()->labelModel().' #'.$id;
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if(isset($_POST['item']) && isset($_POST['tempo_gasto'])) {

            $itens = $_POST['item'];
            $discrepancias = $_POST['Discrepancia'];
            $model->tempo_gasto += $_POST['tempo_gasto'];

            $transaction = new HTransaction();
            $transaction->beginTransaction();
            try {
                if ($model->update() && $model->salvaAplicacaoInspecao($itens, $discrepancias)){
                    $transaction->commit();
                    $this->redirect(array('finalizar', 'id' => $model->primaryKey));
                } else {
                    $transaction->rollback();
                }

            } catch (Exception $e) {
                throw $e;
            }
            $this->redirect(array('search'));
        }

        $categoriaDefeitos = $model->iDTecnicaInspecao->tecnicaInspecaoCategoriaDefeitos;

        if (Yii::app()->user->papel == 'inspetor') {
            $this->render('aplicar/_aplicarInspecao',array(
                'model' => $model,
                'categoriaDefeitos' => $categoriaDefeitos,
            ));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionFinalizar($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Finalizar ' . $model->labelModel() . ' #' . $id;

        if (isset($_POST['finalizar'])) {
            $model->finalizado = true;
            if ($model->update()) {
                $this->redirect(array('search'));
            }
        }

        $this->render(
            'finalizar/_finalizar',
            array(
                'model' => $model,
            )
        );
    }

    public function actionExportarCSV($id)
    {
        $model = $this->loadModel($id);
        $info = $model->montaInfoRelatorioDiscrepancia();
        $discrepancias = $model->montaDiscrepanciasRelatorioDiscrepancia();

        CsvGenerator::generateCsv($info, $discrepancias, 'dados_exportados.csv');
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest)
            foreach ($_POST['id'] as $id)
                $this->loadModel($id)->delete();
        else
            $this->loadModel($id)->delete();
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle= 'Buscar '.Inspecao::model()->labelModel();
        if (Yii::app()->user->papel == 'inspetor'){
            $this->pageTitle= 'Minhas Inspeções';
        }

        if (isset($_GET['pageSize']))
        {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);  // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Inspecao('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Inspecao']))
            $model->attributes=$_GET['Inspecao'];

        if(Yii::app()->request->isAjaxRequest)
            $this->renderPartial('admin', array('model' => $model));
        else
            $this->render('admin', array('model' => $model));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Inspecao the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Inspecao::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Inspecao $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='inspecao-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function salvaInspecoes($IDInspetores, $itens)
    {

        foreach($IDInspetores as $IDInspetor){

            $inspecao = new Inspecao;
            $inspecao->attributes = $_POST['Inspecao'];
            $inspecao->observacao = $_POST['Inspecao']['observacao'];
            $inspecao->IDInspetor = $IDInspetor;
            $inspecao->dt_cadastro = HData::dateTimeBrToSql(HData::agora());
            $inspecao->IDModerador = Moderador::model()->findByAttributes(['IDInspetor' => YII::app()->user->IDInspetor])->IDModerador;

            if($inspecao->save()) {
                if (!$inspecao->salvaItensUtilizadosInspecao($itens)){
                    return false;
                }
            }
        }
        return true;

    }

    public function actionAjaxRetornaArtefatoPorProjeto()
    {
        if (Yii::app()->request->isAjaxRequest && $_REQUEST["IDProjeto"]) {

            $projetoArtefatos = Projeto::model()->findByPk($_REQUEST["IDProjeto"])->getProjetoArtefatos();
            if ($projetoArtefatos){
                foreach ($projetoArtefatos as $projetoArtefato) {
                    echo Html::tag(
                        'option',
                        array('value' => $projetoArtefato->primaryKey,),
                        Html::encode($projetoArtefato->nome_artefato),
                        true
                    );
                }
            }
        }
    }

    public function actionAjaxRetornaChecklistsTecnica()
    {
        if (Yii::app()->request->isAjaxRequest && $_REQUEST["IDTecnicaInspecao"]) {

            $checklistsTecnica = TecnicaInspecao::model()->findByPk($_REQUEST["IDTecnicaInspecao"])->getChecklistsTecnica();

            foreach ($checklistsTecnica as $checklist) {
                $itensChecklist = $checklist->getItensChecklist();

                echo '<table class="table table-bordered table-hover" style="width: 85%; margin: auto">';
                echo '<thead>';
                echo '<tr style="background-color: rgb(0,104,182)">';
                echo '<td colspan="3" style="color: #FFFFFF; font-size: 22px; height: 25px"><b>'. $checklist->nome_checklist .'</b></td>';
                echo '</tr>';
                echo '<tr style="background-color: rgba(1,163,144,0.07)">';
                echo '<td colspan="3">'. $checklist->descricao_checklist .'</td>';
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';

                foreach ($itensChecklist as $Item) {
                    $this->retornaLinhaItemInspecao($Item);
                }
            }
            echo '</tbody>';
            echo '</table>';

        }
    }

    public function retornaLinhaItemInspecao($Item, $desativado = ''){

        $desativado ? $tipoBotao = 'danger' : $tipoBotao = 'success';
        $bgCelula = ($tipoBotao == 'danger') ? '#f8e1e3' : '#daecde';

        $linhaItem = '<tr id="'.$Item->primarykey.'" >
                        <td style="width: 10%" >
                        <input type="hidden" name="Inspecao[Item]['.$Item->primaryKey.']" value="'.$desativado.'" />
                        <b>#' . $Item->numeracao_item .'</b></td>';

        $linhaItem .= '<td style="width: 75%; text-align: left"><p>'. $Item->pergunta_item .'</p></td>';

        if (!$Item->obrigatoriedade){
            $linhaItem .= '<td style="width: 15%; background: '.$bgCelula.'">' .

                Html::link(
                    '<i class="icon-power-off icon-large"></i>',
                    '',
                    array(
                        'class' => 'botao botao-' . $tipoBotao,
                        'title' => 'Ativar/Desativar',
                        'onclick' => 'ativarDesativarItem($(this))'
                    )
                )

                .'</td></tr>';
        } else {
            $linhaItem .= '<td style="width: 15%; background: #efefef"><b>Item Obrigatório</b></td></tr>';
        }
        echo $linhaItem;

    }

    public function actionAjaxAdicionaLinhaDiscrepancia()
    {
        if (Yii::app()->request->isAjaxRequest && $_POST["nItem"] && $_POST["IDInspecao"] && $_POST["numeroDiscrepancia"]) {

            $categoriaDefeitos = Inspecao::model()->findByPk($_POST["IDInspecao"])
                ->iDTecnicaInspecao
                ->tecnicaInspecaoCategoriaDefeitos;

            $this->renderPartial(
                'aplicar/_discrepancia',
                array(
                    'discrepancia' => new Discrepancia(),
                    'nItem' =>  $_REQUEST["nItem"],
                    'numeroDiscrepancia' =>  $_POST["numeroDiscrepancia"],
                    'categoriaDefeitos' =>  $categoriaDefeitos,
                )
            );
        }
    }

}
