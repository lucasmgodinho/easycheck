<?php

/* @var $model TecnicaInspecao */
/* @var $form CActiveForm */
?>

<div class="tab-pane" id="tab_checklist">
    <div class="tabbable tabbable-custom tabs-left">

    <?php if ($model->inspecaos){ ?>
        <p class="note note-danger-fillout">
            Existem <b>inspeções</b> utilizando esta <b>técnica de inspeção</b>, desta forma, <b>não</b> será possível editar esta parte!
        </p>
    <?php } else { ?>

        <p id="info-tabela-an" class="note note-info-fillout">
            Adicione ou remova <b>Checklists</b> e <b>Itens de Checklist</b> a esta técnica de inspeção.
        </p>

        <div id="checklists" class="control-group"></div>
        <div id="bt-adicionar-checklist">
            <button onclick="adicionaChecklist()" type="button" class="botao-large botao-info" style="display: block; margin: 0 auto;">Adicionar Checklist</button>
        </div>

        <span><i id="loadingSpinner-checklists" style="position: absolute; top: 30%; left: 48%;" class="icon-spinner icon-5x icon-spin hide"></i></span>

<?php } ?>

    </div>
</div>
