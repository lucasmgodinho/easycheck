<?php

/* @var $model TecnicaInspecao */
/* @var $form CActiveForm */

?>

<div class="tab-pane" id="tab_categoriaDefeitos">
    <div class="tabbable tabbable-custom tabs-left">

        <?php if ($model->inspecaos){ ?>
            <p class="note note-danger-fillout">
                Existem <b>inspeções</b> utilizando esta <b>técnica de inspeção</b>, desta forma, <b>não</b> será possível editar esta parte!
            </p>
        <?php } else { ?>

        <p id="info-tabela-an" class="note note-info-fillout">
            Adicione ou remova <b>categorias de defeitos</b> a esta técnica de inspeção.
        </p>

        <span><i id="loadingSpinner-categorias" style="position: absolute; top: 30%; left: 48%;" class="icon-spinner icon-5x icon-spin hide"></i></span>

        <table id="tabela-categoria-defeitos" class="table table-striped table-hover table-bordered flip-scroll" style="width: 50%; margin-left: 25%">
            <thead>
            <th style="width: 85%"><?php echo 'Tipos de Defeito'; ?></th>
            <th class="button-column"></th>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <th colspan="2"><button onclick="adicionaCategoriaDefeito()" type="button" class="botao botao-info">Adicionar</button></th>
            </tfoot>
        </table>

<?php } ?>
    </div>
</div>
