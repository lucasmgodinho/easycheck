<?php

/* @var $model TecnicaInspecao */
/* @var $form CActiveForm */

?>

<div class="tab-pane active" id="tab_info">
    <div class="tabbable tabbable-custom tabs-left">
        <br>

        <div class="control-group">
            <?php echo $form->labelEx($model,'nome_tecnica',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'nome_tecnica',array('class'=>'m-wrap span3','maxlength'=>255)); ?>
                <?php echo $form->error($model,'nome_tecnica',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'nome_autor',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'nome_autor',array('class'=>'m-wrap span3','maxlength'=>255)); ?>
                <?php echo $form->error($model,'nome_autor',array('class'=>'help-inline')); ?>
            </div>
        </div>

        <fieldset class="control-group">
            <legend>&nbsp;<?php echo $form->labelEx($model, 'definicao_tecnica', array('class' => 'control-label', 'style' => 'font-size: 15px')); ?></legend>

        <div class="div-textarea-container">
            <?php echo $form->textArea(
                $model,
                'definicao_tecnica',
                array(
                    'rows' => 6,
                    'cols' => 50,
                    'class' => 'm-wrap span6'
                )
            ); ?>
            <?php echo $form->error($model, 'definicao_tecnica', array('class' => 'help-inline')); ?>
        </div>
        </fieldset>

        <fieldset class="control-group">
            <legend>&nbsp;<?php echo $form->labelEx($model, 'descricao_tecnica', array('class' => 'control-label', 'style' => 'font-size: 15px')); ?></legend>

            <div class="div-textarea-container">
                <?php echo $form->textArea(
                    $model,
                    'descricao_tecnica',
                    array(
                        'rows' => 6,
                        'cols' => 50,
                        'class' => 'm-wrap span6'
                    )
                ); ?>
                <?php echo $form->error($model, 'descricao_tecnica', array('class' => 'help-inline')); ?>
            </div>
        </fieldset>

        <fieldset class="control-group">
            <legend style="font-size: medium">&nbsp;Documentos</legend>

            <?php if ($model->inspecaos){ ?>
                <p class="note note-danger-fillout">
                    Existem <b>inspeções</b> utilizando esta <b>técnica de inspeção</b>, desta forma, <b>não</b> será possível editar esta parte!
                </p>
            <?php } else { ?>

            <table id="tabela-tecnica-docuementos" class="table table-striped table-hover table-bordered flip-scroll" style="width: 80%; margin-left: 10%">
                <thead>
                <th style="width: 50%"><?php echo 'Nome do Documento'; ?></th>
                <th style="width: 40%"><?php echo 'Arquivo'; ?></th>
                <th class="button-column"></th>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <th colspan="3"><button onclick="adicionaDocumento()" type="button" class="botao botao-info"">Adicionar Documento</button></th>
                </tfoot>
            </table>

            <?php } ?>

        </fieldset>
    </div>
</div>


