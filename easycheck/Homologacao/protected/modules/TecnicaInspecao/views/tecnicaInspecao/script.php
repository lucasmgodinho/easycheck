<?php
/* @var $model TecnicaInspecao */
?>
<script>

    var nomeDocumento = $('#<?= Html::activeId($model, "nome_documento") ?>');
    var ducomentoTecnica = $('#<?= Html::activeId($model, "documento_tecnica") ?>');

    var quantidadeDocumentosCadastrados = <?php echo json_encode($model->quantidadeDocumentos); ?>;
    var quantidadeDocumentos = 0 + quantidadeDocumentosCadastrados;

    var quantidadeCategoriasCadastrados = <?php echo json_encode($model->quantidadeCategoriasDefeito); ?>;
    var quantidadeCategoriasDefeito = 0 + quantidadeCategoriasCadastrados;

    var quantidadeChecklistsCadastrados = <?php echo json_encode($model->quantidadeChecklists); ?>;
    var quantidadeChecklists = 0 + quantidadeCategoriasCadastrados;


    var quantidadeItensChecklist = 0;


    if (<?php echo json_encode($model->isNewRecord); ?>){
        quantidadeCategoriasDefeito = 5;
    }

    $(document).ready(function() {
        $('textarea').jqte(configuracaoTextEditorSimple);
        carregaDocumentos();
        carregaCategoriasDefeito();
        carregaChecklists();
    });


    function carregaDocumentos(){

        var IDTecnicaInspecao = <?php echo json_encode($model->IDTecnicaInspecao); ?>;
        if (IDTecnicaInspecao) {
            $.ajax({
                url: "<?= $this->createUrl('ajaxCarregaTabelaDocumentosTecnica') ?>",
                type: 'POST',
                dataType: 'text',
                data: {
                    IDTecnicaInspecao: IDTecnicaInspecao,
                },
            }).done(function (body) {
                if (body) {
                    $("#tabela-tecnica-docuementos").append(body);
                }
            });
        }
    }

    function adicionaDocumento(){

        $.ajax({
            url: "<?= $this->createUrl('ajaxAdicionaDocumentosTecnica') ?>",
            type: 'POST',
            dataType: 'text',
            data: {
                quantidadeDocumentos: quantidadeDocumentos,
            },
        }).done(function (body) {
            if (body) {
                $("#tabela-tecnica-docuementos").append(body);
                quantidadeDocumentos++;
            }
        });
    }

    function carregaChecklists(){
        var IDTecnicaInspecao = <?php echo json_encode($model->IDTecnicaInspecao); ?>;
        if (IDTecnicaInspecao) {
            $.ajax({
                url: "<?= $this->createUrl('ajaxCarregaChecklists') ?>",
                type: 'POST',
                dataType: 'text',
                data: {
                    IDTecnicaInspecao: IDTecnicaInspecao,
                },
                beforeSend: function () {
                    $("#loadingSpinner-checklists").removeClass('hide');
                    $("#checklists").addClass('hide');
                    $("#bt-adicionar-checklist").addClass('hide');
                }
            }).done(function (data) {
                if (data) {
                    $("#bt-adicionar-checklist").removeClass('hide');
                    $("#loadingSpinner-checklists").addClass('hide');
                    var novoChecklist = $(data).hide();
                    $("#checklists").removeClass('hide').append(novoChecklist);
                    novoChecklist.fadeIn(500);
                }
            });
        }
    }

    function carregaCategoriasDefeito(){
        var IDTecnicaInspecao = <?php echo json_encode($model->IDTecnicaInspecao); ?>;
        $.ajax({
            url: "<?= $this->createUrl('ajaxCarregaCategoriaDefeitos') ?>",
            type: 'POST',
            dataType: 'text',
            data: {
                IDTecnicaInspecao : IDTecnicaInspecao,
            },
            beforeSend: function (){
                $("#loadingSpinner-categorias").removeClass('hide');
                $("#tabela-categoria-defeitos").addClass('hide');
            }
        }).done(function(body) {
            if (body) {
                $("#tabela-categoria-defeitos").append(body);
                $("#loadingSpinner-categorias").addClass('hide');
                $("#tabela-categoria-defeitos").removeClass('hide');
            }
        });
    }

    function adicionaCategoriaDefeito(){
        $.ajax({
            url: "<?= $this->createUrl('ajaxAdicionaCategoriaDefeito') ?>",
            type: 'POST',
            dataType: 'text',
            data: {
                quantidadeCategoriasDefeito: quantidadeCategoriasDefeito,
            },
        }).done(function (body) {
            if (body) {
                $("#tabela-categoria-defeitos").append(body);
                quantidadeCategoriasDefeito++;
            }
        });
    }

    function adicionaChecklist(){

        $.ajax({
            url: "<?= $this->createUrl('ajaxAdicionaChecklist') ?>",
            type: 'POST',
            dataType: 'text',
            data: {
                quantidadeChecklists: quantidadeChecklists,
            },
        }).done(function(data) {
            if (data) {
                var novoChecklist = $(data).hide();
                $("#checklists").append(novoChecklist);
                novoChecklist.fadeIn(500);
                quantidadeChecklists++
            }
        });

    }

    function adicionaItemChecklist(nItem, nChecklist){

        $.ajax({
            url: "<?= $this->createUrl('ajaxAdicionaItemChecklist') ?>",
            type: 'POST',
            dataType: 'text',
            data: {
                quantidadeItensChecklist: quantidadeItensChecklist,
                nChecklist: nChecklist,
            },
        }).done(function(data) {
            if (data) {
                var novoItem = $(data).hide();
                $("#body-checklist-" + nChecklist).append(novoItem);
                novoItem.fadeIn(350);

                quantidadeItensChecklist++
                renumerarTodasLinhas();
            }
        });

    }

    function renumerarTodasLinhas() {
        var tabelas = document.querySelectorAll('[id^=checklist-]');
        var contagem = 1;

        for (var i = 0; i < tabelas.length; i++) {
            var tabela = tabelas[i];
            var linhas = tabela.getElementsByTagName('tr');

            for (var j = 1; j < linhas.length; j++) {
                var celula = linhas[j].getElementsByTagName('td')[0];

                if (celula) {
                    var nChecklist = tabela.id.match(/\d+/)[0];
                    var nItem = linhas[j].id.match(/\d+/)[0];
                    celula.innerHTML = '<input type="hidden" name="Checklist['+ nChecklist +'][itens][' + nItem + '][numeracao_item]"' +
                        ' value="' + contagem +'"><b>#' + contagem + '</b>';

                    contagem++
                }
            }
        }
    }

    function marcaResposta(item, resposta){

        if (resposta === 'sim'){
            $('#botao-sim-' + item).addClass('botao-success')
            $('#botao-nao-' + item).removeClass('botao-danger')
            $('#resposta-' + item).attr('value', 'sim')

        } else {
            $('#botao-nao-' + item).addClass('botao-danger')
            $('#botao-sim-' + item).removeClass('botao-success')
            $('#resposta-' + item).attr('value', 'nao')
        }
    }


    function removerDocumento(e){
        $('#documentoTecnica_' + e).fadeOut(300, function() {
            $('#documentoTecnica_' + e).remove();
        });
    }

    function removerCategoriaDefeito(e){
        $('#categoriaDefeito_' + e).fadeOut(300, function() {
            $('#categoriaDefeito_' + e).remove();
        });
    }

    function removerChecklist(e){
        var removerChecklist = confirm("Deseja remover este checklist?");
        if(removerChecklist) {
            $('#checklist-' + e).fadeOut(300, function () {
                $('#checklist-' + e).remove();
                renumerarTodasLinhas();
            });
        }
    }

    function removerItemChecklist(e){
        $('#linhaItem_' + e).fadeOut(300, function() {
            $('#linhaItem_' + e).remove();
            renumerarTodasLinhas();
        });
    }

    $('#submeter').click(function (e) {
        var tecnicaUtilizada = <?php echo json_encode((bool)$model->inspecaos); ?>;

            e.preventDefault();
            var error = false;

            var nomeTecnica = $('#<?php echo Html::activeId($model, 'nome_tecnica') ?>').val();
            var nomeAutor = $('#<?php echo Html::activeId($model, 'nome_autor') ?>').val();
            var definicao = $('#<?php echo Html::activeId($model, 'definicao_tecnica') ?>').val();
            var tabelasChecklist = document.querySelectorAll('[id^=checklist-]');
            var linhasItem = document.querySelectorAll('[id^=linhaItem_]');
            var tiposDefeito = document.querySelectorAll('[id^=categoriaDefeito_]');
            var titulosChecklistPreenchidos = true;
            var itemPreenchidos = true;
            var algumChecklistSemItens = false;

        tabelasChecklist.forEach(function (tabela) {
            var linhas = tabela.querySelectorAll('[id^=linhaItem_]');
            if (linhas.length === 0) {
                algumChecklistSemItens = true;
            }
        });

        tabelasChecklist.forEach(function (tabela) {
            var inputs = tabela.querySelectorAll('input');
            console.log(tabelasChecklist)
            for (var i = 0; i < inputs.length; i++) {
                if (!inputs[i].value) {
                    titulosChecklistPreenchidos = false;
                }
            }
        });

            tabelasChecklist.forEach(function (tabela) {
                var inputs = tabela.querySelectorAll('input');
                for (var i = 0; i < inputs.length; i++) {
                    if (!inputs[i].value) {
                        titulosChecklistPreenchidos = false;
                    }
                }
            });

            linhasItem.forEach(function (linha) {
                var textarea = linha.querySelector('textarea');

                if (!textarea.value.trim()) {
                    itemPreenchidos = false;
                }
            });

        if (tecnicaUtilizada === false) {
            if (algumChecklistSemItens) {
                displayFlashes({'error': ["Existe pelo menos um Checklist sem itens. Preencha todos os itens do Checklist"]});
                error = true;
            }

            if (titulosChecklistPreenchidos === false) {
                displayFlashes({'error': ["É necessário preencher todos os títulos dos Checklists"]});
                error = true;
            }

            if (itemPreenchidos === false) {
                displayFlashes({'error': ["É necessário preencher todos os Itens dos Checklists"]});
                error = true;
            }

            if (tabelasChecklist.length === 0) {
                displayFlashes({'error': ["É necessário adicionar ao menos um Checklist"]});
                error = true;
            }

            if (linhasItem.length === 0) {
                displayFlashes({'error': ["É necessário adicionar ao menos um Item ao Checklist"]});
                error = true;
            }

            if (tiposDefeito.length === 0) {
                displayFlashes({'error': ["É necessário adicionar ao menos um tipo de defeito"]});
                error = true;
            }

            if (nomeTecnica == "" || typeof nomeTecnica == "undefined" || nomeTecnica == null) {
                displayFlashes({'error': ["O nome da técnica não pode der vazio"]});
                error = true;
            }
            if (nomeAutor == "" || typeof nomeAutor == "undefined" || nomeAutor == null) {
                displayFlashes({'error': ["O nome do autor da técnica não pode ser vazio"]});
                error = true;
            }
            if (definicao == "" || typeof definicao == "undefined" || definicao == null) {
                displayFlashes({'error': ["A definição da técnica não pode ser vazia"]});
                error = true;
            }
        } else {
            if (nomeTecnica == "" || typeof nomeTecnica == "undefined" || nomeTecnica == null) {
                displayFlashes({'error': ["O nome da técnica não pode der vazio"]});
                error = true;
            }
            if (nomeAutor == "" || typeof nomeAutor == "undefined" || nomeAutor == null) {
                displayFlashes({'error': ["O nome do autor da técnica não pode ser vazio"]});
                error = true;
            }
            if (definicao == "" || typeof definicao == "undefined" || definicao == null) {
                displayFlashes({'error': ["A definição da técnica não pode ser vazia"]});
                error = true;
            }
        }

            if (!error) {
                $('#tecnica-inspecao-form').submit();
            } else {
                mantemNomeLabelSubmit()
                return false;
            }
    });

    function mantemNomeLabelSubmit(){
        $('#submeter').val("<?php echo $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações'?>");
    }

</script>