<?php
/* @var $this TecnicaInspecaoController */
/* @var $data TecnicaInspecao */
?>

<div class="view">

    	<b><?php echo Html::encode($data->getAttributeLabel('IDTecnicaInspecao')); ?>:</b>
	<?php echo Html::link(Html::encode($data->IDTecnicaInspecao), array('view', 'id'=>$data->IDTecnicaInspecao)); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDModerador')); ?>:</b>
	<?php echo Html::encode($data->IDModerador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('nome_tecnica')); ?>:</b>
	<?php echo Html::encode($data->nome_tecnica); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('nome_autor')); ?>:</b>
	<?php echo Html::encode($data->nome_autor); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('descricao_tecnica')); ?>:</b>
	<?php echo Html::encode($data->descricao_tecnica); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('definicao_tecnica')); ?>:</b>
	<?php echo Html::encode($data->definicao_tecnica); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('dt_cadastro')); ?>:</b>
	<?php echo Html::encode($data->dt_cadastro); ?>
	<br />


</div>