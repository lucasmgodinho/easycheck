<?php
/* @var $this TecnicaInspecaoController */
/* @var $model TecnicaInspecao */

?>


<?php $this->widget('application.widgets.DetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'IDTecnicaInspecao',
		'IDModerador' => array(
			'label' => $model->attributeLabels()['IDModerador'],
			'value' =>  $model->iDModerador,
		),
		'nome_tecnica',
		'nome_autor',
		'definicao_tecnica' => array(
			'label' => $model->attributeLabels()['definicao_tecnica'],
			'value' =>  $model->definicao_tecnica,
			'type' => 'html'
		),
		'descricao_tecnica' => array(
			'label' => $model->attributeLabels()['descricao_tecnica'],
			'value' =>  $model->descricao_tecnica,
			'type' => 'html'
		),
		'documento_tecnica' => array(
			'label' => $model->attributeLabels()['documento_tecnica'],
			'value' =>  $model->viewDocumentosTecnica(),
			'type' => 'html'
		),
		'dt_cadastro' => array(
			'label' => $model->attributeLabels()['dt_cadastro'],
			'value' =>  $model->dt_cadastro,
			'type' => 'date'
		),
	),
)); ?>
