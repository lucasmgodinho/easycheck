<?php
/* @var $this TecnicaInspecaoController */
/* @var $model TecnicaInspecao */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'IDTecnicaInspecao'); ?>
            <?php echo $form->textField($model,'IDTecnicaInspecao',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDModerador'); ?>
            <?php echo $form->textField($model,'IDModerador',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'nome_tecnica'); ?>
            <?php echo $form->textField($model,'nome_tecnica',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'nome_autor'); ?>
            <?php echo $form->textField($model,'nome_autor',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'descricao_tecnica'); ?>
            <?php echo $form->textField($model,'descricao_tecnica',array('class'=>'m-wrap span6','maxlength'=>2000)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'definicao_tecnica'); ?>
            <?php echo $form->textField($model,'definicao_tecnica',array('class'=>'m-wrap span6','maxlength'=>2000)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_cadastro'); ?>
            <?php echo $form->textField($model,'dt_cadastro',array('class'=>'m-wrap span6')); ?>
        </div>

        <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->