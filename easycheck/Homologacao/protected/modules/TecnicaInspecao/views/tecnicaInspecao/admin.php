<?php
/* @var $this TecnicaInspecaoController */
/* @var $model TecnicaInspecao */

?>

<p class="note note-warning">
Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada pesquisa.
</p>


<?php $this->widget('application.widgets.grid.GridView', array(
	'id'=>'tecnica-inspecao-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
            'selectableRows' => 2,
	'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
		'nome_tecnica',
		'nome_autor',
		/*
		'dt_cadastro',
		'descricao_tecnica',
		'definicao_tecnica',
		*/
            array(
                 'class' => 'ButtonColumn',
                 'template' => '{view}{update}{delete}',
                 'buttons' =>
                 array(
                      'view' => array(
                           'visible' => 'true'
                      ),
                      'update' => array(
                           'visible' => 'true'
                      ),
                      'delete' => array(
                           'visible' => '$data->inspecaos ? false : true'
                      ),
                 ),
            ),
	),
)); ?>
