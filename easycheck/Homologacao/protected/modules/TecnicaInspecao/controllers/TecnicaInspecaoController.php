<?php
/**
 * Controller do CRUD TecnicaInspecao
 *
 * @package base.Controllers
 */

class TecnicaInspecaoController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;
    private $nItem = 1;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle= 'Visualizar '.$model->labelModel().' #'.$id;
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle= 'Criar '.TecnicaInspecao::model()->labelModel();
        $model=new TecnicaInspecao;

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['TecnicaInspecao']))
        {

            $documentos = [];
            $checklists = $_POST['Checklist'] ? : [];
            $categoriaDefeitos = $_POST['categoria_defeito'] ? : [];

            if ($_FILES['documento_tecnica']){
                foreach ($_POST['nome_documento'] as $key => $nome) {
                    $documentos['novos'][$key]['nome_documento'] = $nome;
                    $documentos['novos'][$key]['tmp_name'] = $_FILES['documento_tecnica']['tmp_name'][$key];
                }
            }

            $model->attributes=$_POST['TecnicaInspecao'];
            $model->IDModerador = Moderador::model()->findByAttributes(['IDInspetor' => YII::app()->user->IDInspetor])->IDModerador;
            $model->dt_cadastro = HData::dateTimeBrToSql(HData::agora());

            $transaction = new HTransaction();
            $transaction->beginTransaction();

            if($model->save()){
                try {
                    if ($model->salvaInformacoesTecnica($documentos, $categoriaDefeitos, $checklists)){
                        $transaction->commit();
                    } else {
                        $transaction->rollback();
                    }

                } catch (Exception $e) {
                    throw $e;
                }
                $this->redirect(array('view','id'=>$model->IDTecnicaInspecao));
            }
        }
        if (Yii::app()->user->papel == 'moderador') {
            $this->render('create',array(
                'model'=>$model,
            ));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle= 'Editar '.TecnicaInspecao::model()->labelModel().' #'.$id;
        $model=$this->loadModel($id);


        $this->performAjaxValidation($model);

        if(isset($_POST['TecnicaInspecao']))
        {

            $documentos = [];
            $checklists = $_POST['Checklist'] ? : [];
            $categoriaDefeitos = $_POST['categoria_defeito'] ? : [];


            if ($_FILES['documento_tecnica']){
                foreach ($_POST['nome_documento'] as $key => $nome) {
                    $documentos['novos'][$key]['nome_documento'] = $nome;
                    $documentos['novos'][$key]['tmp_name'] = $_FILES['documento_tecnica']['tmp_name'][$key];
                }
            }

            if ($_POST['documento_tecnica']) {
                foreach ($_POST['documento_tecnica'] as $nome => $pdf) {
                    $documentos['cadastrados'][$nome] = $pdf;
                }
            }

            $model->attributes=$_POST['TecnicaInspecao'];

            $transaction = new HTransaction();
            $transaction->beginTransaction();


            if($model->save()){
                try {
                    //caso a técnica esteja sendo utilizada em alguma inspeção
                    if ($model->inspecaos){
                        if ($model->update()){
                            $transaction->commit();
                        } else {
                            $transaction->rollback();
                        }
                    } else {
                        if ($model->salvaInformacoesTecnica($documentos, $categoriaDefeitos, $checklists)) {
                            $transaction->commit();
                        } else {
                            $transaction->rollback();
                        }
                    }
                } catch (Exception $e) {
                    throw $e;
                }
                $this->redirect(array('view','id'=>$model->IDTecnicaInspecao));
            }
        }
        if (Yii::app()->user->papel == 'moderador') {
            $this->render('update', array(
                'model' => $model,
            ));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest)
            foreach ($_POST['id'] as $id)
                $this->loadModel($id)->delete();
        else
            $this->loadModel($id)->delete();
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle= 'Buscar '.TecnicaInspecao::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize']))
        {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);  // unseta para evitar atribuições futuras desnecessárias;
        }
        $model=new TecnicaInspecao('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['TecnicaInspecao']))
            $model->attributes=$_GET['TecnicaInspecao'];

        if (Yii::app()->user->papel == 'moderador') {

            if(Yii::app()->request->isAjaxRequest)
                $this->renderPartial('admin', array('model' => $model));
            else
                $this->render('admin', array('model' => $model));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return TecnicaInspecao the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=TecnicaInspecao::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param TecnicaInspecao $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='tecnica-inspecao-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjaxAdicionaDocumentosTecnica()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $i = $_REQUEST['quantidadeDocumentos'];

            echo '<tr id="documentoTecnica_' . $i .'">';
            echo '<td><input id="nome_artefato_'. $i .'" name="nome_documento['. $i .']" autocomplete="off" class="m-wrap span6"></td>';
            echo '<td><input type="file" name="documento_tecnica['. $i .']" accept="application/pdf"></td>';
            echo '<td>'.
                Html::link(
                    '<i class="icon-trash-o icon-large"></i>',
                    '',
                    array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerDocumento('. $i .')')
                )
                .'</td></tr>';
        }
    }

    public function actionAjaxCarregaTabelaDocumentosTecnica()
    {
        $quantidadeDocumentos = 0;

        if (Yii::app()->request->isAjaxRequest) {
            if (!empty($_REQUEST['IDTecnicaInspecao'])) {
                $tecnicaInspecao = TecnicaInspecao::model()->findByPk($_REQUEST['IDTecnicaInspecao']);
                if ($tecnicaInspecao) {
                    $documentos = $tecnicaInspecao->getDocumentosTecnica();
                    foreach ($documentos as $documento) {
                        $this->retornaLinhaDocumentoTecnica($quantidadeDocumentos, $documento);
                        $quantidadeDocumentos++;
                    }
                }
            }
        }
    }

    public function retornaLinhaDocumentoTecnica($i, $documento){

        $nomeDocumento = $documento->nome_documento ? : '';
        $documentoTecnica = stream_get_contents($documento->documento_tecnica) ? : '';

        echo '<tr id="documentoTecnica_' . $i .'">';
        echo '<td><input id="nome_artefato_'. $i .'" name="nome_artefato['. $i .']" autocomplete="off" class="m-wrap span6" value="' . $nomeDocumento . '" disabled></td>';
        echo '<td>
                <input type="hidden" name="documento_tecnica['. $nomeDocumento .']" value="'. $documentoTecnica .'">
                <a target="_blank" href="'. Yii::app()->createUrl("TecnicaInspecao/tecnicaInspecao/printDocumento", array("id"=>$documento->primaryKey)) .'">Visualizar arquivo</a></td>';
        echo '<td>'.
            Html::link(
                '<i class="icon-trash-o icon-large"></i>',
                '',
                array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerDocumento('. $i .')')
            )
            .'</td></tr>';
    }

    public function actionPrintDocumento($id)
    {
        $documento = TecnicaInspecaoDocumento::model()->findByPk($id);

        $pdf = '';

        if (!is_null($documento->documento_tecnica)) {
            $pdf = base64_decode(stream_get_contents($documento->documento_tecnica));
        }

        header('Content-Type: application/pdf;');
        header('Content-Transfer-Encoding: binary');

        echo $pdf;

        Yii::app()->end();
    }

    public function actionAjaxCarregaChecklists()
    {
        $quantidadeChecklits = 0;
        if (Yii::app()->request->isAjaxRequest && !empty($_REQUEST['IDTecnicaInspecao'])) {

            $tecnicaInspecao = TecnicaInspecao::model()->findByPk($_REQUEST['IDTecnicaInspecao']);
            if ($tecnicaInspecao) {
                $checklists = $tecnicaInspecao->getChecklistsTecnica();
                foreach ($checklists as $checklist) {
                    $this->retornaTabelaChecklist($quantidadeChecklits, $checklist);
                    $quantidadeChecklits++;
                }
            }
        }
    }

    public function actionAjaxCarregaCategoriaDefeitos()
    {
        $quantidadeCategorias = 0;

        if (Yii::app()->request->isAjaxRequest) {
            if (!empty($_REQUEST['IDTecnicaInspecao'])) {
                $tecnicaInspecao = TecnicaInspecao::model()->findByPk($_REQUEST['IDTecnicaInspecao']);
                if ($tecnicaInspecao){
                    $categoriasDefeito = $tecnicaInspecao->getCategoriasDefeitoTecnica();
                    foreach ($categoriasDefeito as $categoria){
                        $this->retornaLinhaCategoriaDefeito($quantidadeCategorias, $categoria);
                        $quantidadeCategorias++;
                    }
                }
            } else {
                echo '<tr id="categoriaDefeito_0">';
                echo '<td><input name="categoria_defeito[0]" autocomplete="off" class="m-wrap span4" style="text-align: center" value="Omissão" readonly></td>';
                echo '<td>'. Html::link(
                        '<i class="icon-trash-o icon-large"></i>',
                        '',
                        array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerCategoriaDefeito('. 0 .')')
                    )
                    .'</td></tr>';

                echo '<tr id="categoriaDefeito_1">';
                echo '<td><input name="categoria_defeito[1]" autocomplete="off" class="m-wrap span4" style="text-align: center" value="Fato incorreto" readonly></td>';
                echo '<td>'. Html::link(
                        '<i class="icon-trash-o icon-large"></i>',
                        '',
                        array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerCategoriaDefeito('. 1 .')')
                    )
                    .'</td></tr>';

                echo '<tr id="categoriaDefeito_2">';
                echo '<td><input name="categoria_defeito[2]" autocomplete="off" class="m-wrap span4" style="text-align: center" value="Inconsistência" readonly></td>';
                echo '<td>'. Html::link(
                        '<i class="icon-trash-o icon-large"></i>',
                        '',
                        array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerCategoriaDefeito('. 2 .')')
                    )
                    .'</td></tr>';

                echo '<tr id="categoriaDefeito_3">';
                echo '<td><input name="categoria_defeito[3]" autocomplete="off" class="m-wrap span4" style="text-align: center" value="Ambiguidade" readonly></td>';
                echo '<td>'. Html::link(
                        '<i class="icon-trash-o icon-large"></i>',
                        '',
                        array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerCategoriaDefeito('. 3 .')')
                    )
                    .'</td></tr>';

                echo '<tr id="categoriaDefeito_4">';
                echo '<td><input name="categoria_defeito[4]" autocomplete="off" class="m-wrap span4" style="text-align: center" value="Informação estranha" readonly></td>';
                echo '<td>'. Html::link(
                        '<i class="icon-trash-o icon-large"></i>',
                        '',
                        array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerCategoriaDefeito('. 4 .')')
                    )
                    .'</td></tr>';

            }
        }
    }

    public function actionAjaxAdicionaCategoriaDefeito()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $i = $_REQUEST['quantidadeCategoriasDefeito'];
            $this->retornaLinhaCategoriaDefeito($i);
        }
    }

    public function retornaLinhaCategoriaDefeito($i, $categoria = [])
    {
        $nomeCategoria = $categoria ? $categoria->categoria_defeito : '';
        $somenteLer = $categoria ? 'readonly' : '';
        echo '<tr id="categoriaDefeito_' . $i .'">';
        echo '<td><input name="categoria_defeito['. $i .']" value="'.$nomeCategoria.'" autocomplete="off" style="text-align: center" class="m-wrap span4" '.$somenteLer.'></td>';
        echo '<td>'. Html::link(
                '<i class="icon-trash-o icon-large"></i>',
                '',
                array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerCategoriaDefeito('. $i .')')
            )
            .'</td></tr>';
    }

    public function actionAjaxAdicionaChecklist()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $i = $_REQUEST['quantidadeChecklists'];
            $this->retornaTabelaChecklist($i);
        }
    }

    public function retornaTabelaChecklist($i, $checklist = [])
    {
        $nomeChecklist = $checklist ? $checklist->nome_checklist : '';
        $itens = $checklist ? $checklist->itemChecklists : [];

        echo '<div class="control-group">';
        echo '<table id="checklist-'.$i.'" class="table table-striped table-hover table-bordered">';
        echo '<thead>';

        echo '<tr>
                <th class="text-center" colspan="5" style="background-color: rgba(0,135,3,0.34)">
                <input class="m-wrap span4" placeholder="Título do Checklist" style="text-align: center; font-size: 16px;" name="Checklist['.$i.'][nome_checklist]" value="'.$nomeChecklist.'">
                '. Html::link(
                '<i class="icon-remove icon-large"></i>',
                '',
                array('class' => 'botao botao-danger', 'title' => 'Remover Checklist', 'onclick' => 'removerChecklist('. $i .')','style'=>'float: right; margin-top: 5px;')
            )
            .'</th>
              </tr>';
        echo '<tr>
                 <th style="background-color: rgba(63,153,38,0.15)" colspan="5">' . Html::textArea(
                'Checklist['.$i.'][descricao_checklist]',
                $checklist->descricao_checklist,
                ['class' => 'span8', 'maxlength' => '2000', 'placeholder' => 'Adicione uma descrição para o checklist.']).
            '</th>
              </tr>';
        echo '<tr>
                   <th style="width: 8%">Item</th>
                   <th style="width: 60%">Pergunta</th>
                   <th style="width: 15%">Resposta Esperada</th>
                   <th style="width: 10%">Obrigatório</th>
                   <th class="button-column"></th>
              </tr>';
        echo '</thead>';

        echo '<tbody id="body-checklist-'.$i.'">';

        foreach ($itens as $item) {
            $this->retornaLinhaItemChecklist($this->nItem, $i, $item);
            $this->nItem++;
        }

        echo '</tbody>';
        echo '<tfoot><th colspan="5"><button onclick="adicionaItemChecklist('.$this->nItem.','.$i.')" type="button" class="botao botao-success"">Adicionar Item</button></th></tfoot>';
    }

    public function actionAjaxAdicionaItemChecklist()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $nItem = $_REQUEST['quantidadeItensChecklist'];
            $nChecklist = $_REQUEST['nChecklist'];
            $this->retornaLinhaItemChecklist($nItem, $nChecklist);
        }
    }

    public function retornaLinhaItemChecklist($nItem, $nChecklist, $ItemChecklist = []){

        $respostaEsperada = $ItemChecklist ? ($ItemChecklist->respostaEsperada ? 'sim' : 'nao') : 'sim';
        $classeRespostaSim = $respostaEsperada == 'sim' ? 'success' : '';
        $classeRespostaNao = $respostaEsperada == 'nao' ? 'danger' : '';
        $obrigatorio = !$ItemChecklist || $ItemChecklist->obrigatoriedade == true;

        echo '<tr id="linhaItem_' . $nItem .'">';
        echo '<td><input type="hidden" name="Checklist['.$nChecklist.'][itens]['.$nItem.'][numeracao_item]" value="'.$nItem.'"><b>#' . $nItem . '</b></td>';

        //pergunta
        echo '<td>' . Html::textArea(
                'Checklist['.$nChecklist.'][itens]['.$nItem.'][pergunta_item]',
                $ItemChecklist->pergunta_item,
                ['class' => 'span12', 'maxlength' => '2000', 'placeholder' => 'Adicione a pergunta do item.']).
            '</td>';

        //resposta esperada
        echo '<td><input id="resposta-'.$nItem.'" type="hidden" name="Checklist['.$nChecklist.'][itens]['.$nItem.'][resposta-esperada]" value="'.$respostaEsperada.'">' .

            Html::link('<i class="icon-check-square"> Sim</i>', '',
                array('class' => 'botao botao-'.$classeRespostaSim,
                    'id' => 'botao-sim-'.$nItem,
                    'onclick' => 'marcaResposta("' . $nItem . '","sim")')
            ),
            Html::link('<i class="icon-close"> Não</i>', '',
                array('class' => 'botao botao-'.$classeRespostaNao,
                    'id' => 'botao-nao-'.$nItem,
                    'onclick' => 'marcaResposta("' . $nItem . '","nao")')
            ) .'</td>';

        //obrigatoriedade
        echo '<td>' . Html::checkBox('Checklist['.$nChecklist.'][itens]['.$nItem.'][obrigatorio]', $obrigatorio) . '</td>';

        //botao remover item
        echo '<td>' . Html::link('<i class="icon-trash-o icon-resize-small"></i>', '',
                array('class' => 'botao botao-danger',
                    'title' => 'Excluir',
                    'onclick' => 'removerItemChecklist("' . $nItem . ','.$nChecklist.'")')
            ) . '</td>';
        echo '</tr>';
    }

}

