<?php
/* @var $this ProjetoController */
/* @var $data Projeto */
?>

<div class="view">

    	<b><?php echo Html::encode($data->getAttributeLabel('IDProjeto')); ?>:</b>
	<?php echo Html::link(Html::encode($data->IDProjeto), array('view', 'id'=>$data->IDProjeto)); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDModerador')); ?>:</b>
	<?php echo Html::encode($data->IDModerador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('nome_projeto')); ?>:</b>
	<?php echo Html::encode($data->nome_projeto); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('descricao_projeto')); ?>:</b>
	<?php echo Html::encode($data->descricao_projeto); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('observacao_projeto')); ?>:</b>
	<?php echo Html::encode($data->observacao_projeto); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('dt_cadastro')); ?>:</b>
	<?php echo Html::encode($data->dt_cadastro); ?>
	<br />


</div>