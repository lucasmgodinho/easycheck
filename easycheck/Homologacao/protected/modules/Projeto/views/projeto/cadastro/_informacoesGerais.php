<?php

/* @var $model Projeto */
/* @var $form CActiveForm */

/* @var $moderadores Moderador */

?>

<div class="tab-pane active" id="tab_info">
    <div class="tabbable tabbable-custom tabs-left">
        <br>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDModerador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo
                $form->dropDownList(
                    $model,
                    'IDModerador',
                    Html::listData(
                        $moderadores,
                        'IDModerador',
                        'LabelNomeModerador'
                    ),
                    array(
                        'class' => 'select2 span6',
                        'placeholder' => 'Escolha um Moderador',
                    )
                )
                ?>
                <?php echo $form->error($model, 'IDModerador', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'nome_projeto',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'nome_projeto',array('class'=>'m-wrap span3','maxlength'=>255)); ?>
                <?php echo $form->error($model,'nome_projeto',array('class'=>'help-inline')); ?>
            </div>
        </div><br>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'descricao_projeto', array('class' => 'control-label')); ?>
        </div>

        <div class="div-textarea-container">
            <?php echo $form->textArea(
                $model,
                'descricao_projeto',
                array(
                    'rows' => 6,
                    'cols' => 50,
                    'class' => 'm-wrap span6'
                )
            ); ?>
            <?php echo $form->error($model, 'descricao_projeto', array('class' => 'help-inline')); ?>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'observacao_projeto', array('class' => 'control-label')); ?>
        </div>

        <div class="div-textarea-container">
            <?php echo $form->textArea(
                $model,
                'observacao_projeto',
                array(
                    'rows' => 6,
                    'cols' => 50,
                    'class' => 'm-wrap span6'
                )
            ); ?>
            <?php echo $form->error($model, 'observacao_projeto', array('class' => 'help-inline')); ?>
        </div>
    </div>

</div>


