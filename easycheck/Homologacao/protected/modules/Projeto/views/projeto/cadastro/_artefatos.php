<?php

/* @var $model Projeto */
/* @var $form CActiveForm */

/* @var $moderadores Moderador */

?>

<div class="tab-pane" id="tab_artefatos">
    <div class="tabbable tabbable-custom tabs-left">
        <p id="info-tabela-an" class="note note-info-fillout">
            Preencha os campos para adicionar <b>artefatos de software</b> a este projeto.
        </p>

        <table id="tabela-artefatos" class="table table-striped table-hover table-bordered flip-scroll" style="width: 80%; margin-left: 10%">
            <thead>
            <th style="width: 50%"><?php echo 'Nome do Artefato'; ?></th>
            <th style="width: 40%"><?php echo 'Arquivo'; ?></th>
            <th class="button-column"></th>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <th colspan="3"><button onclick="adicionaArtefato()" type="button" class="botao botao-info"">Adicionar Artefato</button></th>
            </tfoot>
        </table>

    </div>
</div>
