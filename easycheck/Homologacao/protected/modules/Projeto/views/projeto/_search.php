<?php
/* @var $this ProjetoController */
/* @var $model Projeto */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'IDProjeto'); ?>
            <?php echo $form->textField($model,'IDProjeto',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDModerador'); ?>
            <?php echo $form->textField($model,'IDModerador',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'nome_projeto'); ?>
            <?php echo $form->textField($model,'nome_projeto',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'descricao_projeto'); ?>
            <?php echo $form->textField($model,'descricao_projeto',array('class'=>'m-wrap span6','maxlength'=>2000)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'observacao_projeto'); ?>
            <?php echo $form->textField($model,'observacao_projeto',array('class'=>'m-wrap span6','maxlength'=>2000)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_cadastro'); ?>
            <?php echo $form->textField($model,'dt_cadastro',array('class'=>'m-wrap span6')); ?>
        </div>

        <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->