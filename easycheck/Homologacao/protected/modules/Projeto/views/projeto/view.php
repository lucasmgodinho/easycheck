<?php
/* @var $this ProjetoController */
/* @var $model Projeto */

?>


<?php $this->widget('application.widgets.DetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'IDModerador' => [
			'name' => 'Moderador',
			'value' => $model->iDModerador,
		],
		'nome_projeto',
		'descricao_projeto' => [
			'name' => 'Descrição',
			'value' => $model->descricao_projeto,
			'type' => 'html'
		],
		'observacao_projeto' => [
			'name' => 'Observação',
			'value' => $model->observacao_projeto,
			'type' => 'html'
		],
		'documento_artefato' => array(
			'label' => $model->attributeLabels()['documento_artefato'],
			'value' =>  $model->viewDocumentosArtefato(),
			'type' => 'html'
		),
		'dt_cadastro' => array(
			'label' => $model->attributeLabels()['dt_cadastro'],
			'value' =>  $model->dt_cadastro,
			'type' => 'date'
		),
	),
)); ?>
