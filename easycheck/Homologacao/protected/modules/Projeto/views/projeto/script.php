<?php
/* @var $model Projeto */
?>
<script>

    var nomeArtefato = $('#<?= Html::activeId($model, "nome_artefato") ?>');
    var ducomentoArtefato = $('#<?= Html::activeId($model, "documento_artefato") ?>');

    $(document).ready(function() {
        $('textarea').jqte(configuracaoTextEditorSimple);
        carregaArtefatos();
    });

    var quantidadeArtefatosCadastrados = <?php echo json_encode($model->quantidadeProjetos); ?>;
    var quantidadeArtefatos = 0 + quantidadeArtefatosCadastrados;

    function carregaArtefatos(){

        var IDProjeto = <?php echo json_encode($model->IDProjeto); ?>;
        $.ajax({
            url: "<?= $this->createUrl('ajaxCarregaTabelaArtefatoProjeto') ?>",
            type: 'POST',
            dataType: 'text',
            data: {
                IDProjeto : IDProjeto,
            },
        }).done(function(body) {
            if (body) {
                $("#tabela-artefatos").append(body);
            }
        });
    }


    function adicionaArtefato(){

        $.ajax({
            url: "<?= $this->createUrl('ajaxAdicionaArtefatoProjeto') ?>",
            type: 'POST',
            dataType: 'text',
            data: {
                quantidadeArtefatos: quantidadeArtefatos,
            },
        }).done(function (body) {
            if (body) {
                $("#tabela-artefatos").append(body);
                quantidadeArtefatos++;
                console.log(quantidadeArtefatosCadastrados)
            }
        });
    }

    function removerArtefato(e){
        $('#artefato_' + e).fadeOut(300, function() {
            $('#artefato_' + e).remove();
        });
    }

    $('#submeter').click(function (e) {

        e.preventDefault();
        var error = false;

        var artefatos = document.querySelectorAll('[id^=artefato_]');

        artefatos.forEach(function (tabela) {
            var nomeInput = tabela.querySelector('input[id^=nome_artefato]');
            var documentoInput = tabela.querySelector('input[type="file"]');

            if (!nomeInput.value.trim()) {
                error = true;
                alert('Por favor, preencha o nome do artefato.');
                nomeInput.focus();
                return false; // Sai do loop
            }

            if (!documentoInput.files.length) {
                error = true;
                alert('Por favor, selecione um arquivo PDF para o artefato.');
                documentoInput.focus();
                return false; // Sai do loop
            }
        });


        if (!error) {
            $('#projeto-form').submit();
        } else {
            mantemNomeLabelSubmit()
            return false;
        }
    });

    function mantemNomeLabelSubmit(){
        $('#submeter').val("<?php echo $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações'?>");
    }
</script>