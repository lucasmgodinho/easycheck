<?php
/* @var $this ProjetoController */
/* @var $model Projeto */

?>

<p class="note note-warning">
Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada pesquisa.
</p>


<?php $this->widget('application.widgets.grid.GridView', array(
	'id'=>'projeto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
            'selectableRows' => 2,
	'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
		'nome_projeto',
        array(
            'name' => 'IDModerador',
            'value' => '$data->iDModerador->LabelNomeModerador',
            'filter' => ''
        ),
        array(
            'header' => 'Artefatos',
            'value' => '$data->viewDocumentosArtefato()',
            'type' => 'raw',
        ),
		/*
		'descricao_projeto',
		'observacao_projeto',
		'dt_cadastro',
		*/
            array(
                 'class' => 'ButtonColumn',
                 'template' => '{view}{update}{delete}',
                 'buttons' =>
                 array(
                      'view' => array(
                           'visible' => 'true'
                      ),
                      'update' => array(
                           'visible' => 'true'
                      ),
                      'delete' => array(
                           'visible' => '$data->projetoPossuiInspecao() ? false : true'
                      ),
                 ),
            ),
	),
)); ?>
