<?php
/**
 * Controller do CRUD Projeto
 *
 * @package base.Controllers
 */

class ProjetoController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle= 'Visualizar '.$model->labelModel().' #'.$id;
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle= 'Criar '.Projeto::model()->labelModel();
        $model=new Projeto;

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Projeto']))
        {
            $model->attributes=$_POST['Projeto'];
            $model->dt_cadastro = HData::dateTimeBrToSql(HData::agora());
            $artefatos = [];

            if ($_FILES['documento_artefato']){
                foreach ($_POST['nome_artefato'] as $key => $nome) {
                    $artefatos['novos'][$key]['nome_artefato'] = $nome;
                    $artefatos['novos'][$key]['tmp_name'] = $_FILES['documento_artefato']['tmp_name'][$key];
                }
            }

            $transaction = new HTransaction();
            $transaction->beginTransaction();
            try {
                if($model->save() && $model->salvaArtefatos($artefatos)){
                    $transaction->commit();
                } else {
                    $transaction->rollback();
                }

            } catch (Exception $e) {
                throw $e;
            }
            $this->redirect(array('view','id'=>$model->IDProjeto));
        }

        $moderadores = Moderador::model()->findAll() ? : [];
        if ($moderadores){
            array_unshift($moderadores, "");
        }
        if (Yii::app()->user->papel == 'moderador') {
            $this->render('create', array(
                'model' => $model,
                'moderadores' => $moderadores,
            ));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle= 'Editar '.Projeto::model()->labelModel().' #'.$id;
        $model=$this->loadModel($id);


        $this->performAjaxValidation($model);

        if(isset($_POST['Projeto']))
        {
            $model->attributes=$_POST['Projeto'];

            $artefatos = [];

            if ($_FILES['documento_artefato']){
                foreach ($_POST['nome_artefato'] as $key => $nome) {
                    $artefatos['novos'][$key]['nome_artefato'] = $nome;
                    $artefatos['novos'][$key]['tmp_name'] = $_FILES['documento_artefato']['tmp_name'][$key];
                }
            }

            if ($_POST['documento_artefato']) {
                foreach ($_POST['documento_artefato'] as $nome => $pdf) {
                    $artefatos['cadastrados'][$nome] = $pdf;
                }
            }
            $transaction = new HTransaction();
            $transaction->beginTransaction();
            try {
                if($model->save() && $model->salvaArtefatos($artefatos)){
                    $transaction->commit();
                } else {
                    $transaction->rollback();
                }

            } catch (Exception $e) {
                throw $e;
            }
            $this->redirect(array('view','id'=>$model->IDProjeto));
        }

        $moderadores = Moderador::model()->findAll() ? : [];
        if ($moderadores){
            array_unshift($moderadores, "");
        }

        if (Yii::app()->user->papel == 'moderador') {
            $this->render('update', array(
                'model' => $model,
                'moderadores' => $moderadores,
            ));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest)
            foreach ($_POST['id'] as $id)
                $this->loadModel($id)->delete();
        else
            $this->loadModel($id)->delete();
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle= 'Buscar '.Projeto::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize']))
        {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);  // unseta para evitar atribuições futuras desnecessárias;
        }
        $model=new Projeto('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Projeto']))
            $model->attributes=$_GET['Projeto'];

        if (Yii::app()->user->papel == 'moderador') {

            if (Yii::app()->request->isAjaxRequest)
                $this->renderPartial('admin', array('model' => $model));
            else
                $this->render('admin', array('model' => $model));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Projeto the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Projeto::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Projeto $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='projeto-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPrintArtefato($id)
    {
        $artefato = ProjetoArtefato::model()->findByPk($id);

        $pdf = '';

        if (!is_null($artefato->documento_artefato)) {
            $pdf = base64_decode(stream_get_contents($artefato->documento_artefato));
        }

        header('Content-Type: application/pdf;');
        header('Content-Transfer-Encoding: binary');

        echo $pdf;

        Yii::app()->end();
    }

    public function actionAjaxCarregaTabelaArtefatoProjeto()
    {

        $quantidadeArtefatos = 0;
        if (Yii::app()->request->isAjaxRequest) {
            if (!empty($_REQUEST['IDProjeto'])) {
                $projeto = Projeto::model()->findByPk($_REQUEST['IDProjeto']);
                if ($projeto){
                    $artefatos = $projeto->getProjetoArtefato();
                    foreach ($artefatos as $artefato){
                        $this->retornaLinhaArtefatoCadastrado($quantidadeArtefatos, $artefato);
                        $quantidadeArtefatos++;
                    }
                }
            }
        }
    }

    public function retornaLinhaArtefatoCadastrado($i, $artefato){

        $nomeArtefato = $artefato->nome_artefato ? : '';

        $botaoExcluir = Html::link(
            '<i class="icon-trash-o icon-large"></i>',
            '',
            array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerArtefato('. $i .')')
        );

        if ($artefato->inspecaos){
            $botaoExcluir = '<div style="color: #9d0000">Existem inspeções para este artefato!</div>';
        }

        echo '<tr id="artefato_' . $i .'">';
        echo '<td><input id="nome_artefato_'. $i .'" name="nome_artefato['. $i .']" autocomplete="off" class="m-wrap span6" value="' . $nomeArtefato . '" disabled></td>';
        echo '<td>
                <input type="hidden" name="documento_artefato['. $artefato->IDProjetoArtefato .']" value="'. $nomeArtefato .'">
                <a target="_blank" href="'. Yii::app()->createUrl("Projeto/projeto/printArtefato", array("id"=>$artefato->primaryKey)) .'">Visualizar arquivo</a></td>';
        echo '<td>'.
            $botaoExcluir
            .'</td></tr>';
    }

    public function actionAjaxAdicionaArtefatoProjeto()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $i = $_REQUEST['quantidadeArtefatos'];

            echo '<tr id="artefato_' . $i .'">';
            echo '<td><input id="nome_artefato_'. $i .'" name="nome_artefato['. $i .']" autocomplete="off" class="m-wrap span6"></td>';
            echo '<td><input type="file" name="documento_artefato['. $i .']" accept="application/pdf"></td>';
            echo '<td>'.
                Html::link(
                    '<i class="icon-trash-o icon-large"></i>',
                    '',
                    array('class' => 'botao botao-danger', 'title' => 'Excluir', 'onclick' => 'removerArtefato('. $i .')')
                )
                .'</td></tr>';
        }

    }

}
