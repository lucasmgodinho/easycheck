<?php
/**
 * Controller do CRUD Inspetor
 *
 * @package base.Controllers
 */

class InspetorController extends Controller
{

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Inspecao the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Inspecao::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionMudarSenha()
    {
        $model = new FMudarSenha();
        if (isset($_POST["ajax"]) && $_POST["ajax"] === 'mudar-senha-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

// collect user input data
        if (isset($_POST['FMudarSenha'])) {
            $model->attributes = $_POST['FMudarSenha'];
            if ($model->validate()) {
                $model->changePassword();
            }
        }
        $this->redirect($this->createUrl('/perfil/', array('IDInspetor')));
    }

    public function actionMudarSenhaExpirada()
    {
        $model = new FMudarSenha();
        $this->layout = '//layouts/center';
        if (isset($_POST["ajax"]) && $_POST["ajax"] === 'mudar-senha-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
// collect user input data
        if (isset($_POST['FMudarSenha'])) {
            $model->attributes = $_POST['FMudarSenha'];
            if ($model->validate()) {
                $model->changePassword();
                $this->redirect($this->createUrl('/site/login', array('trocasenha' => 1)));
            }
        }
        $this->render('application.views.site.senhaExpirada', array('model' => $model));
    }

}