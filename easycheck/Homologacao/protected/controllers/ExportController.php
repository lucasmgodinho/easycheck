<?php
//aumenta o max_input_vars para poder processar mais registros
putenv('max_input_vars = 5000');

/**
 * Exporta os dados selecionados em um CGridView para o formato selecionado
 * @package base.Controllers
 */
class ExportController extends Controller
{

    public function actionExport()
    {
        $formato = (isset($_POST['target'])) ? $_POST['target'] : '';
        if ($formato != '') {
            $this->$formato();
        } //chama dinamicamente o método certo para exportar no formato especificado
        else {
            die("erro");
        }
    }


    /**
     * Renderiza os registros selecionados de uma CGridView em formato XML
     */
    public function XML()
    {
        echo __METHOD__;
    }

    /**
     * Renderiza os registros selecionados de uma CGridView em formato CSV
     * seguindo padrão do Excel, para facilitar a vida dos usuários, o caractere separador
     * utilizado será o ponto e vírgula (";")
     */
    public function CSV()
    {
        $tempName = uniqid('csv_') . '.pdf';
        //cria um arquivo temporário que será excluído assim que lido
        $f = fopen(Yii::app()->basePath . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . $tempName, 'w+');

        // Imprimindo os cabeçalhos
        if (isset($_POST['header'])) {
            fputcsv($f, $_POST['header'], ";");
        }

        $data = $_POST;
        $data['rows'] = json_decode($_POST['rows']);

        if (isset($data['rows'])) {
            foreach ($data['rows'] as $line) {
                if (is_array($line)) //somente tentar imprimir se o resultado for um array
                {
                    fputcsv($f, $line, ";");
                }
            }
        }
        fclose($f); //fecha a conexão com o arquivo para economizar memória
        $title = (isset($_POST['title']) && $_POST['title'] != null ? $_POST['title'] : $tempName);
        echo $this->createUrl('view', array('temp' => $tempName, 'type' => 'csv', 'title' => $title));
        Yii::app()->end();
    }

    /**
     * Printa na tela com os headers necessários o arquivo exportado
     * Automáticamente remove o arquivo temporário lido
     * @param String $temp nome do arquivo temporário a ser mostrado
     * @param String $title titulo que será utilizado (nome do arquivo)
     * @param String $type tipo do arquivo exportado (CSV, XML, Etc...)
     */
    public function actionView($temp, $title, $type)
    {
        //headers padrão para o navegador entender qual é o tipo de arquivo que será gerado
        header('Content-Type: application/' . $type . '; charset=utf-8');
        header('Content-Disposition: attachement; filename="' . $title . '.' . $type . '"');
        header('Content-Transfer-Encoding: binary');

        //path padrão para arquivos temporários
        $path = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . $temp;
        //se o arquivo realmente existir e for um arquivo
        if (file_exists($path) && is_file($path)) {
            echo $type === 'csv' ? "\xEF\xBB\xBF" : null; // hack maldito pra codificação no Excel
            echo file_get_contents($path);
            unlink($path);
        } else {
            echo 'erro';
        }
        Yii::app()->end();
    }

}

?>
