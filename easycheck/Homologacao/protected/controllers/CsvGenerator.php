<?php

class CsvGenerator
{
    public static function generateCsv($info, $discrepancias, $filename = 'output.csv')
    {
        require_once dirname(__FILE__) . '/../extensions/phpexcel/vendor/PHPExcel.php';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        if (!empty($info)) {
            $col = 0;
            foreach (array_keys($info) as $header) {
                $sheet->setCellValueByColumnAndRow($col, 1, $header);
                $col++;
            }

            // Adiciona a linha de dados
            $row = 2;
            $col = 0;
            foreach ($info as $cell) {
                $sheet->setCellValueByColumnAndRow($col, $row, $cell);
                $col++;
            }
        }



        if (!empty($discrepancias)) {

            $sheet->setCellValueByColumnAndRow(0, 3, '#');
            $sheet->setCellValueByColumnAndRow(1, 3, 'item_relacionado');
            $sheet->setCellValueByColumnAndRow(2, 3, 'tipo_defeito');
            $sheet->setCellValueByColumnAndRow(3, 3, 'descricao');
            $sheet->setCellValueByColumnAndRow(4, 3, 'localizacao');
            $sheet->setCellValueByColumnAndRow(5, 3, 'onde_repete');

            $numeroTotalDiscrepancias = 1;
            $row = 4;
            foreach ($discrepancias as $discrepancia) {
                $sheet->setCellValueByColumnAndRow(0, $row, $numeroTotalDiscrepancias);
                $col = 1;
                foreach ($discrepancia as $valor) {
                    $sheet->setCellValueByColumnAndRow($col, $row, $valor);
                    $col++;
                }
                $row++;
                $numeroTotalDiscrepancias++;
            }
        }



        // Limpa qualquer saída anterior
        if (ob_get_contents()) {
            ob_end_clean();
        }

        // Configuração do cabeçalho para forçar o download do arquivo CSV
        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Pragma: no-cache');
        header('Expires: 0');

        // Salva o arquivo CSV na saída padrão
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');

        // Termina a execução do script para evitar qualquer saída adicional
        Yii::app()->end();
    }
}
