<?php

/**
 * Controller Default da aplicação. páginas básicas ou estáticas são renderizadas por aqui.
 * @package base.Controllers
 */
class SiteController extends Controller
{

    public $outrasCrumbs = array();

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        //  $this->IDAcao = 1;

        $inspetor = Inspetor::model()->findByPk(Yii::app()->user->IDInspetor)->with('iDModerador');
        $moderador = $inspetor->moderadors[0];
        $this->render('index', array('inspetor' => $inspetor, 'moderador' => $moderador));
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            header("HTTP/1.0 " . $error['code']);

            $this->pageTitle = $this->getLabelPaginaErro($error['code']);

            $this->render('error', array(
                'error' => $error));
        }
    }

    /**
     * @param $codigoErro
     * @return string
     */
    private function getLabelPaginaErro($codigoErro)
    {
        switch ($codigoErro) {
            case 0:
                return 'Página Em Construção';
            case 1:
                return 'Página Em Manutenção';

            default:
                return 'Erro - ' . $codigoErro;
        }
    }

    /**
     * Ação para o Login do Usuário
     */
    public function actionLogin()
    {
        $this->pageTitle = 'Login';

        // Consulta para verificar se o domínio digitado é válido
        $schema = Yii::app()->db->createCommand()
            ->selectDistinct('schemaname AS esquema')
            ->from('pg_catalog.pg_tables')
            ->where('schemaname = :cliente', array(':cliente' => CLIENTE))
            ->queryRow();

        if (!$schema) {
            $this->render('application.views.site.error', array('code' => '404', 'externo' => 'TRUE'));
            exit();
        }

        if (isset($_POST['Login'])) {
            $identity = new UserIdentity($_POST['Login']['username'], $_POST['Login']['password']);
            $identity->authenticate();
            if ($identity->errorCode === UserIdentity::ERROR_NONE) {
                Yii::app()->user->login($identity);
                Yii::app()->user->setState('cliente', $schema['esquema']);

                $user = Inspetor::model()->findByPk(Yii::app()->user->IDInspetor);
                if ($user->trocou_senha === false) {
                        $this->layout = '//layouts/center';
                        Yii::app()->user->setState('senhaExpirada', true);
                        $this->render('application.views.site.senhaExpirada');
                        exit();
                }

                $moderador = Inspetor::model()->findByPk(YII::app()->user->IDInspetor)->moderadors;
                if (!empty($moderador)){
                    $this->redirect(array('site/escolhaPapel'));
                    return;
                }
                Yii::app()->user->setState('papel', 'inspetor');
                $this->getModulosSchema();
                $this->getMenu();
                Yii::app()->request->redirect(
                    Yii::app()->user->returnUrl
                );
            } else {
                $tempo = null;
                switch ($identity->errorCode) {
                    case UserIdentity::ERROR_USER_EXPIRED:
                        $msgErro = 'Conta expirada por tempo';
                        break;
                    case UserIdentity::ERROR_PROJECT_INVALID:
                        $msgErro = 'Esse Schema expirou ou está desativado';
                        break;
                    case UserIdentity::ERROR_USER_DISABLED:
                        $msgErro = 'Conta desativada';
                        break;
                    case UserIdentity::ERROR_USERNAME_INVALID:
                        $msgErro = 'Usuário ou senha incorretos';
                        break;
                    case UserIdentity::ERROR_USER_BLOCKED:
                        $msgErro = 'Seu usuário está bloqueado pelos próximos: ';
                        $tempo = $identity->getTempoBloqueio();
                        break;
                    default:
                        $msgErro = 'Usuário ou senha incorretos';
                        break;
                }
                $this->layout = '//layouts/center';
                $this->render('login', array('msgErro' => $msgErro, 'tempo' => $tempo));
                exit(0);
            }
        }


        $this->layout = '//layouts/center';
        $this->render('login');
    }

    public function actionEscolhaPapel()
    {
        if (Yii::app()->user->isGuest) {
            Yii::app()->request->redirect(array('site/login'));
        }

        if (isset($_POST['papel'])) {
            $papel = $_POST['papel'];
            Yii::app()->user->setState('papel', $papel);

            // Carregar o menu de acordo com o papel escolhido
            $this->getMenu($papel);
            $this->getModulosSchema();

            Yii::app()->request->redirect(Yii::app()->homeUrl);
        }

        // Renderizar a escolha de papel com o layout de tela cheia
        $this->layout = '//layouts/escolhaPapelLayout';
        $this->render('escolhaPapel');
    }


    /**
     * Ação para deslogar o usuario e redirecioná-lo para a página de login
     */
    public function actionLogout()
    {
        Yii::app()->user->logout(); //Onde o usuario realmente e deslogado
        $this->pageTitle = 'Logout';
        $this->actionLogin();
    }


    /**
     * Retorna um JSON contendo os flashes do usuário corrente
     */
    public function actionGetFlashes()
    {
        echo CJSON::encode((Yii::app()->user->getFlashes(true)));
    }

    /**
     * Renderiza a página de ajuda
     */
    public function actionAjuda()
    {
        $this->pageTitle = 'Ajuda';
        $this->outrasCrumbs = array(array('nome' => 'Ajuda', 'icone' => 'question', 'url' => 'site/ajuda'));
        $this->render('ajuda');
    }

    /*
     * @return void
     */
    public function actionPaginaManutencao()
    {
        $this->pageTitle = 'Sistema em atualização';
        $this->layout = '//layouts/clean';
        $this->render('application.views.site.error', array('code' => '1'));
        exit(0);
    }
}
