<?php

class m240404_203019_cria_eschemas extends CMigrations
{
	public function safeUp()
	{
        $this->execute("CREATE SCHEMA IF NOT EXISTS desenvolvimento");
        $this->execute("CREATE SCHEMA IF NOT EXISTS coppe");
	}

	public function safeDown()
	{
        $this->execute("DROP SCHEMA IF EXISTS desenvolvimento CASCADE");
        $this->execute("DROP SCHEMA IF EXISTS coppe CASCADE");
	}
}

