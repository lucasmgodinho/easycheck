<?php

class m240513_174209_altera_tabela_inspecao_item_checklist extends CMigrations
{
	public function safeUp()
	{
        foreach (CLIENTES as $cliente) {
            $this->addColumn($cliente . '.Inspecao_Item_Checklist', 'resposta_item', 'varchar(3)');
        }
	}

	public function safeDown()
	{
        foreach (CLIENTES as $cliente) {
            $this->dropColumn($cliente . '.Inspecao_Item_Checklist', 'resposta_item');
        }
	}
}