<?php

class m171214_163359_init extends CDbMigration
{
    public function up()
    {
        try {
            $config = Yii::app()->db;

            $db = new PDO($config->connectionString, $config->username, $config->password);
            $db->exec('ALTER TABLE public.tbl_migration DROP CONSTRAINT tbl_migration_pkey;');
            return true;
        } catch (PDOException $e) {
            var_dump($e);
            return false;
        }
    }

    public function down()
    {
        return false;
    }

}