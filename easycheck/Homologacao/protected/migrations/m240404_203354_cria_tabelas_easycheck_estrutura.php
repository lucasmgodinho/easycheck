<?php

class m240404_203354_cria_tabelas_easycheck_estrutura extends CMigrations
{
	public function safeUp()
	{

        foreach (CLIENTES as $cliente) {
            $this->criaTabelaInspetor($cliente);
            $this->criaTabelaModerador($cliente);
            $this->criaTabelaProjeto($cliente);
            $this->criaTabelaProjetoArtefato($cliente);
            $this->criaTabelaTecnica_Inspecao($cliente);
            $this->criaTabelaTecnica_Inspecao_Documento($cliente);
            $this->criaTabelaTecnica_Inspecao_Categoria_Defeito($cliente);
            $this->criaTabelaTecnica_Inspecao_Checklist($cliente);
            $this->criaTabelaItem_Checklist($cliente);
            $this->criaTabelaInspecao($cliente);
            $this->criaTabelaInspecao_Item_Checklist($cliente);
            $this->criaTabelaDiscrepancia($cliente);
            $this->criaTabelaRelatorio_Discrepancia($cliente);

            $this->criaTabelaHistoricoLogin($cliente);
        }
	}
        public function criaTabelaInspetor($cliente)
    {
        $colunasInspetor = [
            'IDInspetor' => 'serial4 PRIMARY KEY',
            'nome_inspetor' => 'varchar(255) not null',
            'login_inspetor' => 'varchar(255) not null',
            'email_inspetor' => 'varchar(255) not null',
            'senha_inspetor' => 'VARCHAR(255) not null',
            'dt_cadastro' => 'datetime NOT NULL default CURRENT_TIMESTAMP',
        ];
        $this->createTable($cliente . '.Inspetor', $colunasInspetor);
    }
    public function criaTabelaModerador($cliente)
    {
        $colunasModerador = [
            'IDModerador' => 'serial4 PRIMARY KEY',
            'IDInspetor' => 'int unique',
            'organizacao_moderador' => 'VARCHAR(255)',
        ];
        $this->createTable($cliente . '.Moderador', $colunasModerador);

        $this->addForeignKey(
            'fk_Moderador_to_Inspetor',
            $cliente . '.Moderador', 'IDInspetor',
            $cliente . '.Inspetor', 'IDInspetor',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaProjeto($cliente)
    {
        $colunasProjeto = [
            'IDProjeto' => 'serial4 PRIMARY KEY',
            'IDModerador' => 'integer not null',
            'nome_projeto' => 'varchar(255) not null',
            'descricao_projeto' => 'text not null',
            'observacao_projeto' => 'text',
            'dt_cadastro' => 'datetime NOT NULL default CURRENT_TIMESTAMP',
        ];

        $this->createTable($cliente . '.Projeto', $colunasProjeto);

        $this->addForeignKey(
            'fk_Projeto_to_Moderador',
            $cliente . '.Projeto', 'IDModerador',
            $cliente . '.Moderador', 'IDModerador',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaProjetoArtefato($cliente)
    {
        $colunasProjetoArtefato = [
            'IDProjetoArtefato' => 'serial4 PRIMARY KEY',
            'IDProjeto' => 'integer not null',
            'nome_artefato' => 'varchar(255) not null',
            'documento_artefato' => 'bytea not null',
        ];

        $this->createTable($cliente . '.Projeto_Artefato', $colunasProjetoArtefato);

        $this->addForeignKey(
            'fk_ProjetoArtefato_to_Projeto',
            $cliente . '.Projeto_Artefato', 'IDProjeto',
            $cliente . '.Projeto', 'IDProjeto',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaTecnica_Inspecao($cliente)
    {
        $colunasTecnicaInspecao = [
            'IDTecnicaInspecao' => 'serial4 PRIMARY KEY',
            'IDModerador' => 'integer not null',
            'nome_tecnica' => 'varchar(255) not null',
            'nome_autor' => 'varchar(255) not null',
            'descricao_tecnica' => 'text',
            'definicao_tecnica' => 'text not null',
            'dt_cadastro' => 'datetime NOT NULL default CURRENT_TIMESTAMP',
        ];

        $this->createTable($cliente . '.Tecnica_Inspecao', $colunasTecnicaInspecao);

        $this->addForeignKey(
            'fk_TecnicaInspecao_to_Moderador',
            $cliente . '.Tecnica_Inspecao', 'IDModerador',
            $cliente . '.Moderador', 'IDModerador',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaTecnica_Inspecao_Documento($cliente)
    {
        $colunasTecnicaInspecaoDocumento = [
            'IDTecnicaInspecaoDocumento' => 'serial4 PRIMARY KEY',
            'IDTecnicaInspecao' => 'integer not null',
            'nome_documento' => 'varchar(255) not null',
            'documento_tecnica' => 'bytea not null',
        ];

        $this->createTable($cliente . '.Tecnica_Inspecao_Documento', $colunasTecnicaInspecaoDocumento);

        $this->addForeignKey(
            'fk_TecnicaInspecaoDocumento_to_TecnicaInspecao',
            $cliente . '.Tecnica_Inspecao_Documento', 'IDTecnicaInspecao',
            $cliente . '.Tecnica_Inspecao', 'IDTecnicaInspecao',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaTecnica_Inspecao_Categoria_Defeito($cliente)
    {
        $colunasTecnicaInspecaoDefeito = [
            'IDTecnicaInspecaoCategoriaDefeito' => 'serial4 PRIMARY KEY',
            'IDTecnicaInspecao' => 'integer not null',
            'categoria_defeito' => 'varchar(255) not null',
        ];

        $this->createTable($cliente . '.Tecnica_Inspecao_Categoria_Defeito', $colunasTecnicaInspecaoDefeito);

        $this->addForeignKey(
            'fk_TecnicaInspecaoCategoriaDefeito_to_TecnicaInspecao',
            $cliente . '.Tecnica_Inspecao_Categoria_Defeito', 'IDTecnicaInspecao',
            $cliente . '.Tecnica_Inspecao', 'IDTecnicaInspecao',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaTecnica_Inspecao_Checklist($cliente)
    {
        $colunasTecnicaInspecaoChecklist = [
            'IDTecnicaInspecaoChecklist' => 'serial4 PRIMARY KEY',
            'IDTecnicaInspecao' => 'integer not null',
            'nome_checklist' => 'varchar(255) not null',
            'descricao_checklist' => 'varchar(2000)',
        ];

        $this->createTable($cliente . '.Tecnica_Inspecao_Checklist', $colunasTecnicaInspecaoChecklist);

        $this->addForeignKey(
            'fk_TecnicaInspecaoChecklist_to_TecnicaInspecao',
            $cliente . '.Tecnica_Inspecao_Checklist', 'IDTecnicaInspecao',
            $cliente . '.Tecnica_Inspecao', 'IDTecnicaInspecao',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaItem_Checklist($cliente)
    {
        $colunasItemChecklist = [
            'IDItemChecklist' => 'serial4 PRIMARY KEY',
            'IDTecnicaInspecaoChecklist' => 'integer not null',
            'numeracao_item' => 'int not null',
            'pergunta_item' => 'varchar(2000) not null',
            'respostaEsperada' => 'boolean not null',
            'obrigatoriedade' => 'boolean',
        ];

        $this->createTable($cliente . '.Item_Checklist', $colunasItemChecklist);

        $this->addForeignKey(
            'fk_ItemChecklist_to_TecnicaInspecaoChecklist',
            $cliente . '.Item_Checklist', 'IDTecnicaInspecaoChecklist',
            $cliente . '.Tecnica_Inspecao_Checklist', 'IDTecnicaInspecaoChecklist',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaInspecao($cliente)
    {
        $colunasInspecao = [
            'IDInspecao' => 'serial4 PRIMARY KEY',
            'IDProjetoArtefato' => 'integer not null',
            'IDInspetor' => 'integer not null',
            'IDModerador' => 'integer not null',
            'IDTecnicaInspecao' => 'integer not null',
            'observacao' => 'text',
            'dt_cadastro' => 'datetime NOT NULL default CURRENT_TIMESTAMP',
            'finalizado' => 'boolean',
        ];

        $this->createTable($cliente . '.Inspecao', $colunasInspecao);

        $this->addForeignKey(
            'fk_Inspecao_to_ProjetoArtefato',
            $cliente . '.Inspecao', 'IDProjetoArtefato',
            $cliente . '.Projeto_Artefato', 'IDProjetoArtefato',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_Inspecao_to_Inspetor',
            $cliente . '.Inspecao', 'IDInspetor',
            $cliente . '.Inspetor', 'IDInspetor',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_Inspecao_to_Moderador',
            $cliente . '.Inspecao', 'IDModerador',
            $cliente . '.Moderador', 'IDModerador',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_Inspecao_to_TecnicaInspecao',
            $cliente . '.Inspecao', 'IDTecnicaInspecao',
            $cliente . '.Tecnica_Inspecao', 'IDTecnicaInspecao',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaInspecao_Item_Checklist($cliente)
    {
        $colunasInspecaoItemChecklist = [
            'IDInspecaoItemChecklist' => 'serial4 PRIMARY KEY',
            'IDInspecao' => 'integer not null',
            'IDItemChecklist' => 'integer not null',
        ];

        $this->createTable($cliente . '.Inspecao_Item_Checklist', $colunasInspecaoItemChecklist);

        $this->addForeignKey(
            'fk_InspecaoItemChecklist_to_Inspecao',
            $cliente . '.Inspecao_Item_Checklist', 'IDInspecao',
            $cliente . '.Inspecao', 'IDInspecao',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_InspecaoItemChecklist_to_ItemChecklist',
            $cliente . '.Inspecao_Item_Checklist', 'IDItemChecklist',
            $cliente . '.Item_Checklist', 'IDItemChecklist',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaDiscrepancia($cliente)
    {
        $colunasDiscrepancia = [
            'IDDiscrepancia' => 'serial4 PRIMARY KEY',
            'IDInspecaoItemChecklist' => 'integer not null',
            'IDTecnicaInspecaoCategoriaDefeito' => 'integer not null',
            'descricao_discrepancia' => 'text not null',
            'localizacao_discrepancia' => 'text',
            'localOndeRepete' => 'text',
        ];

        $this->createTable($cliente . '.Discrepancia', $colunasDiscrepancia);

        $this->addForeignKey(
            'fk_Discrepancia_to_InspecaoItemChecklist',
            $cliente . '.Discrepancia', 'IDInspecaoItemChecklist',
            $cliente . '.Inspecao_Item_Checklist', 'IDInspecaoItemChecklist',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_Discrepancia_to_TecnicaInspecaoCategoriaDefeito',
            $cliente . '.Discrepancia', 'IDTecnicaInspecaoCategoriaDefeito',
            $cliente . '.Tecnica_Inspecao_Categoria_Defeito', 'IDTecnicaInspecaoCategoriaDefeito',
            'CASCADE', 'CASCADE'
        );
    }
    public function criaTabelaRelatorio_Discrepancia($cliente)
    {
        $colunasRelatorioDiscrepancia = [
            'IDRelatorioDiscrepancia' => 'serial4 PRIMARY KEY',
            'IDInspecao' => 'integer not null',
            'documento_relatorio' => 'bytea not null',
        ];

        $this->createTable($cliente . '.Relatorio_Discrepancia', $colunasRelatorioDiscrepancia);

        $this->addForeignKey(
            'fk_RelatorioDiscrepancia_to_Inspecao',
            $cliente . '.Relatorio_Discrepancia', 'IDInspecao',
            $cliente . '.Inspecao', 'IDInspecao',
            'CASCADE', 'CASCADE'
        );
    }

    public function criaTabelaHistoricoLogin($cliente)
    {
        $colunasHistoricoLogin = [
            'IDHistoricoLogin' => 'serial4 PRIMARY KEY',
            'IDInspetor' => 'integer not null',
            'ip_historicoLogin' => 'varchar(20)',
            'dt_historicoLogin' => 'TIMESTAMP DEFAULT (\'now\'::text)::timestamp(0) with time zone NOT NULL',
            'error_historicoLogin' => 'smallint not null',
        ];

        $this->createTable($cliente . '.Historico_login', $colunasHistoricoLogin);

        $this->addForeignKey(
            'fk_historicoLogin_to_Inspetor',
            $cliente . '.Historico_login', 'IDInspetor',
            $cliente . '.Inspetor', 'IDInspetor',
            'CASCADE', 'CASCADE'
        );
    }


	public function safeDown()
	{
        foreach (CLIENTES as $cliente) {
            $this->dropTable($cliente . '.Relatorio_Discrepancia');
            $this->dropTable($cliente . '.Discrepancia');
            $this->dropTable($cliente . '.Inspecao_Item_Checklist');
            $this->dropTable($cliente . '.Inspecao');
            $this->dropTable($cliente . '.Item_Checklist');
            $this->dropTable($cliente . '.Tecnica_Inspecao_Checklist');
            $this->dropTable($cliente . '.Tecnica_Inspecao_Categoria_Defeito');
            $this->dropTable($cliente . '.Tecnica_Inspecao_Documento');
            $this->dropTable($cliente . '.Tecnica_Inspecao');
            $this->dropTable($cliente . '.Projeto_Artefato');
            $this->dropTable($cliente . '.Projeto');
            $this->dropTable($cliente . '.Historico_login');
            $this->dropTable($cliente . '.Inspetor');
            $this->dropTable($cliente . '.Moderador');

        }

	}
}

