<?php

class m240408_213847_cria_tabelas_publicas extends CMigrations
{
	public function safeUp()
	{

        $colunasModulo = [
            'IDModulo' => 'serial4 PRIMARY KEY',
            'nome_modulo' => 'varchar(255) not null',
            'link_modulo' => 'varchar(255) not null',
            'slug_modulo' => 'varchar(20) not null',
            'n_ordemModulo' => 'int',
            'classe_tipoModulo' => 'varchar(25)',
        ];

        $colunasSchema = [
            'IDSchema' => 'serial4 PRIMARY KEY',
            'dt_inicioSchema' => 'date not null',
            'slug_Schema' => 'varchar(255) not null',
            'descricao_Schema' => 'varchar(2000)',
            'img_Schema' => 'bytea',
        ];

        $this->createTable('public.Modulo', $colunasModulo);
        $this->createTable('public.Schema', $colunasSchema);

    }

	public function safeDown()
	{
        $this->dropTable('public.Schema');
        $this->dropTable('public.Modulo');
	}
}

