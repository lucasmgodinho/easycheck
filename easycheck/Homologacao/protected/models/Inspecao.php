<?php

/**
 * This is the model class for table "desenvolvimento.Inspecao".
 *
 * The followings are the available columns in table 'desenvolvimento.Inspecao':
 * @property integer $IDInspecao
 * @property integer $IDProjetoArtefato
 * @property integer $IDInspetor
 * @property integer $IDModerador
 * @property integer $IDTecnicaInspecao
 * @property string $observacao
 * @property string $dt_cadastro
 * @property boolean $finalizado
 * @property integer $tempo_gasto
 *
 * The followings are the available model relations:
 * @property ProjetoArtefato $iDProjetoArtefato
 * @property Inspetor $iDInspetor
 * @property Moderador $iDModerador
 * @property TecnicaInspecao $iDTecnicaInspecao
 * @property InspecaoItemChecklist[] $inspecaoItemChecklists
 * @property RelatorioDiscrepancia[] $relatorioDiscrepancias
 * @package base.Models
 */
class Inspecao extends ActiveRecord
{

    public $IDProjeto;
    public $nome_inspetor;
    public $nome_projeto;
    public $nome_tecnica;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Inspecao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDProjetoArtefato, IDInspetor, IDModerador, IDTecnicaInspecao, dt_cadastro, IDProjeto', 'required'),
            array('IDProjetoArtefato, IDInspetor, IDModerador, IDTecnicaInspecao, tempo_gasto', 'numerical', 'integerOnly'=>true),
            array('finalizado, nome_inspetor', 'safe'),
// @todo Please remove those attributes that should not be searched.
            array('IDInspecao, dt_cadastro, nome_tecnica, IDInspetor, nome_projeto,
             IDTecnicaInspecao, nome_inspetor, finalizado, tempo_gasto', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDProjetoArtefato' => array(self::BELONGS_TO, 'ProjetoArtefato', 'IDProjetoArtefato'),
            'iDInspetor' => array(self::BELONGS_TO, 'Inspetor', 'IDInspetor'),
            'iDModerador' => array(self::BELONGS_TO, 'Moderador', 'IDModerador'),
            'iDTecnicaInspecao' => array(self::BELONGS_TO, 'TecnicaInspecao', 'IDTecnicaInspecao'),
            'inspecaoItemChecklists' => array(self::HAS_MANY, 'InspecaoItemChecklist', 'IDInspecao'),
            'relatorioDiscrepancias' => array(self::HAS_MANY, 'RelatorioDiscrepancia', 'IDInspecao'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDInspecao' => 'Inspeção',
            'IDProjetoArtefato' => 'Artefato',
            'IDInspetor' => 'Inspetor',
            'IDModerador' => 'Idmoderador',
            'IDProjeto' => 'Projeto',
            'IDTecnicaInspecao' => 'Técnica de Inspeção',
            'observacao' => 'Observação',
            'dt_cadastro' => 'Data da Inspeção',
            'finalizado' => 'Finalizado',
            'nome_projeto' => 'Projeto',
            'nome_inspetor' => 'Inspetor',
            'nome_tecnica' => 'Técnica de Inspeção',
            'nome_moderador' => 'Moderador',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('iDInspetor', 'iDModerador', 'iDProjetoArtefato.iDProjeto', 'iDTecnicaInspecao');

        $criteria->compare('"IDInspecao"',HTexto::tiraLetras($this->IDInspecao));
        $criteria->compare('"IDProjetoArtefato"',$this->IDProjetoArtefato);
        $criteria->compare('"IDInspetor"',$this->IDInspetor);
        $criteria->compare('"IDModerador"',$this->IDModerador);
        $criteria->compare('"IDTecnicaInspecao"',$this->IDTecnicaInspecao);
        $criteria->compare('LOWER("observacao")',mb_strtolower($this->observacao),true);
        $criteria->compare('LOWER("dt_cadastro")',mb_strtolower($this->dt_cadastro),true);
        $criteria->compare('"finalizado"',$this->finalizado);
        $criteria->compare('UNACCENT(LOWER("iDInspetor"."nome_inspetor"))', mb_strtolower(HTexto::tiraAcentos(trim($this->nome_inspetor))), true);
        $criteria->compare('UNACCENT(LOWER("iDProjeto"."nome_projeto"))', mb_strtolower(HTexto::tiraAcentos(trim($this->nome_projeto))), true);
        $criteria->compare('UNACCENT(LOWER("iDTecnicaInspecao"."nome_tecnica"))', mb_strtolower(HTexto::tiraAcentos(trim($this->nome_tecnica))), true);
        $criteria->compare('"tempo_gasto"',$this->tempo_gasto);

        $papel = Yii::app()->user->papel;
        if ($papel == 'inspetor') {
            $criteria->compare('"iDInspetor"."IDInspetor"', Yii::app()->user->IDInspetor);
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"t"."dt_cadastro" DESC',
                'attributes' => array(
                    'nome_responsavel' => array(
                        'asc' => '"iDInspetor"."nome_inspetor" ASC',
                        'desc' => '"iDInspetor"."nome_inspetor" DESC'
                    ),
                )
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Inspecao the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getProjeto()
    {
        return $this->iDProjetoArtefato ? $this->iDProjetoArtefato->iDProjeto : '';
    }

    public function salvaAplicacaoInspecao($itens, $discrepancias)
    {
        if ($this->inspecaoItemChecklists){
            foreach ($this->inspecaoItemChecklists as $inspecaoItemChecklist){
                if ($inspecaoItemChecklist->discrepancias){
                    foreach ($inspecaoItemChecklist->discrepancias as $discrepancia){
                        if (!$discrepancia->delete()){
                            return false;
                        }
                    }
                }
            }
        }

        if (!$this->salvaItensInspecao($itens)){
            return false;
        }

        if (!$this->salvaDiscrepanciasInspecoa($discrepancias)){
            return false;
        }

        return true;
    }

    public function salvaDiscrepanciasInspecoa($discrepanciasItens)
    {
        foreach ($this->inspecaoItemChecklists as $inspecaoItem){
            if ($discrepanciasItens[$inspecaoItem['IDItemChecklist']]){

                $discrepancias = $discrepanciasItens[$inspecaoItem['IDItemChecklist']];

                foreach ($discrepancias as $discrepancia) {

                    $novaDiscrepancia = new Discrepancia();
                    $novaDiscrepancia->IDInspecaoItemChecklist = $inspecaoItem->IDInspecaoItemChecklist;
                    $novaDiscrepancia->IDTecnicaInspecaoCategoriaDefeito = $discrepancia['IDTecnicaInspecaoCategoriaDefeito'];
                    $novaDiscrepancia->descricao_discrepancia = $discrepancia['descricao_discrepancia'];
                    $novaDiscrepancia->localizacao_discrepancia = $discrepancia['localizacao_discrepancia'];
                    $novaDiscrepancia->localOndeRepete = $discrepancia['localOndeRepete'];
                    if (!$novaDiscrepancia->save()){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public function salvaItensInspecao($itens)
    {
        foreach ($this->inspecaoItemChecklists as $inspecaoItem){
            if ($itens[$inspecaoItem['IDItemChecklist']]){
                $inspecaoItem->resposta_item = $itens[$inspecaoItem['IDItemChecklist']];
                if (!$inspecaoItem->update()){
                    return false;
                }
            }
        }
        return true;
    }

    public function salvaItensUtilizadosInspecao($itens)
    {
        foreach($itens as $IDItem => $desativado){
            if (!$desativado){
                $inspecaoItem = new InspecaoItemChecklist();
                $inspecaoItem->IDInspecao = $this->IDInspecao;
                $inspecaoItem->IDItemChecklist = $IDItem;
                if (!$inspecaoItem->save()){
                    return false;
                }
            }
        }
        return true;
    }

    public function viewArtefato()
    {
        return Html::link(
            $this->iDProjetoArtefato->nome_artefato,
            Yii::app()->createUrl("Projeto/projeto/printArtefato", array("id"=>$this->IDProjetoArtefato)),
            ['target' => '_blank']
        );
    }

    public function viewTecnicaInspecao()
    {
        return Html::link(
            $this->iDTecnicaInspecao->nome_tecnica,
            Yii::app()->createUrl("TecnicaInspecao/tecnicaInspecao/view&id=", array("id"=>$this->IDTecnicaInspecao)),
            ['target' => '_blank']
        );
    }

    public function validaFinalizar()
    {
        $validacao = true;

        if ($this->inspecaoItemChecklists){
            foreach ($this->inspecaoItemChecklists as $inspecaoItemChecklist){
                if (!$inspecaoItemChecklist->resposta_item){
                    $validacao = false;
                }
            }
        }
        return $validacao;
    }

    public function montaInfoRelatorioDiscrepancia(){

        $info['nome_inspetor'] = $this->iDInspetor->nome_inspetor ? : '';
        $info['data_inspecao'] = $this->dt_cadastro ? : '';
        $info['tempo_inspecao'] = HTexto::milissegundosParaMinutos($this->tempo_gasto) ? : '';
        $info['projeto'] = $this->iDProjetoArtefato->iDProjeto->nome_projeto ? : '';
        $info['atividade'] = $this->iDProjetoArtefato->nome_artefato ? : '';
        $info['tecnica_inspecao'] = $this->iDTecnicaInspecao->nome_tecnica ? : '';

        return $info;

    }

    public function montaDiscrepanciasRelatorioDiscrepancia()
    {
        $discrepancias = [];
        if ($this->inspecaoItemChecklists){
            foreach ($this->inspecaoItemChecklists as $inspecaoItemChecklist){
                if ($inspecaoItemChecklist->discrepancias){
                    foreach ($inspecaoItemChecklist->discrepancias as $discrepancia){
                        $discrepancias[$discrepancia->IDDiscrepancia]['item_relacionado'] = $inspecaoItemChecklist->iDItemChecklist->numeracao_item ? : '';
                        $discrepancias[$discrepancia->IDDiscrepancia]['tipo_defeito'] = $discrepancia->iDTecnicaInspecaoCategoriaDefeito->categoria_defeito ? : '';
                        $discrepancias[$discrepancia->IDDiscrepancia]['descricao'] = $discrepancia->descricao_discrepancia ? : '';
                        $discrepancias[$discrepancia->IDDiscrepancia]['localizacao'] = $discrepancia->localizacao_discrepancia ? : '';
                        $discrepancias[$discrepancia->IDDiscrepancia]['onde_repete'] = $discrepancia->localOndeRepete ? : '';
                    }
                }
            }
        }
        return $discrepancias;
    }

    public function getSituacaoInspecao()
    {
        $situacao = '';

        if ($this->finalizado){
            $situacao = '<b style="color: #1a8700">Finalizada!</b>';
        } elseif ($this->tempo_gasto){
            $situacao = '<b style="color: #d8a300">Em andamento...</b>';
        } else {
            $situacao = '<b style="color: #0087bd">Pendente</b>';
        }

        return $situacao;
    }

}
