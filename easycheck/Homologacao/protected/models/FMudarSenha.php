<?php
/**
 * @package base.Models
 */
class FMudarSenha extends CFormModel
{

    public $currentPassword;
    public $newPassword;
    public $newPassword_repeat;
    private $_user;


    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array(
                'currentPassword',
                'compareCurrentPassword'
            ),
            array(
                'newPassword',
                'compareLogin'
            ),
            array(
                'currentPassword, newPassword, newPassword_repeat',
                'required',
                //'message'=>'Introduzca su {attribute}.',
            ),
            array('newPassword', 'length', 'min' => 7, 'max' => 60),
            array(
                'newPassword_repeat',
                'compare',
                'compareAttribute' => 'newPassword',
                'message' => 'As senhas não coincidem.',
            ),
            array(
                'newPassword',
                'compare',
                'compareAttribute' => 'currentPassword',
                'operator' => '!=',
                'message' => 'A nova senha não pode ser igual a antiga',
            ),
        );
    }

    public function compareCurrentPassword($attribute, $params)
    {
        $user = Yii::app()->db->createCommand()
            ->select('*')
            ->from(CLIENTE . '.Inspetor')
            ->where("login_inspetor = :login", array(':login' => Yii::app()->user->login_inspetor))
            ->queryRow();

        if ($user) {
            $hashSenhaArmazenada = $user['senha_inspetor'];
            if (!$hashSenhaArmazenada || crypt($this->currentPassword, $hashSenhaArmazenada) != $hashSenhaArmazenada) {
                $this->addError($attribute, 'Senha Incorreta');
            }
        } else {
                $this->addError($attribute, 'Senha Incorreta');
        }
    }

    public function verificaPassword($attribute, $params)
    {
        $user = Inspetor::model()->FindBySql(
            'SELECT * FROM "' . CLIENTE . '"."Inspetor" WHERE crypt(\'' . $this->currentPassword . '\', senha_inspetor) = senha_inspetor '
        );
        if ($user === null) {
            $this->addError($attribute, 'Senha Incorreta');
        }
    }

    /**
     * Verifica se a senha contém o Login do usuário, nesse caso retorna um erro
     * @param $attribute
     * @param $params
     */
    public function compareLogin($attribute, $params)
    {
        $login = Yii::app()->user->getState('login_usuario');
        if (is_int(strpos($this->newPassword, $login))) {
            $this->addError($attribute, 'A senha não pode conter o Login do Usuário');
        }
    }

    public function init()
    {
        $this->_user = Inspetor::model()->findByAttributes(array('IDInspetor' => Yii::app()->User->IDInspetor));
    }

    public function attributeLabels()
    {
        return array(
            'currentPassword' => 'Senha Atual',
            'newPassword' => 'Nova Senha',
            'newPassword_repeat' => 'Confirme a senha',
        );
    }

    public function changePassword()
    {

        $this->_user->senha_inspetor = new CDbExpression("crypt('" . $this->newPassword . "', gen_salt('md5'))");
        $this->_user->trocou_senha = true;
        $this->_user->save();
        Yii::app()->user->logout();
    }

}