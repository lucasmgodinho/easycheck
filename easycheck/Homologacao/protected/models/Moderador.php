<?php

/**
 * This is the model class for table "desenvolvimento.Moderador".
 *
 * The followings are the available columns in table 'desenvolvimento.Moderador':
 * @property integer $IDModerador
 * @property integer $IDInspetor
 * @property string $organizacao_moderador
 *
 * The followings are the available model relations:
 * @property Inspetor $iDInspetor
 * @property Projeto[] $projetos
 * @property TecnicaInspecao[] $tecnicaInspecaos
 * @property Inspecao[] $inspecaos
 * @package base.Models
 */
class Moderador extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Moderador';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDInspetor', 'numerical', 'integerOnly'=>true),
            array('organizacao_moderador', 'length', 'max'=>255),
// @todo Please remove those attributes that should not be searched.
            array('IDModerador, IDInspetor, organizacao_moderador', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDInspetor' => array(self::BELONGS_TO, 'Inspetor', 'IDInspetor'),
            'projetos' => array(self::HAS_MANY, 'Projeto', 'IDModerador'),
            'tecnicaInspecaos' => array(self::HAS_MANY, 'TecnicaInspecao', 'IDModerador'),
            'inspecaos' => array(self::HAS_MANY, 'Inspecao', 'IDModerador'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDModerador' => 'Idmoderador',
            'IDInspetor' => 'Idinspetor',
            'organizacao_moderador' => 'Organizacao Moderador',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDModerador"',HTexto::tiraLetras($this->IDModerador));
        $criteria->compare('"IDInspetor"',$this->IDInspetor);
        $criteria->compare('LOWER("organizacao_moderador")',mb_strtolower($this->organizacao_moderador),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDModerador" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Moderador the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getFoto()
    {
        return $this->getImagemPadrao();
    }

    public function getImagemPadrao()
    {
        return ASSETS_LINK . '/images/main/usuarios/default_moderador.png';

    }

    public function __toString()
    {
        return $this->getLabelNomeModerador();
    }

    public function getLabelNomeModerador(){
        return $this->iDInspetor ? $this->iDInspetor->nome_inspetor : '';
    }
}
