<?php

/**
 * This is the model class for table "desenvolvimento.Tecnica_Inspecao_Checklist".
 *
 * The followings are the available columns in table 'desenvolvimento.Tecnica_Inspecao_Checklist':
 * @property integer $IDTecnicaInspecaoChecklist
 * @property integer $IDTecnicaInspecao
 * @property string $nome_checklist
 * @property string $descricao_checklist
 *
 * The followings are the available model relations:
 * @property TecnicaInspecao $iDTecnicaInspecao
 * @property ItemChecklist[] $itemChecklists
 * @package base.Models
 */
class TecnicaInspecaoChecklist extends ActiveRecord
{

    public $quantidadeItensChecklist;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Tecnica_Inspecao_Checklist';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDTecnicaInspecao, nome_checklist', 'required'),
            array('IDTecnicaInspecao', 'numerical', 'integerOnly'=>true),
            array('nome_checklist', 'length', 'max'=>255),
            array('descricao_checklist', 'length', 'max'=>2000),
// @todo Please remove those attributes that should not be searched.
            array('IDTecnicaInspecaoChecklist, IDTecnicaInspecao, nome_checklist, descricao_checklist', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDTecnicaInspecao' => array(self::BELONGS_TO, 'TecnicaInspecao', 'IDTecnicaInspecao'),
            'itemChecklists' => array(self::HAS_MANY, 'ItemChecklist', 'IDTecnicaInspecaoChecklist'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTecnicaInspecaoChecklist' => 'Idtecnica Inspecao Checklist',
            'IDTecnicaInspecao' => 'Idtecnica Inspecao',
            'nome_checklist' => 'Nome Checklist',
            'descricao_checklist' => 'Descricao Checklist',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDTecnicaInspecaoChecklist"',HTexto::tiraLetras($this->IDTecnicaInspecaoChecklist));
        $criteria->compare('"IDTecnicaInspecao"',$this->IDTecnicaInspecao);
        $criteria->compare('LOWER("nome_checklist")',mb_strtolower($this->nome_checklist),true);
        $criteria->compare('LOWER("descricao_checklist")',mb_strtolower($this->descricao_checklist),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDTecnicaInspecaoChecklist" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TecnicaInspecaoChecklist the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function afterFind()
    {
        $this->quantidadeItensChecklist = count(ItemChecklist::model()
            ->findAllByAttributes(['IDTecnicaInspecaoChecklist' => $this->IDTecnicaInspecaoChecklist]));

        parent::afterFind();
    }

    public function getItensChecklist(){
        return $this->itemChecklists ? : array();
    }

}
