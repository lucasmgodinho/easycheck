<?php

/**
 * This is the model class for table "desenvolvimento.Item_Checklist".
 *
 * The followings are the available columns in table 'desenvolvimento.Item_Checklist':
 * @property integer $IDItemChecklist
 * @property integer $IDTecnicaInspecaoChecklist
 * @property string $pergunta_item
 * @property boolean $respostaEsperada
 * @property boolean $obrigatoriedade
 * @property integer $numeracao_item
 *
 * The followings are the available model relations:
 * @property TecnicaInspecaoChecklist $iDTecnicaInspecaoChecklist
 * @property InspecaoItemChecklist[] $inspecaoItemChecklists
 * @package base.Models
 */
class ItemChecklist extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Item_Checklist';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDTecnicaInspecaoChecklist, pergunta_item, numeracao_item', 'required'),
            array('IDTecnicaInspecaoChecklist, numeracao_item', 'numerical', 'integerOnly'=>true),
            array('pergunta_item', 'length', 'max'=>2000),
            array('obrigatoriedade', 'safe'),
// @todo Please remove those attributes that should not be searched.
            array('IDItemChecklist, IDTecnicaInspecaoChecklist, pergunta_item, respostaEsperada, obrigatoriedade, numeracao_item', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDTecnicaInspecaoChecklist' => array(self::BELONGS_TO, 'TecnicaInspecaoChecklist', 'IDTecnicaInspecaoChecklist'),
            'inspecaoItemChecklists' => array(self::HAS_MANY, 'InspecaoItemChecklist', 'IDItemChecklist'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDItemChecklist' => 'Iditem Checklist',
            'IDTecnicaInspecaoChecklist' => 'Idtecnica Inspecao Checklist',
            'pergunta_item' => 'Pergunta Item',
            'respostaEsperada' => 'Resposta Esperada',
            'obrigatoriedade' => 'Obrigatoriedade',
            'numeracao_item' => 'Numeracao Item',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDItemChecklist"',HTexto::tiraLetras($this->IDItemChecklist));
        $criteria->compare('"IDTecnicaInspecaoChecklist"',$this->IDTecnicaInspecaoChecklist);
        $criteria->compare('LOWER("pergunta_item")',mb_strtolower($this->pergunta_item),true);
        $criteria->compare('"respostaEsperada"',$this->respostaEsperada);
        $criteria->compare('"obrigatoriedade"',$this->obrigatoriedade);
        $criteria->compare('"numeracao_item"',$this->numeracao_item);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDItemChecklist" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ItemChecklist the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
