<?php

/**
 * This is the model class for table "desenvolvimento.Projeto".
 *
 * The followings are the available columns in table 'desenvolvimento.Projeto':
 * @property integer $IDProjeto
 * @property integer $IDModerador
 * @property string $nome_projeto
 * @property string $descricao_projeto
 * @property string $observacao_projeto
 * @property string $dt_cadastro
 *
 * The followings are the available model relations:
 * @property ProjetoArtefato[] $projetoArtefatos
 * @property Moderador $iDModerador
 * @package base.Models
 */
class Projeto extends ActiveRecord
{

    public $nome_artefato;
    public $documento_artefato;
    public $quantidadeProjetos;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Projeto';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDModerador, nome_projeto, descricao_projeto, dt_cadastro', 'required'),
            array('IDModerador', 'numerical', 'integerOnly'=>true),
            array('nome_projeto', 'length', 'max'=>255),
            array('observacao_projeto', 'safe'),
            // @todo Please remove those attributes that should not be searched.
            array('IDProjeto, IDModerador, nome_projeto, descricao_projeto, observacao_projeto, dt_cadastro', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'projetoArtefatos' => array(self::HAS_MANY, 'ProjetoArtefato', 'IDProjeto'),
            'iDModerador' => array(self::BELONGS_TO, 'Moderador', 'IDModerador'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDProjeto' => 'Projeto',
            'IDModerador' => 'Moderador',
            'nome_projeto' => 'Nome do Projeto',
            'descricao_projeto' => 'Descrição',
            'observacao_projeto' => 'Observação',
            'dt_cadastro' => 'Data do Cadastro',
            'nome_artefato' => 'Nome do Artefato',
            'documento_artefato' => 'Artefatos do Projeto',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDProjeto"',HTexto::tiraLetras($this->IDProjeto));
        $criteria->compare('"IDModerador"',$this->IDModerador);
        $criteria->compare('LOWER("nome_projeto")',mb_strtolower($this->nome_projeto),true);
        $criteria->compare('LOWER("descricao_projeto")',mb_strtolower($this->descricao_projeto),true);
        $criteria->compare('LOWER("observacao_projeto")',mb_strtolower($this->observacao_projeto),true);
        $criteria->compare('LOWER("dt_cadastro")',mb_strtolower($this->dt_cadastro),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDProjeto" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Projeto the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function __toString()
    {
        return $this->getLabelNomeProjeto();
    }

    public function getLabelNomeProjeto(){
        return $this->nome_projeto ? : '';
    }

    public function afterFind()
    {
        $this->quantidadeProjetos = count(ProjetoArtefato::model()
            ->findAllByAttributes(['IDProjeto' => $this->IDProjeto]));

        parent::afterFind();
    }

    public function getProjetoArtefato()
    {
        return $this->projetoArtefatos ? : [];
    }

    public function viewDocumentosArtefato()
    {

        $documentosArtefato = $this->getProjetoArtefato();
        $documentos = [];

        foreach ($documentosArtefato as $documentoArtefato) {

            $documentos[] = Html::link(
                $documentoArtefato->nome_artefato,
                Yii::app()->createUrl("Projeto/projeto/PrintArtefato", array("id"=>$documentoArtefato->IDProjetoArtefato)),
                ['target' => '_blank']
            );
        }
        return Html::htmlList($documentos);
    }

    public function salvaArtefatos($artefatos)
    {
        Yii::import('ext.upload.Upload');

        if (!$this->isNewRecord) {
            $artefatosSalvos = ProjetoArtefato::model()->findAllByAttributes(['IDProjeto' => $this->IDProjeto]);

            foreach ($artefatosSalvos as $artefatoSalvo) {
                if (!$artefatos['cadastrados'][$artefatoSalvo->IDProjetoArtefato]) {
                    if (!$artefatoSalvo->delete()) {
                        return false;
                    }
                }
            }
        }

        if ($artefatos['novos']) {
            foreach ($artefatos['novos'] as $artefatoNovo) {
                $file = $artefatoNovo['tmp_name'];
                $arquivo = new Upload($file);

                $projetoArtefato = new ProjetoArtefato();
                $projetoArtefato->IDProjeto = $this->IDProjeto;
                $projetoArtefato->nome_artefato = $artefatoNovo['nome_artefato'];
                $projetoArtefato->documento_artefato = base64_encode($arquivo->process());

                if (!$projetoArtefato->save()) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getProjetoArtefatos()
    {
        return $this->projetoArtefatos ? : [];
    }

    public function projetoPossuiInspecao()
    {
        $artefatos = $this->getProjetoArtefatos();

        $possuiInspecao = false;
        foreach ($artefatos as $artefato) {
            if ($artefato->inspecaos){
                $possuiInspecao = true;
            }
        }
        return $possuiInspecao;
    }

}
