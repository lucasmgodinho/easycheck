<?php

/**
 * This is the model class for table "desenvolvimento.Projeto_Artefato".
 *
 * The followings are the available columns in table 'desenvolvimento.Projeto_Artefato':
 * @property integer $IDProjetoArtefato
 * @property integer $IDProjeto
 * @property string $documento_artefato
 * @property string $nome_artefato
 *
 * The followings are the available model relations:
 * @property Projeto $iDProjeto
 * @property Inspecao[] $inspecaos
 * @package base.Models
 */
class ProjetoArtefato extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Projeto_Artefato';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDProjeto, documento_artefato, nome_artefato', 'required'),
            array('IDProjeto', 'numerical', 'integerOnly'=>true),
            array('nome_artefato', 'length', 'max'=>255),
            array(
                'documento_artefato',
                'file',
                'types' => 'pdf',
                'allowEmpty' => true,
                'skipOnError' => true
            ),
// @todo Please remove those attributes that should not be searched.
            array('IDProjetoArtefato, IDProjeto, nome_artefato', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDProjeto' => array(self::BELONGS_TO, 'Projeto', 'IDProjeto'),
            'inspecaos' => array(self::HAS_MANY, 'Inspecao', 'IDProjetoArtefato'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDProjetoArtefato' => 'Idprojeto Artefato',
            'IDProjeto' => 'Idprojeto',
            'documento_artefato' => 'Documento Artefato',
            'nome_artefato' => 'Nome Artefato',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDProjetoArtefato"',HTexto::tiraLetras($this->IDProjetoArtefato));
        $criteria->compare('"IDProjeto"',$this->IDProjeto);
        $criteria->compare('LOWER("documento_artefato")',mb_strtolower($this->documento_artefato),true);
        $criteria->compare('LOWER("nome_artefato")',mb_strtolower($this->nome_artefato),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDProjetoArtefato" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProjetoArtefato the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
