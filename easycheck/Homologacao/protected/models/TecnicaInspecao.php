<?php

/**
 * This is the model class for table "desenvolvimento.Tecnica_Inspecao".
 *
 * The followings are the available columns in table 'desenvolvimento.Tecnica_Inspecao':
 * @property integer $IDTecnicaInspecao
 * @property integer $IDModerador
 * @property string $nome_tecnica
 * @property string $nome_autor
 * @property string $descricao_tecnica
 * @property string $definicao_tecnica
 * @property string $dt_cadastro
 *
 * The followings are the available model relations:
 * @property Moderador $iDModerador
 * @property TecnicaInspecaoDocumento[] $tecnicaInspecaoDocumentos
 * @property TecnicaInspecaoCategoriaDefeito[] $tecnicaInspecaoCategoriaDefeitos
 * @property TecnicaInspecaoChecklist[] $tecnicaInspecaoChecklists
 * @property Inspecao[] $inspecaos
 * @package base.Models
 */
class TecnicaInspecao extends ActiveRecord
{
    public $nome_documento;
    public $documento_tecnica;
    public $quantidadeDocumentos;
    public $quantidadeCategoriasDefeito;
    public $quantidadeChecklists;

    public $nome_moderador;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Tecnica_Inspecao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDModerador, nome_tecnica, nome_autor, definicao_tecnica, dt_cadastro', 'required'),
            array('IDModerador', 'numerical', 'integerOnly'=>true),
            array('nome_tecnica, nome_autor', 'length', 'max'=>255),
            array('descricao_tecnica', 'safe'),
// @todo Please remove those attributes that should not be searched.
            array('IDTecnicaInspecao, IDModerador, nome_tecnica, nome_autor, descricao_tecnica, definicao_tecnica, dt_cadastro', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDModerador' => array(self::BELONGS_TO, 'Moderador', 'IDModerador'),
            'tecnicaInspecaoDocumentos' => array(self::HAS_MANY, 'TecnicaInspecaoDocumento', 'IDTecnicaInspecao'),
            'tecnicaInspecaoCategoriaDefeitos' => array(self::HAS_MANY, 'TecnicaInspecaoCategoriaDefeito', 'IDTecnicaInspecao'),
            'tecnicaInspecaoChecklists' => array(self::HAS_MANY, 'TecnicaInspecaoChecklist', 'IDTecnicaInspecao'),
            'inspecaos' => array(self::HAS_MANY, 'Inspecao', 'IDTecnicaInspecao'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTecnicaInspecao' => 'Técnica de Inspeção',
            'IDModerador' => 'Moderador',
            'nome_tecnica' => 'Nome da Técnica',
            'nome_autor' => 'Nome do Autor',
            'definicao_tecnica' => 'Definição da Técnica',
            'descricao_tecnica' => 'Descrição da Técnica',
            'dt_cadastro' => 'Data de Cadastro',
            'documento_tecnica' => 'Documentos',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('iDModerador');

        $criteria->compare('"IDTecnicaInspecao"',HTexto::tiraLetras($this->IDTecnicaInspecao));
        $criteria->compare('UNACCENT(LOWER("iDModerador"."nome_moderador"))', mb_strtolower(HTexto::tiraAcentos(trim($this->nome_moderador))), true);
        $criteria->compare('UNACCENT(LOWER("nome_tecnica"))', mb_strtolower(HTexto::tiraAcentos(trim($this->nome_tecnica))), true);
        $criteria->compare('LOWER("nome_autor")',mb_strtolower($this->nome_autor),true);
        $criteria->compare('LOWER("descricao_tecnica")',mb_strtolower($this->descricao_tecnica),true);
        $criteria->compare('LOWER("definicao_tecnica")',mb_strtolower($this->definicao_tecnica),true);
        $criteria->compare('LOWER("dt_cadastro")',mb_strtolower($this->dt_cadastro),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDTecnicaInspecao" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TecnicaInspecao the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function afterFind()
    {
        $this->quantidadeDocumentos = count(TecnicaInspecaoDocumento::model()
            ->findAllByAttributes(['IDTecnicaInspecao' => $this->IDTecnicaInspecao]));

        $this->quantidadeCategoriasDefeito = count(TecnicaInspecaoCategoriaDefeito::model()
            ->findAllByAttributes(['IDTecnicaInspecao' => $this->IDTecnicaInspecao]));

        $this->quantidadeChecklists = count(TecnicaInspecaoChecklist::model()
            ->findAllByAttributes(['IDTecnicaInspecao' => $this->IDTecnicaInspecao]));

        parent::afterFind();
    }

    public function getDocumentosTecnica()
    {
        return $this->tecnicaInspecaoDocumentos ? : [];
    }

    public function getCategoriasDefeitoTecnica()
    {
        return $this->tecnicaInspecaoCategoriaDefeitos ? : [];
    }

    public function getChecklistsTecnica()
    {
        return $this->tecnicaInspecaoChecklists ? : [];
    }

    public function viewDocumentosTecnica()
    {
        $documentosTecnica = $this->getDocumentosTecnica();
        $documentos = [];

        foreach ($documentosTecnica as $documentoTecnica) {

            $documentos[] = Html::link(
                $documentoTecnica->nome_documento,
                Yii::app()->createUrl("TecnicaInspecao/tecnicaInspecao/PrintDocumento", array("id"=>$documentoTecnica->IDTecnicaInspecaoDocumento)),
                ['target' => '_blank']
            );
        }
        return Html::htmlList($documentos);
    }

    public function excluiDadosAnterioresTecnica()
    {

        if ($this->tecnicaInspecaoDocumentos){
            if (!TecnicaInspecaoDocumento::model()->deleteAllByAttributes(['IDTecnicaInspecao' => $this->IDTecnicaInspecao]))
                return false;
        }

        if ($this->tecnicaInspecaoCategoriaDefeitos){
            if (!TecnicaInspecaoCategoriaDefeito::model()->deleteAllByAttributes(['IDTecnicaInspecao' => $this->IDTecnicaInspecao]))
                return false;
        }

        if ($this->tecnicaInspecaoChecklists){
            if (!TecnicaInspecaoChecklist::model()->deleteAllByAttributes(['IDTecnicaInspecao' => $this->IDTecnicaInspecao]))
                return false;
        }

        return true;
    }

    public function salvaInformacoesTecnica($documentos, $categoriaDefeitos, $checklists)
    {

        if (!$this->excluiDadosAnterioresTecnica()){
            return false;
        }

        if (!$this->salvaDocumentos($documentos)){
            return false;
        }

        if (!$this->salvaCategoriaDefdeitos($categoriaDefeitos)){
            return false;
        }
        if (!$this->salvaChecklists($checklists)){
            return false;
        }

        return true;
    }

    public function salvaChecklists($checklists){

        if ($checklists){
            foreach ($checklists as $checklist){

                $checklistTecnica = new TecnicaInspecaoChecklist();
                $checklistTecnica->IDTecnicaInspecao = $this->IDTecnicaInspecao;
                $checklistTecnica->nome_checklist = $checklist['nome_checklist'];
                $checklistTecnica->descricao_checklist = $checklist['descricao_checklist'];
                if ($checklistTecnica->save()) {
                    foreach ($checklist['itens'] as $item){

                        $itemChecklist = new ItemChecklist();
                        $itemChecklist->IDTecnicaInspecaoChecklist = $checklistTecnica->IDTecnicaInspecaoChecklist;
                        $itemChecklist->numeracao_item = $item['numeracao_item'];
                        $itemChecklist->pergunta_item = $item['pergunta_item'];
                        $itemChecklist->respostaEsperada = ($item['resposta-esperada'] == 'sim') ? true : false;
                        $itemChecklist->obrigatoriedade = (bool)$item['obrigatorio'];
                        if (!$itemChecklist->save()) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    public function salvaCategoriaDefdeitos($categoriaDefeitos)
    {

        if ($categoriaDefeitos){
            foreach ($categoriaDefeitos as $categoriaDefeito){
                $categoriaTecnica = new TecnicaInspecaoCategoriaDefeito();
                $categoriaTecnica->IDTecnicaInspecao = $this->IDTecnicaInspecao;
                $categoriaTecnica->categoria_defeito = $categoriaDefeito;
                if (!$categoriaTecnica->save()) {
                    return false;
                }
            }
        }
        return true;
    }

    public function salvaDocumentos($documentos)
    {
        Yii::import('ext.upload.Upload');

        if (!$this->isNewRecord) {
            TecnicaInspecaoDocumento::model()->deleteAllByAttributes(['IDTecnicaInspecao' => $this->IDTecnicaInspecao]);

            if ($documentos['cadastrados']) {
                foreach ($documentos['cadastrados'] as $nome => $pdf) {
                    $documentoTecnica = new TecnicaInspecaoDocumento();
                    $documentoTecnica->IDTecnicaInspecao = $this->IDTecnicaInspecao;
                    $documentoTecnica->nome_documento = $nome;
                    $documentoTecnica->documento_tecnica = $pdf;
                    if (!$documentoTecnica->save()) {
                        return false;
                    }
                }
            }
        }

        if ($documentos['novos']) {
            foreach ($documentos['novos'] as $documentoNovo) {
                $file = $documentoNovo['tmp_name'];
                $arquivo = new Upload($file);

                $documentoTecnica = new TecnicaInspecaoDocumento();
                $documentoTecnica->IDTecnicaInspecao = $this->IDTecnicaInspecao;
                $documentoTecnica->nome_documento = $documentoNovo['nome_documento'];
                $documentoTecnica->documento_tecnica = base64_encode($arquivo->process());
                if (!$documentoTecnica->save()) {
                    return false;
                }
            }
        }
        return true;
    }
}
