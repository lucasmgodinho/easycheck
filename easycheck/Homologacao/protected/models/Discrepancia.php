<?php

/**
 * This is the model class for table "desenvolvimento.Discrepancia".
 *
 * The followings are the available columns in table 'desenvolvimento.Discrepancia':
 * @property integer $IDDiscrepancia
 * @property integer $IDInspecaoItemChecklist
 * @property integer $IDTecnicaInspecaoCategoriaDefeito
 * @property string $descricao_discrepancia
 * @property string $localizacao_discrepancia
 * @property string $localOndeRepete
 *
 * The followings are the available model relations:
 * @property TecnicaInspecaoCategoriaDefeito $iDTecnicaInspecaoCategoriaDefeito
 * @property InspecaoItemChecklist $iDInspecaoItemChecklist
 * @package base.Models
 */
class Discrepancia extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Discrepancia';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDInspecaoItemChecklist, IDTecnicaInspecaoCategoriaDefeito, descricao_discrepancia', 'required'),
            array('IDInspecaoItemChecklist, IDTecnicaInspecaoCategoriaDefeito', 'numerical', 'integerOnly'=>true),
            array('localizacao_discrepancia, localOndeRepete', 'safe'),
// @todo Please remove those attributes that should not be searched.
            array('IDDiscrepancia, IDInspecaoItemChecklist, IDTecnicaInspecaoCategoriaDefeito, descricao_discrepancia, localizacao_discrepancia, localOndeRepete', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDTecnicaInspecaoCategoriaDefeito' => array(self::BELONGS_TO, 'TecnicaInspecaoCategoriaDefeito', 'IDTecnicaInspecaoCategoriaDefeito'),
            'iDInspecaoItemChecklist' => array(self::BELONGS_TO, 'InspecaoItemChecklist', 'IDInspecaoItemChecklist'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDDiscrepancia' => 'Iddiscrepancia',
            'IDInspecaoItemChecklist' => 'Idinspecao Item Checklist',
            'IDTecnicaInspecaoCategoriaDefeito' => 'Idtecnica Inspecao Categoria Defeito',
            'descricao_discrepancia' => 'Descricao Discrepancia',
            'localizacao_discrepancia' => 'Localizacao Discrepancia',
            'localOndeRepete' => 'Local Onde Repete',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDDiscrepancia"',HTexto::tiraLetras($this->IDDiscrepancia));
        $criteria->compare('"IDInspecaoItemChecklist"',$this->IDInspecaoItemChecklist);
        $criteria->compare('"IDTecnicaInspecaoCategoriaDefeito"',$this->IDTecnicaInspecaoCategoriaDefeito);
        $criteria->compare('LOWER("descricao_discrepancia")',mb_strtolower($this->descricao_discrepancia),true);
        $criteria->compare('LOWER("localizacao_discrepancia")',mb_strtolower($this->localizacao_discrepancia),true);
        $criteria->compare('LOWER("localOndeRepete")',mb_strtolower($this->localOndeRepete),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDDiscrepancia" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Discrepancia the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
