<?php

/**
 * This is the model class for table "desenvolvimento.Inspecao_Item_Checklist".
 *
 * The followings are the available columns in table 'desenvolvimento.Inspecao_Item_Checklist':
 * @property integer $IDInspecaoItemChecklist
 * @property integer $IDInspecao
 * @property integer $IDItemChecklist
 * @property string $resposta_item
 *
 * The followings are the available model relations:
 * @property Discrepancia[] $discrepancias
 * @property Inspecao $iDInspecao
 * @property ItemChecklist $iDItemChecklist
 * @package base.Models
 */
class InspecaoItemChecklist extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Inspecao_Item_Checklist';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDInspecao, IDItemChecklist', 'required'),
            array('IDInspecao, IDItemChecklist', 'numerical', 'integerOnly'=>true),
            array('resposta_item', 'length', 'max'=>3),
// @todo Please remove those attributes that should not be searched.
            array('IDInspecaoItemChecklist, IDInspecao, IDItemChecklist, resposta_item', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'discrepancias' => array(self::HAS_MANY, 'Discrepancia', 'IDInspecaoItemChecklist'),
            'iDInspecao' => array(self::BELONGS_TO, 'Inspecao', 'IDInspecao'),
            'iDItemChecklist' => array(self::BELONGS_TO, 'ItemChecklist', 'IDItemChecklist'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDInspecaoItemChecklist' => 'Idinspecao Item Checklist',
            'IDInspecao' => 'Idinspecao',
            'IDItemChecklist' => 'Iditem Checklist',
            'resposta_item' => 'Resposta Item',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDInspecaoItemChecklist"',HTexto::tiraLetras($this->IDInspecaoItemChecklist));
        $criteria->compare('"IDInspecao"',$this->IDInspecao);
        $criteria->compare('"IDItemChecklist"',$this->IDItemChecklist);
        $criteria->compare('LOWER("resposta_item")',mb_strtolower($this->resposta_item),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDInspecaoItemChecklist" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return InspecaoItemChecklist the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getClasseItem($respostaEsperada)
    {
        $classe = '';
        if ($this->resposta_item) {
            if ($this->resposta_item == $respostaEsperada) {
                $classe = 'success';
            } elseif ($this->resposta_item != $respostaEsperada) {
                if ($this->resposta_item == 'na') {
                    $classe = 'nao-aplica';
                } else {
                    $classe = 'error';
                }
            }
        }
        return $classe;
    }

    public function getClasseItemFooter($respostaEsperada)
    {
        $classe = '';

        if (!$this->resposta_item || $this->resposta_item == $respostaEsperada || $this->resposta_item == 'na') {
            $classe = 'hide';
        }
        return $classe;
    }

}
