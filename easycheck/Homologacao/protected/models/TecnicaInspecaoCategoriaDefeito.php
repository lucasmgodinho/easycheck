<?php

/**
 * This is the model class for table "desenvolvimento.Tecnica_Inspecao_Categoria_Defeito".
 *
 * The followings are the available columns in table 'desenvolvimento.Tecnica_Inspecao_Categoria_Defeito':
 * @property integer $IDTecnicaInspecaoCategoriaDefeito
 * @property integer $IDTecnicaInspecao
 * @property string $categoria_defeito
 *
 * The followings are the available model relations:
 * @property TecnicaInspecao $iDTecnicaInspecao
 * @property Discrepancia[] $discrepancias
 * @package base.Models
 */
class TecnicaInspecaoCategoriaDefeito extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Tecnica_Inspecao_Categoria_Defeito';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDTecnicaInspecao, categoria_defeito', 'required'),
            array('IDTecnicaInspecao', 'numerical', 'integerOnly'=>true),
            array('categoria_defeito', 'length', 'max'=>255),
// @todo Please remove those attributes that should not be searched.
            array('IDTecnicaInspecaoCategoriaDefeito, IDTecnicaInspecao, categoria_defeito', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDTecnicaInspecao' => array(self::BELONGS_TO, 'TecnicaInspecao', 'IDTecnicaInspecao'),
            'discrepancias' => array(self::HAS_MANY, 'Discrepancia', 'IDTecnicaInspecaoCategoriaDefeito'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTecnicaInspecaoCategoriaDefeito' => 'Idtecnica Inspecao Categoria Defeito',
            'IDTecnicaInspecao' => 'Idtecnica Inspecao',
            'categoria_defeito' => 'Categoria Defeito',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDTecnicaInspecaoCategoriaDefeito"',HTexto::tiraLetras($this->IDTecnicaInspecaoCategoriaDefeito));
        $criteria->compare('"IDTecnicaInspecao"',$this->IDTecnicaInspecao);
        $criteria->compare('LOWER("categoria_defeito")',mb_strtolower($this->categoria_defeito),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDTecnicaInspecaoCategoriaDefeito" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TecnicaInspecaoCategoriaDefeito the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
