<?php

/**
 * This is the model class for table "desenvolvimento.Inspetor".
 *
 * The followings are the available columns in table 'desenvolvimento.Inspetor':
 * @property integer $IDInspetor
 * @property string $nome_inspetor
 * @property string $login_inspetor
 * @property string $email_inspetor
 * @property string $senha_inspetor
 * @property string $dt_cadastro
 * @property string $dt_ultimatrocasenha
 * @property boolean $trocou_senha
 *
 * The followings are the available model relations:
 * @property Moderador[] $moderadors
 * @property Inspecao[] $inspecaos
 * @property HistoricoLogin[] $historicoLogins
 * @package base.Models
 */
class Inspetor extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Inspetor';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('nome_inspetor, login_inspetor, email_inspetor, senha_inspetor, dt_cadastro', 'required'),
            array('nome_inspetor, login_inspetor, email_inspetor, senha_inspetor', 'length', 'max'=>255),
            array('dt_ultimatrocasenha, trocou_senha', 'safe'),
// @todo Please remove those attributes that should not be searched.
            array('IDInspetor, nome_inspetor, login_inspetor, email_inspetor, senha_inspetor, dt_cadastro', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'moderadors' => array(self::HAS_MANY, 'Moderador', 'IDInspetor'),
            'inspecaos' => array(self::HAS_MANY, 'Inspecao', 'IDInspetor'),
            'historicoLogins' => array(self::HAS_MANY, 'HistoricoLogin', 'IDInspetor'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDInspetor' => 'Idinspetor',
            'nome_inspetor' => 'Nome Inspetor',
            'login_inspetor' => 'Login Inspetor',
            'email_inspetor' => 'Email Inspetor',
            'senha_inspetor' => 'Senha Inspetor',
            'dt_cadastro' => 'Dt Cadastro',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDInspetor"',HTexto::tiraLetras($this->IDInspetor));
        $criteria->compare('LOWER("nome_inspetor")',mb_strtolower($this->nome_inspetor),true);
        $criteria->compare('LOWER("login_inspetor")',mb_strtolower($this->login_inspetor),true);
        $criteria->compare('LOWER("email_inspetor")',mb_strtolower($this->email_inspetor),true);
        $criteria->compare('LOWER("senha_inspetor")',mb_strtolower($this->senha_inspetor),true);
        $criteria->compare('LOWER("dt_cadastro")',mb_strtolower($this->dt_cadastro),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDInspetor" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Inspetor the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function __toString()
    {
        return $this->getLabelNomeInspetor();
    }

    public function getLabelNomeInspetor(){
        return $this->nome_inspetor ? : '';
    }

    public function getFoto()
    {
        return $this->getImagemPadrao();
    }

    public function getImagemPadrao()
    {
        return ASSETS_LINK . '/images/main/usuarios/default_male.png';

    }

}
