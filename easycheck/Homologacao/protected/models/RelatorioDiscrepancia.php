<?php

/**
 * This is the model class for table "desenvolvimento.Relatorio_Discrepancia".
 *
 * The followings are the available columns in table 'desenvolvimento.Relatorio_Discrepancia':
 * @property integer $IDRelatorioDiscrepancia
 * @property integer $IDInspecao
 * @property string $documento_relatorio
 *
 * The followings are the available model relations:
 * @property Inspecao $iDInspecao
 * @package base.Models
 */
class RelatorioDiscrepancia extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Relatorio_Discrepancia';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDInspecao, documento_relatorio', 'required'),
            array('IDInspecao', 'numerical', 'integerOnly'=>true),
// @todo Please remove those attributes that should not be searched.
            array('IDRelatorioDiscrepancia, IDInspecao, documento_relatorio', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDInspecao' => array(self::BELONGS_TO, 'Inspecao', 'IDInspecao'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDRelatorioDiscrepancia' => 'Idrelatorio Discrepancia',
            'IDInspecao' => 'Idinspecao',
            'documento_relatorio' => 'Documento Relatorio',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDRelatorioDiscrepancia"',HTexto::tiraLetras($this->IDRelatorioDiscrepancia));
        $criteria->compare('"IDInspecao"',$this->IDInspecao);
        $criteria->compare('LOWER("documento_relatorio")',mb_strtolower($this->documento_relatorio),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDRelatorioDiscrepancia" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RelatorioDiscrepancia the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
