<?php

/**
 * This is the model class for table "desenvolvimento.Tecnica_Inspecao_Documento".
 *
 * The followings are the available columns in table 'desenvolvimento.Tecnica_Inspecao_Documento':
 * @property integer $IDTecnicaInspecaoDocumento
 * @property integer $IDTecnicaInspecao
 * @property string $documento_tecnica
 * @property string $nome_documento
 *
 * The followings are the available model relations:
 * @property TecnicaInspecao $iDTecnicaInspecao
 * @package base.Models
 */
class TecnicaInspecaoDocumento extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Tecnica_Inspecao_Documento';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDTecnicaInspecao, documento_tecnica, nome_documento', 'required'),
            array('IDTecnicaInspecao', 'numerical', 'integerOnly'=>true),
            array('nome_documento', 'length', 'max'=>255),
// @todo Please remove those attributes that should not be searched.
            array('IDTecnicaInspecaoDocumento, IDTecnicaInspecao, documento_tecnica, nome_documento', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDTecnicaInspecao' => array(self::BELONGS_TO, 'TecnicaInspecao', 'IDTecnicaInspecao'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTecnicaInspecaoDocumento' => 'Idtecnica Inspecao Documento',
            'IDTecnicaInspecao' => 'Idtecnica Inspecao',
            'documento_tecnica' => 'Documento Tecnica',
            'nome_documento' => 'Nome do documento',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDTecnicaInspecaoDocumento"',HTexto::tiraLetras($this->IDTecnicaInspecaoDocumento));
        $criteria->compare('"IDTecnicaInspecao"',$this->IDTecnicaInspecao);
        $criteria->compare('LOWER("documento_tecnica")',mb_strtolower($this->documento_tecnica),true);
        $criteria->compare('LOWER("nome_documento")',mb_strtolower($this->nome_documento),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDTecnicaInspecaoDocumento" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TecnicaInspecaoDocumento the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
