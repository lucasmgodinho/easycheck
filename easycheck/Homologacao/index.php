<?php
mb_internal_encoding('UTF8');
// Se necessário, mude o Path para o core do Yii//
$yii = '../yii-core/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

/**
 * @author Vinicius Schettino 04/10/13
 * Definições Importantes para identificar o ambiente e o cliente que está entrando na aplicação
 * Estas configurações serão diferentes para cada ambiente: Testes (local), Homologação (interno)
 * e Produção (remoto)
 */

//remove the following lines when in production mode//
defined('YII_DEBUG') or define('YII_DEBUG', true);

/** * Cliente que está tentando acessar o sistema */
define('CLIENTE', explode('.', $_SERVER["HTTP_HOST"])[0]);

// Caso o sistema esteja em manutenção, descomente abaixo

//if($_SERVER["REQUEST_URI"] != '/index.php?r=site/paginaManutencao')
//{
//    header("location: index.php?r=site/paginaManutencao");
//    exit(0);
//}

// Caso o sistema esteja em manutenção, comente abaixo

if($_SERVER["REQUEST_URI"] == '/index.php?r=site/paginaManutencao')
{
    header("location: index.php?r=site/login");
    exit(0);
}

/**
 * Dominios que nunca poderão ser acessados
 */

if (CLIENTE == 'public' || CLIENTE == 'information_schema' || CLIENTE == 'pg_catalog') {
    header("Status: 404 not found");
    header("location: erro.php");
    exit(0);
}
/** *  Link para arquivos estáticos */
define('ASSETS_LINK', 'http://' . $_SERVER["HTTP_HOST"] . '/assets/');

/** * Link principal da aplicação */
define('MAIN_LINK', 'http://' . $_SERVER["HTTP_HOST"] . '/');

/** * Caminho para a raiz do yii */
define('PATH', __DIR__);

define('PASTA_SISTEMA', DIRECTORY_SEPARATOR );

// ***fim das definições personalizadas*** \\

date_default_timezone_set("America/Bahia");
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
//Inclui a classe Yii na aplicação
require_once($yii);
//Cria a aplicação com a configuração setada em protected/config/main.php

Yii::createWebApplication($config)->run();


