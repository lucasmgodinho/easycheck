
jQuery(function () {

    $('.aviso').click(function () {
        var dataRole = $(this).attr("data-role");
        if (dataRole != "" && dataRole != "undefined" && dataRole != null) {
            alert(dataRole);
        }
    });

});

var FormComponents = function () {

    var handleColorPicker = function () {
        if (!jQuery().colorpicker) {
            return;
        }
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });
        $('.colorpicker-rgba').colorpicker();
    }

    return {
        //main function to initiate the module
        init: function () {
            handleColorPicker();
        }

    };

}();