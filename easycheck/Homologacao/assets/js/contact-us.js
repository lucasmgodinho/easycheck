var ContactUs = function() {

    return {
        //main function to initiate the module
        init: function() {
            var map;
            $(document).ready(function() {
                map = new GMaps({
                    div: '#map',
                    lat: -21.7598,
                    lng: -43.351392
                });
                var marker = map.addMarker({
                    lat: -21.7598,
                    lng: -43.351392,
                    title: 'VOCÊ Tecnologia da Informação',
                    infoWindow: {
                        content: "<b>VOCÊ Tecnologia da Informação</b> Rua Santo Antônio, 2° e 3° andares. Juiz de Fora, MG, Brasil"
                    }
                });

                marker.infoWindow.open(map, marker);
            });
        }
    };

}();