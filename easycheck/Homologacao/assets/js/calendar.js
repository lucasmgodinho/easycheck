var Calendar = function () {


    return {
        //main function to initiate the module
        init: function (data) {
            Calendar.initCalendar(data);
        },

        initCalendar: function (data) {

            if (!jQuery().fullCalendar) {
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            if (App.isRTL()) {
                 if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        right: 'title',
                        center: ''
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        right: 'title',
                        center: ''
                    };
                }                
            } else {
                 if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        left: 'title',
                        center: ''
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        left: 'title',
                        center: ''
                    };
                }
            }
           

            var initDrag = function (el) {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim(el.text()) // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                el.data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                el.draggable({
                    zIndex: 999,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0 //  original position after the drag
                });
            }

            var addEvent = function (title) {
                if(title.length != 0) {
                    title = title.length == 0 ? "Untitled Event" : title;
                    var html = $('<div class="external-event label">' + title + '</div>');
                    jQuery('#event_box').append(html);
                    initDrag(html);
                }
            }

            $('#external-events div.external-event').each(function () {
                initDrag($(this))
            });

            $('#event_add').unbind('click').click(function () {
                var title = $('#event_title').val();
                if(title.length != 0) {
                    addEvent(title);
                    $('#event_title').val("");
                }else{
                    alert("Não se pode agendar um horário sem uma descrição.");
                }
            });

            //predefined events
            $('#event_box').html("");
            addEvent("Ex. Clínico de João Alberto (Maracutaia) ");
            addEvent("Ex. Complementar de Deise Linda (Grupo Você)");
            addEvent("Ex. Clínico reservado para a Santa Luzia");

            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                header: h,
                defaultView: 'agendaDay',
                slotMinutes: 5,
                defaultEventMinutes: 5,
                allDaySlot: false,
                allDayText: 'Dia inteiro',
                firstHour: 7,
                axisFormat: 'H(:mm)',
                timeFormat: {
                    agenda: 'H:mm{ - h:mm}'
                },
                monthNames: ['Janeiro', 'Fevereiro', 'Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
                buttonText: {
                    prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
                    next: "<span class='fc-text-arrow'>&rsaquo;</span>",
                    today: 'Hoje',
                },
                columnFormat: {
                    month: 'ddd',
                    week: 'ddd d/M',
                    day: "dddd, d 'de' MMMM"
                },
                titleFormat: {
                    month: 'MMMM yyyy',
                    week: "d 'de' MMM [ yyyy]{ '&#8212;'[ d 'de' MMM] '('yyyy')'}",
                    day: "dddd, d 'de' MMMM 'de' yyyy"
                },
                allDay: false,
                editable: false, // não permite mudar um agendamento depois de colocado no calendário por "Drag"
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function (date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.className = $(this).attr("data-class");

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // remove o evento depois de colocado no calendário
                    $(this).remove();
                },
                events: data,
                eventClick: function(data) {
                    var modal = $('#modal-trabalhador');
                    modal.modal('show');
                    $("#iframe-trabalhador").attr("src", data.action+"&in-modal=in-modal&id="+data.id);
                },
            });
        }

    };

}();
// green
// yellow
// blue